<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $product_id
 * @property integer $vendor_id
 * @property string $uom
 * @property string $batch
 * @property string $notes
 * @property float $trans_sale
 * @property float $trans_amount
 * @property string $trans_type
 * @property string $trans_on
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property Product $product
 * @property Vendor $vendor
 */
class Inventory extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'product_id',
        'vendor_id',
        'uom',
        'batch',
        'notes',
        'trans_sale',
        'trans_amount',
        'trans_type',
        'trans_on',
        'created_by'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor');
    }
}
