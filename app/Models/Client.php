<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $client_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $birthday
 * @property string $gender
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property integer $state_id
 * @property string $zip
 * @property string $phone
 * @property boolean $to_text
 * @property string $id_type
 * @property string $id_num
 * @property string $cert_type
 * @property string $cert_num
 * @property string $referred
 * @property string $notes
 * @property string $uip
 * @property string $pass_code
 * @property string $status
 * @property string $last_activity
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Client extends Model
{
    use SoftDeletes;

    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';
    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * @var array
     */
    protected $fillable = [
        'client_id',
        'first_name',
        'last_name',
        'email',
        'birthday',
        'gender',
        'address_1',
        'address_2',
        'city',
        'state_id',
        'zip',
        'phone',
        'to_text',
        'id_type',
        'id_num',
        'cert_type',
        'cert_num',
        'referred',
        'notes',
        'uip',
        'pass_code',
        'status',
        'last_activity',
        'created_by'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public function state()
    {
        return $this->hasOne(State::class);
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE
        ];
    }

    public static function getGenders()
    {
        return [
            self::GENDER_MALE,
            self::GENDER_FEMALE
        ];
    }

    public static function getIdTypes()
    {
        return [
            'test'
        ];
    }

    public static function getIdTypesList()
    {
        return [
            'test' => 'test'
        ];
    }

    public static function getCertTypes()
    {
        return [
            'test'
        ];
    }

    public static function getCertTypesList()
    {
        return [
            'test' => 'test'
        ];
    }
}
