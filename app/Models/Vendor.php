<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $vendor_id
 * @property string $vendor
 * @property string $first_name
 * @property string last_name
 * @property string email
 * @property string phone
 * @property string $city
 * @property string $zip
 * @property string $address_1
 * @property string $address_2
 * @property string $status
 * @property string $last_activity
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Vendor extends Model
{
    use SoftDeletes;

    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';

    /**
     * @var array
     */
    protected $fillable = [
        'vendor_id',
        'vendor',
        'first_name',
        'last_name',
        'email',
        'phone',
        'city',
        'zip',
        'address_1',
        'address_2',
        'status',
        'last_activity',
        'created_by'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE
        ];
    }
}
