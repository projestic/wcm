<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $pass_code
 * @property string $phone
 * @property string $email
 * @property boolean $auth
 * @property string $role_p
 * @property string $role_d
 * @property string $role_u
 * @property string $auth_a
 * @property string $auth_w
 * @property string $auth_p
 * @property string $auth_c
 * @property string $auth_d
 * @property string $auth_s
 * @property string $auth_v
 * @property string $auth_r
 * @property string $auth_x
 * @property string $auth_z
 * @property string $status
 * @property string $last_login
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Staff extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'staff';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'pass_code',
        'phone',
        'email',
        'auth',
        'role_p',
        'role_d',
        'role_u',
        'auth_a',
        'auth_w',
        'auth_p',
        'auth_c',
        'auth_d',
        'auth_s',
        'auth_v',
        'auth_r',
        'auth_x',
        'auth_z',
        'status',
        'last_login',
        'created_by'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

}
