<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $item_id
 * @property string $category
 * @property string $product
 * @property string $description
 * @property string detail_link
 * @property string $picture_1
 * @property string $picture_2
 * @property string $picture_3
 * @property float $cost
 * @property string $unit
 * @property string $option_1
 * @property float $units_1
 * @property float $price_1
 * @property string $option_2
 * @property float $units_2
 * @property float $price_2
 * @property string $option_3
 * @property float $units_3
 * @property float $price_3
 * @property string $option_4
 * @property float $units_4
 * @property float $price_4
 * @property string $option_5
 * @property float $units_5
 * @property float $price_5
 * @property string $tax
 * @property float $tax_rate
 * @property float $inv_alert
 * @property string $position
 * @property string $status
 * @property integer $sales_count
 * @property float $sales_dollars
 * @property string $notes
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Product extends Model
{
    use SoftDeletes;

    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';
    const TAX_METHOD_NONE = 'None';
    const TAX_METHOD_SIMPLE = 'Simple';
    const TAX_METHOD_TABLE = 'Table';
    const POSITION_FEATURED = 'Featured';
    const POSITION_REGULAR = 'Regular';

    /**
     * @var array
     */
    protected $fillable = [
        'item_id',
        'category',
        'product',
        'description',
        'detail_link',
        'picture_1',
        'picture_2',
        'picture_3',
        'cost',
        'unit',
        'option_1',
        'units_1',
        'price_1',
        'option_2',
        'units_2',
        'price_2',
        'option_3',
        'units_3',
        'price_3',
        'option_4',
        'units_4',
        'price_4',
        'option_5',
        'units_5',
        'price_5',
        'tax',
        'tax_rate',
        'inv_alert',
        'position',
        'status',
        'sales_count',
        'sales_dollars',
        'notes',
        'created_by'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE
        ];
    }

    public static function getPositions()
    {
        return [
            self::POSITION_FEATURED,
            self::POSITION_REGULAR
        ];
    }

    public static function getCategories()
    {
        return [
            'Big Stuff',
            'Cool Stuff'
        ];
    }

    public static function getCategoriesList()
    {
        return [
            'Big Stuff' => 'Big Stuff',
            'Cool Stuff' => 'Cool Stuff'
        ];
    }

    public static function getUnitTypes()
    {
        return [
            'test',
        ];
    }

    public static function getUnitTypesList()
    {
        return [
            'test' => 'test',
        ];
    }

    public static function getTaxMethods()
    {
        return [
            self::TAX_METHOD_NONE,
            self::TAX_METHOD_SIMPLE,
            self::TAX_METHOD_TABLE,
        ];
    }

    public static function getTaxMethodsList()
    {
        return [
            self::TAX_METHOD_NONE => 'None',
            self::TAX_METHOD_SIMPLE => 'Simple %',
            self::TAX_METHOD_TABLE => 'Tax Table'
        ];
    }
}
