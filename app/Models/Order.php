<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $client_id
 * @property string $description
 * @property string $notes
 * @property float $sub_total
 * @property float $tax
 * @property float $delivery
 * @property float $adjust
 * @property float $amount_due
 * @property string $pay_method
 * @property string $deliver_by
 * @property string $source
 * @property string $uip
 * @property integer $did
 * @property string $delivery_accepted
 * @property string $delivery_made
 * @property string $date
 * @property integer $uid
 * @property string $status
 * @property string $last_login
 * @property string $created_at
 * @property string $updated_at
 */
class Order extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'client_id',
        'order_id',
        'description',
        'notes',
        'sub_total',
        'tax',
        'delivery',
        'adjust',
        'amount_due',
        'pay_method',
        'deliver_by',
        'source',
        'uip',
        'did',
        'delivery_accepted',
        'delivery_made',
        'date',
        'uid',
        'status',
        'last_login'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
