<?php

namespace App\Search;

use App\Models\Vendor;
use Illuminate\Http\Request;

class VendorSearch
{
    const PAGE_SIZE = 50;

    public static function apply(Request $request)
    {
        $query = (new Vendor)->newQuery();

        if ($search = $request->get('search')) {
                $query->where('vendor_id', 'LIKE', "%$search%")
                    ->orWhere('vendor', 'LIKE', "%$search%");
        }

        return $query->paginate(self::PAGE_SIZE)
            ->appends($request->except('page'));
    }
}
