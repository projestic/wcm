<?php

namespace App\Search;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientSearch
{
    const PAGE_SIZE = 50;

    public static function apply(Request $request)
    {
        $query = (new Client)->newQuery();

        if ($search = $request->get('search')) {
            $query->where('client_id', 'LIKE', "%$search%")
                ->orWhere('first_name', 'LIKE', "%$search%")
                ->orWhere('last_name', 'LIKE', "%$search%");
        }

        return $query->paginate(self::PAGE_SIZE)
            ->appends($request->except('page'));
    }
}
