<?php

namespace App\Search;

use App\Models\Staff;
use Illuminate\Http\Request;

class StaffSearch
{
    const PAGE_SIZE = 50;

    public static function apply(Request $request)
    {
        $query = (new Staff)->newQuery();

        if ($search = $request->get('search')) {
            $query->where('user_id', 'LIKE', "%$search%")
                ->orWhere('first_name', 'LIKE', "%$search%")
                ->orWhere('last_name', 'LIKE', "%$search%");
        }

        return $query->paginate(self::PAGE_SIZE)
            ->appends($request->except('page'));
    }
}
