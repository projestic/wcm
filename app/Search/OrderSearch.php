<?php

namespace App\Search;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderSearch
{
    const PAGE_SIZE = 50;

    public static function apply(Request $request)
    {
        $query = (new Order)->newQuery();

        if ($search = $request->get('search')) {
            $query->where('order_id', 'LIKE', "%$search%");
        }

        return $query->paginate(self::PAGE_SIZE)
            ->appends($request->except('page'));
    }
}
