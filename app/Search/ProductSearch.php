<?php

namespace App\Search;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductSearch
{
    const PAGE_SIZE = 50;

    public static function apply(Request $request)
    {
        $query = (new Product)->newQuery();

        if ($search = $request->get('search')) {
            $query->where('item_id', 'LIKE', "%$search%")
                ->orWhere('product', 'LIKE', "%$search%");
        }

        return $query->paginate(self::PAGE_SIZE)
            ->appends($request->except('page'));
    }
}
