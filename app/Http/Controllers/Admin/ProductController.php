<?php namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Search\ProductSearch;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;

class ProductController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.product.index', [
            'products' => ProductSearch::apply($request)
        ]);
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function view($id)
    {
        return view('admin.product.view');
    }

    public function store(StoreProductRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = '1';
        $input['item_id'] = 'P-' . strtoupper(substr(uniqid(), 0, 7));

        Product::create($input);

        return redirect('admin/products');
    }

    public function delete($id)
    {
        Product::find($id)->delete();

        return redirect('admin/products');
    }

    public function uploadPicture()
    {
        return view('admin.product.upload_picture');
    }

    public function storePicture(Request $request)
    {
        $input = $request->all();

        if ($input['picture']){
            $picture = json_decode($input['picture']);
            $filteredData = substr($picture->data, strpos($picture->data, ",") + 1);
            $unencodedData = base64_decode($filteredData);
            \Storage::disk('local')->put($input['original_file_name'], $unencodedData);
        }

        return redirect('admin/products');
    }
}