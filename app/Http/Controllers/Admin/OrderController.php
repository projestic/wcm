<?php namespace App\Http\Controllers\Admin;

use App\Search\OrderSearch;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.order.index', [
            'orders' => OrderSearch::apply($request)
        ]);
    }

    public function delete($id)
    {
        Order::find($id)->delete();

        return redirect('admin/orders');
    }
}