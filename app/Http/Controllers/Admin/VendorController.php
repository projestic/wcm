<?php namespace App\Http\Controllers\Admin;

use App\Models\Vendor;
use App\Http\Requests\StoreVendorRequest;
use App\Search\VendorSearch;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.vendor.index', [
            'vendors' => VendorSearch::apply($request)
        ]);
    }

    public function create()
    {
        return view('admin.vendor.create');
    }

    public function view($id)
    {
        return view('admin.vendor.view');
    }

    public function delete($id)
    {
        Vendor::find($id)->delete();

        return redirect('admin/vendors');
    }

    public function store(StoreVendorRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = '1';
        $input['vendor_id'] = 'V-' . strtoupper(substr(uniqid(), 0, 7));

        Vendor::create($input);

        return redirect('admin/vendors');
    }
}