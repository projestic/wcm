<?php namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\State;
use App\Search\ClientSearch;
use Illuminate\Http\Request;
use App\Http\Requests\StoreClientRequest;

class ClientController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.client.index', [
            'clients' => ClientSearch::apply($request)
        ]);
    }

    public function create()
    {
        return view('admin.client.create', [
            'states' => State::pluck('name', 'id')
        ]);
    }

    public function view($id)
    {
        return view('admin.client.view');
    }

    public function store(StoreClientRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = '1';
        $input['status'] = Client::STATUS_ACTIVE;
        $input['client_id'] = 'C-' . strtoupper(substr(uniqid(), 0, 7));

        Client::create($input);

        return redirect('admin/clients');
    }

    public function delete($id)
    {
        Client::find($id)->delete();

        return redirect('admin/clients');
    }
}