<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreInventoryRequest;
use App\Models\Vendor;
use App\Models\Inventory;
use App\Models\Product;

class InventoryController extends Controller
{
    public function create()
    {
        $vendors = Vendor::all()->mapWithKeys(function ($item) {
            return [
                $item['id'] => $item['vendor_id'] . ': ' . $item['vendor']
            ];
        });
        $products = Product::all()->mapWithKeys(function ($item) {
            return [
                $item['id'] => $item['item_id'] . ': ' . $item['product'] . ' ' . $item['unit_1']
            ];
        });
        return view('admin.inventory.create', [
            'vendors' => $vendors,
            'products' => $products
        ]);
    }

    public function store(StoreInventoryRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = '1';

        Inventory::create($input);

        return redirect('admin/products');
    }

    public function delete($id)
    {
        Inventory::find($id)->delete();

        return redirect('admin/inventories');
    }
}