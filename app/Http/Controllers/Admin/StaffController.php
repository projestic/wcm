<?php namespace App\Http\Controllers\Admin;

use App\Search\StaffSearch;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.staff.index', [
            'staff' => StaffSearch::apply($request)
        ]);
    }

    public function delete($id)
    {
        Staff::find($id)->delete();

        return redirect('admin/staff');
    }
}