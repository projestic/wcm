<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Client;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'max:75',
            'last_name' => 'max:75',
            'email' => 'nullable|unique:clients,email|email|max:75',
            'phone' => 'max:15',
            'birthday' => 'nullable|date_format:Y-m-d',
            'gender' => 'required|in:' . implode(',', Client::getGenders()),
            'address_1' => 'max:50',
            'address_2' => 'max:50',
            'city' => 'max:50',
            'state' => 'exists:states,id',
            'zip' => 'max:15',
            'to_text' => 'accepted',
            'id_type' => 'in:' . implode(',', Client::getIdTypes()),
            'id_num' => 'max:50',
            'cert_type' => 'nullable|in:' . implode(',', Client::getCertTypes()),
            'cert_num' => 'max:50',
            'referred' => 'max:100',
            'uip' => 'max:15',
            'pass_code' => 'max:25'
        ];
    }
}
