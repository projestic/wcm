<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Product;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|in:' . implode(',', Product::getCategories()),
            'product' => 'required|max:50',
            'picture_1' => 'max:75',
            'picture_2' => 'max:75',
            'option_1' => 'max:25',
            'units_1' => 'numeric',
            'price_1' => 'numeric',
            'option_2' => 'max:25',
            'units_2' => 'numeric',
            'price_2' => 'nullable|numeric',
            'option_3' => 'max:25',
            'units_3' => 'numeric',
            'price_3' => 'nullable|numeric',
            'option_4' => 'max:25',
            'units_4' => 'numeric',
            'price_4' => 'nullable|numeric',
            'option_5' => 'max:25',
            'units_5' => 'numeric',
            'price_5' => 'nullable|numeric',
            'tax' => 'required|in:' . implode(',', Product::getTaxMethods()),
            'tax_rate' => 'required|numeric',
            'position' => 'required|in:' . implode(',', Product::getPositions()),
            'status' => 'required|in:' . implode(',', Product::getStatuses()),
            'inv_alert' => 'nullable|numeric'
        ];
    }
}
