<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Vendor;

class StoreVendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor' => 'required|unique:vendors,vendor|max:75',
            'first_name' => 'max:75',
            'last_name' => 'max:75',
            'email' => 'nullable|unique:vendors,email|email|max:75',
            'phone' => 'max:15',
            'city' => 'max:50',
            'zip' => 'max:15',
            'address_1' => 'max:50',
            'address_2' => 'max:50',
            'status' => 'in:' . implode(',', Vendor::getStatuses())
        ];
    }
}
