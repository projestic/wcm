<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Menu;

class MenuMiddleware
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $user = $this->auth->user();

        Menu::make('adminMenu', function($menu){
            $menu->add('<i class="fa fa-rocket"></i>Dashboard', ['route' => 'admin.dashboard'])
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-sitemap"></i>Workflow')
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-cart-plus"></i>Products', ['route' => 'admin.products'])
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-rocket"></i>Clients', ['route' => 'admin.clients'])
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-dollar"></i>Orders', ['route' => 'admin.orders'])
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-automobile"></i>Deliveries')
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-user-o"></i>Staff', ['route' => 'admin.staff'])
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-truck"></i>Vendors', ['route' => 'admin.vendors'])
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-bar-chart"></i>Reports')
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-wrench"></i>System')
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-wrench"></i>Super User')
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-life-buoy"></i>Support')
                ->link->attr(['class' => 'navlink w-nav-link']);
            $menu->add('<i class="fa fa-sign-out"></i>Log Out', ['route' => 'logout'])
                ->link->attr([
                    'class' => 'navlink w-nav-link',
                    'onclick' => 'event.preventDefault(); document.getElementById(\'logout-form\').submit();'
                ]);
        });

        return $next($request);
    }
}