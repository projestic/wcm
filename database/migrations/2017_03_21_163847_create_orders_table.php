<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->string('order_id', 25);
            $table->text('description')->nullable();
            $table->text('notes')->nullable();
            $table->float('sub_total', 7)->nullable();
            $table->float('tax', 7)->nullable();
            $table->float('delivery', 7)->nullable();
            $table->float('adjust', 7)->nullable();
            $table->float('amount_due', 7)->nullable();
            $table->string('pay_method', 1)->nullable();
            $table->string('deliver_by', 25)->nullable();
            $table->string('source', 200)->nullable();
            $table->string('uip', 15)->nullable();
            $table->integer('did')->nullable();
            $table->string('delivery_accepted', 14)->nullable();
            $table->string('delivery_made', 14)->nullable();
            $table->date('date')->nullable();
            $table->integer('uid')->nullable();
            $table->string('status', 1)->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
