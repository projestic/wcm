<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftDeletesForAllModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function ($table) {
            $table->softDeletes();
        });
        Schema::table('inventories', function ($table) {
            $table->softDeletes();
        });
        Schema::table('orders', function ($table) {
            $table->softDeletes();
        });
        Schema::table('products', function ($table) {
            $table->softDeletes();
        });
        Schema::table('staff', function ($table) {
            $table->softDeletes();
        });
        Schema::table('vendors', function ($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function ($table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('inventories', function ($table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('orders', function ($table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('products', function ($table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('staff', function ($table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('vendors', function ($table) {
            $table->dropColumn('deleted_at');
        });
    }
}
