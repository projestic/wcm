<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->string('uom', 2)->nullable();
            $table->string('batch', 75)->nullable();
            $table->text('notes')->nullable();
            $table->float('trans_sale', 10)->nullable();
            $table->decimal('trans_amount', 10, 4)->nullable();
            $table->string('trans_type', 1)->nullable();
            $table->string('trans_on', 14)->nullable();
            $table->integer('created_by');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
