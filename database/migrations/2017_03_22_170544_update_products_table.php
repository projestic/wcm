<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('product', 50)->change();
            $table->text('detail_link')->nullable()->change();
            $table->string('tax', 25)->nullable(false)->change();
            $table->string('position', 25)->nullable(false)->change();
            $table->string('status', 25)->nullable(false)->change();
            $table->decimal('tax_rate', 10, 4)->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
