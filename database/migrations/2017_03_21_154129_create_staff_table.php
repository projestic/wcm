<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 15)->unique();
            $table->string('first_name', 15)->nullable();
            $table->string('last_name', 15)->nullable();
            $table->string('pass_code', 15)->nullable();
            $table->string('phone', 25)->nullable();
            $table->string('email', 75)->nullable();
            $table->tinyInteger('auth')->nullable();
            $table->string('role_p', 1)->nullable();
            $table->string('role_d', 1)->nullable();
            $table->string('role_u', 1)->nullable();
            $table->string('auth_a', 1)->nullable();
            $table->string('auth_w', 1)->nullable();
            $table->string('auth_p', 1)->nullable();
            $table->string('auth_c', 1)->nullable();
            $table->string('auth_d', 1)->nullable();
            $table->string('auth_s', 1)->nullable();
            $table->string('auth_v', 1)->nullable();
            $table->string('auth_r', 1)->nullable();
            $table->string('auth_x', 1)->nullable();
            $table->string('auth_z', 1)->nullable();
            $table->string('status', 1)->nullable();
            $table->timestamp('last_login')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
