<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE clients MODIFY COLUMN client_id VARCHAR(15) NOT NULL");
        DB::statement("ALTER TABLE clients MODIFY COLUMN first_name VARCHAR(75) DEFAULT NULL");
        DB::statement("ALTER TABLE clients MODIFY COLUMN last_name VARCHAR(75) DEFAULT NULL");
        DB::statement("ALTER TABLE clients MODIFY COLUMN gender VARCHAR(1) NOT NULL");
        DB::statement("ALTER TABLE clients MODIFY COLUMN email VARCHAR(75) DEFAULT NULL");
        DB::statement("ALTER TABLE clients MODIFY COLUMN phone VARCHAR(15) DEFAULT NULL AFTER email");
        DB::statement("ALTER TABLE clients MODIFY COLUMN status VARCHAR(25) DEFAULT NULL AFTER pass_code");

        Schema::table('clients', function (Blueprint $table) {
            $table->date('birthday')->nullable()->change();
            $table->string('address_1', 50)->nullable()->change();
            $table->string('address_2', 50)->nullable()->change();
            $table->string('city', 50)->nullable()->change();
            $table->string('zip', 15)->nullable()->change();
            $table->string('cert_type', 50)->nullable()->change();
            $table->string('cert_num', 50)->nullable()->change();
            $table->string('referred', 50)->nullable()->change();
            $table->text('notes')->nullable()->change();
            $table->string('uip', 15)->nullable()->change();
            $table->string('pass_code', 25)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
