<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_id', 25)->unique();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email', 100)->unique();
            $table->date('birthday');
            $table->enum('gender', ['F', 'M']);
            $table->string('address_1', 50);
            $table->string('address_2', 50);
            $table->string('city', 50);
            $table->integer('state_id')->unsigned();
            $table->string('zip', 15);
            $table->string('phone', 20);
            $table->tinyInteger('to_text');
            $table->string('id_type', 15);
            $table->string('id_num', 50);
            $table->string('cert_type', 50);
            $table->string('cert_num', 50);
            $table->string('referred', 100);
            $table->text('notes');
            $table->string('uip', 15);
            $table->string('pass_code', 25);
            $table->string('status', 1);
            $table->timestamp('last_activity')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
