<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE vendors MODIFY COLUMN first_name VARCHAR(75) DEFAULT NULL AFTER vendor");
        DB::statement("ALTER TABLE vendors MODIFY COLUMN last_name VARCHAR(75) DEFAULT NULL AFTER first_name");

        Schema::table('vendors', function (Blueprint $table) {
            $table->string('email', 75)->nullable()->after('last_name');
            $table->string('phone', 25)->nullable()->after('email');
            $table->string('status', 25)->change();
        });

        DB::statement("ALTER TABLE vendors MODIFY COLUMN city VARCHAR(50) DEFAULT NULL AFTER phone");
        DB::statement("ALTER TABLE vendors MODIFY COLUMN zip VARCHAR(15) DEFAULT NULL AFTER city");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors', function ($table) {
            $table->dropColumn('email');
            $table->dropColumn('phone');
        });
    }
}
