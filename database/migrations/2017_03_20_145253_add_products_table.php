<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_id', 25)->unique();
            $table->string('category', 50);
            $table->string('product', 50)->nullable();
            $table->text('description')->nullable();
            $table->string('detail_link', 75)->nullable();
            $table->string('picture_1', 75)->nullable();
            $table->string('picture_2', 75)->nullable();
            $table->string('picture_3', 75)->nullable();
            $table->float('cost', 7)->nullable();
            $table->string('unit', 10)->nullable();
            $table->string('option_1', 25)->nullable();
            $table->float('units_1', 7)->nullable();
            $table->float('price_1', 7)->nullable();
            $table->string('option_2', 25)->nullable();
            $table->float('units_2', 7)->nullable();
            $table->float('price_2', 7)->nullable();
            $table->string('option_3', 25)->nullable();
            $table->float('units_3', 7)->nullable();
            $table->float('price_3', 7)->nullable();
            $table->string('option_4', 25)->nullable();
            $table->float('units_4', 7)->nullable();
            $table->float('price_4', 7)->nullable();
            $table->string('option_5', 25)->nullable();
            $table->float('units_5', 7)->nullable();
            $table->float('price_5', 7)->nullable();
            $table->string('tax', 1)->nullable();
            $table->float('tax_rate', 10, 4)->nullable();
            $table->float('inv_alert', 10, 4)->nullable();
            $table->string('position', 2)->nullable();
            $table->string('status', 1);
            $table->integer('sales_count')->nullable();
            $table->float('sales_dollars', 7)->nullable();
            $table->text('notes')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
