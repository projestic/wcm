<?php
	$PageName = "Staff";
	include_once('preamble.php');
	include_once('header.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthU']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if (empty($_REQUEST["pp"])) {$pp=100000;} else {$pp = $_REQUEST['pp'];}
	if (empty($_REQUEST["offset"])) {$offset=0;} else {$offset = $_REQUEST['offset'];}

	//BUILD SORT
        if (empty($_REQUEST["s"])) {$s = "asc"; $s2 = "desc";} else {$s = $_REQUEST["s"];}
		if (empty($_REQUEST["sort"])) {$sort = "X";} else {$sort = $_REQUEST["sort"];}
        if ($sort =="P"){$Sorter = "FName $s";}
		elseif ($sort =="X"){$Sorter = "Fname $s";}
        else {$Sorter = "Fname $s";}

	//BUILD CLAUSES
		$URLphrase = "&pp=$pp";
		$Archive = date('YmdHi', strtotime('-2 years'));
			//if ($_SESSION['UserAuth'] > 1){$dbClause  = "WHERE (1)";}
			//else {$dbClause  = "WHERE (`LastActivity` > '{$Archive}')";}
		$dbClause  = "WHERE (`Status` != 'X')";
		$URLphrase = str_replace(" ", "+", $URLphrase); //&sort=$sort&

	//GRAB DATA
		$query1 = "
			SELECT * 
			FROM `Staff` 
			$dbClause 
			ORDER BY $Sorter
			LIMIT $offset,$pp
			";
		$result1 = sqliQuery($query1);
		$r = count($result1);
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader"><div><!-- HEADER TEXT -->&nbsp;</div>
        <div class="actionblock">
          <div class="submenudropdown w-dropdown" data-delay="0">
            <div class="submenutoggle w-dropdown-toggle">
              <div class="dropdowntext"><i class="iconimage fa" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true">&#xf013;</i></div>
              <div class="submenuicon w-icon-dropdown-toggle"></div>
            </div>
            <nav class="submenulist w-dropdown-list">
				<a class="submenulink w-dropdown-link" href="user-new.php">NEW Staff</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Sales Report</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Activity Report</a>
            </nav>
          </div>
        </div>
      </div>

      <div class="outputblock">
		<!-- SEARCH -->
        <div class="searchblock">
          <div class="w-form">
            <form class="w-clearfix" name="SearchForm" method="get" action="search.js">
              <input class="searchbutton w-button fa" type="submit" value="&#xf002;">
              <input class="searchbox w-input" id="Search" maxlength="75" name="Search" placeholder="Search" type="text">
            </form>
          </div>
        </div>
		<!-- SEARCH -->

<?php	if($r){	?>

		<table class="resultstable" id="table1">
		<thead>
		<tr>  
			<th class="resultsheader" style="text-align:left;" title="User #">User #</th>
			<th class="resultsheader" style="text-align:left;" title="Name">Name</th>
			<th class="resultsheader" style="text-align:center;" title="Email, Phone">Contact</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Roles">Roles</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Staff Access"> Admin Access</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Date Added">Added</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Last Login">Last Login</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Status">Status</th>
			<th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
		</tr>
		</thead>
<?php
			while ($row = mysqli_fetch_assoc($result1)){$r++; extract($row);
				print ($r % 2) ? "<tr class=\"resultsrow\" title=\"[ID: ".escape($UserID)."] ".escape($Fname)."\" style=\"cursor: default;\"> \n" 
					: "<tr class=\"resultsrow oddrow\" title=\"[ID: ".escape($UserID)."] ".escape($Fname)."\" style=\"cursor: default;\"> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($UserID)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($Fname)."</td> \n";
				/* if($Birthdate){
					$Month = substr($Birthdate, 2, 2); $BMonth = $_Months[$Month];
					$BDay = substr($Birthdate, 0, 2);
					echo "<td class=\"hidden-xs\">".$BMonth." ".$BDay."</td> \n";}
					else{echo "<td class=\"hidden-xs\">--</td> \n";}
				*/
				echo "<td class=\"resultscell\" style=\"text-align:center;\"> ";
				if($Email){
					echo "<a href=\"mailto:".escape($Email)."\" title=\"Email ".escape($Fname)."\" style=\"margin: 1px 5px;\"><i class=\"iconimage fa\" aria-hidden=\"true\">&#xf003;</i></a> \n";}
					else{echo "No Email";}
				if($Phone){
					echo "<a href=\"tel:".escape($Phone)."\" title=\"Call ".escape($Fname)."\" style=\"margin: 1px 5px;\"><i class=\"iconimage fa\" aria-hidden=\"true\">&#xf095;</i></a> \n";}
					else{echo "&nbsp;";}
				echo "</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\"> ";
				$Role = 0;
				if ($RoleP == "Y"){$Role ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Point of Sale\">&#xf155</i> ";}
				if ($RoleD == "Y"){$Role ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Delivery\">&#xf1b9</i> ";}
				if ($RoleU == "Y"){$Role ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"User\">&#xf2c0</i> ";}
				if ($Role == 0){
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"None\">&#xf05e</i> ";}
				echo "</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\"> ";
				$Access = 0;
				if ($AuthA == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Dashboard\">&#xf135</i> ";}
				if ($AuthW == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Workflow\">&#xf0e8</i> ";}
				if ($AuthP == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Products\">&#xf217</i> ";}
				if ($AuthC == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Clients\">&#xf1ae</i> ";}
				if ($AuthO == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Orders\">&#xf155</i> ";}
				if ($AuthD == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Deliveries\">&#xf1b9</i> ";}
				if ($AuthU == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Staff\">&#xf2c0</i> ";}
				if ($AuthV == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Vendors\">&#xf0d1</i> ";}
				if ($AuthR == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"Reports\">&#xf080</i> ";}
				if ($AuthS == "Y"){$Access ++; 
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"System\">&#xf0ad</i> ";}
				if($Access == 0){
					echo "<i class=\"fa\" style=\"margin-right: 5px;\" title=\"None\">&#xf05e</i> ";}
				echo "</td> \n";
					if($AddedOn){$Joined = date("M d Y", date2code(escape($AddedOn)));}else {$Joined = "--";}
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".$Joined."</td> \n";
					if($LastLogin){$Active = date("M d Y", date2code(escape($LastLogin)));}else {$Active = "--";}
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".$Active."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".escape($Status)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:center;\">  \n";
				echo "<i class=\"iconaction fa\" title=\"VIEW\" aria-hidden=\"true\" onclick=\"self.location='user-view.php?u=".$UID."';\">&#xf06e;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"EDIT\" aria-hidden=\"true\" onclick=\"self.location='user-edit.php?u=".$UID."';\" >&#xf044;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"DELETE\" aria-hidden=\"true\" onclick=\"confirmDelete('Are you sure you wish to delete: \\n[ID: ".escape($UserID)."] ".escape($Fname)."?', 'user-delete.php?u=".$UID."');\" >&#xf014;</i> \n";
				echo "</td> \n";
				echo "</tr>\n";
			}
			echo "</table>";
				if($r < 51){
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." STAFF</div> \n";
				}else{
					//replace with pagination
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." STAFF</div> \n";
				}
		//No Results
			}else{
				echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\"><h4>NO STAFF IN DATABASE</h4></div>";
			}
	?>

<!-- /results insert -->
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>
