//get file name every time input file change
var selected_file_name = '';
//this input file is inside your "crop-element"
$('input[type=file]').on('change', function(){
	selected_file_name = $(this)[0].files[0].name;
});
//before upload
robocrop.events.upload.before = function(picture){
    return [
        {name:'original_file_name' ,value:selected_file_name}
    ];
};

robocrop.events.upload.end = function(response){
    window.location = "products.php";
};