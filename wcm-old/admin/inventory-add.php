<?php
	$PageName = "Add Inventory";
	include_once('preamble.php'); 


  //STEVE'S CHECKING OF PARAMETERS

//echo "Inserted Variables: " . $_SESSION['UserID'] . " AND " . $_SERVER["REMOTE_ADDR"] . "<br>";

 //END  STEVE'S CHECKING OF PARAMETERS

	//CHECK AUTHORITY
	if ($_SESSION['AuthP']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

//echo $_SERVER["REMOTE_ADDR"] . "<BR>";

   //POST TO ITSELF
	if(!empty($_POST)){
		if(isset($_POST["ItemNo"], $_POST["InvAmt"])){
			//$Category	= trim($_POST["Category"]);
			//$ProductName	= trim($_POST["ProductName"]);
			$ItemNo	= trim($_POST["ItemNo"]); 
			$InvAmt = trim($_POST["InvAmt"]); 
			$VID = trim($_POST["VID"]); 
			$Batch = trim($_POST["Batch"]); 
			$Notes = trim($_POST["Notes"]);
			if(!empty($InvAmt) && !empty($ItemNo)){
				$TS = date("YmdHis");
				$Activity = "ADDED INVENTORY TO: ".$ItemNo;

			   //$Query1 = "INSERT INTO `Inventory` (`PID`, `TransAmnt`,`VID`,`Batch`, `Notes`, `AddedBy`, `TransOn`) VALUES (?, ?, ?, ?, ?, ?, ?)";
			   $Query1 = "INSERT INTO `Inventory` (`PID`, `TransAmnt`,`VID`,`Batch`, `Notes`, `TransOn`) VALUES (?, ?, ?, ?, ?, ?)";

			if($insert1 = $db->prepare($Query1)){

         //$insert1->bind_param('idissis', $ItemNo, $InvAmt, $VID, $Batch, $Notes, $_SESSION['UserID'], $TS);
		  $insert1->bind_param('idisss', $ItemNo, $InvAmt, $VID, $Batch, $Notes, $TS);
				if($insert1->execute()){
					$insert1->close();

					addActivity($Activity,$TS);
					updateVActivity($VID,$TS);
					header('Location: products.php');
					die();			
				}else{
					$error = $db->errno . ' ' . $db->error;
					echo "INSERT ERROR<br><div style=\"color:gray;\">". $error;
					//var_dump($this->db->error);
					var_dump($insert1);
					$insert1->error_list;
					$insert2->error_list;
					echo "</div>";
					die();
				}
			$insert->close();
			}else{
				$error = $db->errno . ' ' . $db->error;
				echo "PREPARE ERROR<br><div style=\"color:gray;\">". $error;
				//var_dump($this->db->error);
				$insert1->error_list;
				$insert2->error_list;
				echo "</div>";
				die();
			}
//
			} else {echo "Empty Fields: $ItemNo $InvAmt"; exit;}
		} else {echo "Fields not set: $ItemNo $InvAmt"; exit;}
	}//end post

	include_once('header.php');
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader">
        <div>&nbsp;</div>
      </div>

      <div class="outputblock">
        <div class="form700px w-form">
          <div class="formtitle">Enter Inventory Amount</div>
          <div class="forminstruction">This process adds inventory to your products in stock. Asterisked (*) information is necessary. You can add/edit inventory at any time. Product Status will automatically change to Inactive when inventory reaches 0.</div>
          <form id="form1" name="AddInventory" enctype="multipart/form-data" method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="ItemNo">*Product</label></div>
              <div class="w-col w-col-9 w-col-small-9"><select class="inputfield w-select" id="ItemNo" name="ItemNo" required="required">
              <option value="">Select Product...</option>
<?php
				
				//GRAB DATA
				listProductDD();
				//END GRAB DATA
?>
            </select>
              </div>
            </div>

<script>
      // Retrieve last key pressed.  Works in IE and Netscape.
      // Returns the numeric key code for the key pressed.
      function getKey(e)
      {
        if (window.event)
           return window.event.keyCode;
        else if (e)
           return e.which;
        else
           return null;
      }
      function restrictChars(e, obj)
      {
        var CHAR_AFTER_DP = 4;  // number of decimal places
        var validList = "0123456789.";  // allowed characters in field
        var key, keyChar;
        key = getKey(e);
        if (key == null) return true;
        // control keys
        // null, backspace, tab, carriage return, escape
        if ( key==0 || key==8 || key==9 || key==13 || key==27 )
           return true;
        // get character
        keyChar = String.fromCharCode(key);
        // check valid characters
        if (validList.indexOf(keyChar) != -1)
        {
          // check for existing decimal point
          var dp = 0;
          if( (dp = obj.value.indexOf( ".")) > -1)
          {
            if( keyChar == ".")
              return false;  // only one allowed
            else
            {
              // room for more after decimal point?
              if( obj.value.length - dp <= CHAR_AFTER_DP)
                return true;
            }
          }
          else return true;
        }
        // not a valid character
        return false;
      }
    </script>
            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="InvAmt">*Inventory Amount</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="InvAmt" maxlength="75" name="InvAmt" step=".0001" placeholder="Enter Desired Amount to be added to Inventory*" required="required" type="number" onKeyPress="return restrictChars(event, this)">
              </div>
            </div>

<div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="VID">Vendor Info</label></div>
              <div class="w-col w-col-4 w-col-small-4"><select class="inputfield w-select" id="VID" name="VID" required="required">
              <option value="">Select Vendor...</option>
<?php
				
				//GRAB DATA
				listVendorDD();
				//END GRAB DATA
?>
            </select>
              </div>
              <div class="w-col w-col-5 w-col-small-5"><input class="inputfield w-input" id="Batch" maxlength="500" name="Batch" placeholder="Batch Number" type="text"></div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Notes">Inventory Notes</label></div>
              <div class="w-col w-col-9 w-col-small-9"><textarea class="inputfield w-input" id="Notes" maxlength="500" name="Notes" placeholder="Inventory Transaction Notes - not visible to customers"></textarea></div>
            </div>

            <div class="floatcleardiv"></div>
            <div class="flexrow">
              <div><a class="cancelbutton w-button" href="products.php">Cancel</a>
              </div>
              <input class="submitbutton w-button" data-wait="Please wait..." type="submit" value="Submit">
            </div>
          </form>

        </div>
        <div class="spacerblock50"></div>
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>