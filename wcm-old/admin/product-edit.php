<?php
	$PageName = "Edit Product ";
	$ItemNo = $_REQUEST["p"];
	include_once('preamble.php'); 


	//CHECK AUTHORITY
	if ($_SESSION['AuthP']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

//GET PRODUCT INFO

 getProductInfo();
				
//DO POST TO ITSELF -SC
	if(!empty($_POST)){
		if(isset($_POST["Category"], $_POST["ProductName"])){
			$PID	= trim($_POST["PID"]);
			$Category	= trim($_POST["Category"]);
			$ProductName	= trim($_POST["ProductName"]);
			if(!empty($Category) && !empty($ProductName)){
				//$ItemID	= "P-".strtoupper(uniqidReal("6"));
				$ItemID	= trim($_POST["ItemID"]);			
				$ProductName = trim($_POST["ProductName"]); 
				$Description = trim($_POST["Description"]); 
				$DetailLink = trim($_POST["DetailLink"]); 
				$PicLink1 = trim($_POST["PicLink1"]); 
				$PicLink2 = trim($_POST["PicLink2"]);
				$PicLink3 = trim($_POST["PicLink3"]);
				$PType = trim($_POST["PType"]);
				$Strain = trim($_POST["Strain"]);
				$CBD = sprintf("%.2f", trim($_POST["CBD"])); 
				$CBN = sprintf("%.2f", trim($_POST["CBN"])); 
				$THC = sprintf("%.2f", trim($_POST["THC"])); 
				$Cost1 = sprintf("%.2f", trim($_POST["Cost1"])); 
				$Price1 = sprintf("%.2f", trim($_POST["Price1"])); 
				$Unit1 = trim($_POST["Unit1"]); 
				$Cost2 = sprintf("%.2f", trim($_POST["Cost2"])); 
				$Price2 = sprintf("%.2f", trim($_POST["Price2"])); 
				$Unit2 = trim($_POST["Unit2"]); 
				$Cost3 = sprintf("%.2f", trim($_POST["Cost3"])); 
				$Price3 = sprintf("%.2f", trim($_POST["Price3"])); 
				$Unit3 = trim($_POST["Unit3"]); 
				$Cost4 = sprintf("%.2f", trim($_POST["Cost4"])); 
				$Price4 = sprintf("%.2f", trim($_POST["Price4"])); 
				$Unit4 = trim($_POST["Unit4"]); 
				$Cost5 = sprintf("%.2f", trim($_POST["Cost5"])); 
				$Price5 = sprintf("%.2f", trim($_POST["Price5"])); 
				$Unit5 = trim($_POST["Unit5"]); 
				$Position = $_POST["Position"];
				$Status = $_POST["Status"];
				$Tax = trim($_POST["Tax"]); 
				$TaxRate = sprintf("%.4f", trim($_POST["TaxRate"])); 
				$InvAlert = trim($_POST["InvAlert"]);
				$Notes = trim($_POST["Notes"]);
				$TS = date("YmdHis");
				$Activity = "EDITED PRODUCT: ".$PID;
				

			//$Query1 = "INSERT INTO `Products` ( `ItemID`, `Category`, `Product`, `Descr`, `Detail`, `Pic1`, `Pic2`, `Pic3`, `PType`, `Strain`, `CBD`, `CBN`, `THC`, `Cost1`, `Price1`, `Unit1`, `Cost2`, `Price2`, `Unit2`, `Cost3`, `Price3`, `Unit3`, `Cost4`, `Price4`, `Unit4`, `Cost5`, `Price5`, `Unit5`, `Position`, `Status`, `Tax`, `TaxRate`, `InvAlert`, `Notes`, `AddedBy`, `AddedOn`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			//$Query1 = "INSERT INTO `Products` (`Category`, `Product`, `Descr`, `Detail`, `PType`, `Strain`, `CBD`, `CBN`, `THC`, `Cost1`, `Price1`, `Unit1`, `Cost2`, `Price2`, `Unit2`, `Cost3`, `Price3`, `Unit3`, `Cost4`, `Price4`, `Unit4`, `Cost5`, `Price5`, `Unit5`, `Position`, `Status`, `Tax`, `TaxRate`, `InvAlert`, `Notes`, `AddedBy`, `AddedOn`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			$Query1 = "UPDATE `Products` SET `Category`=?, `Product`=?, `Descr`=?, `Detail`=?, `Pic1`=?, `Pic2`=?, `Pic3`=?,`PType`=?, `Strain`=?, `CBD`=?, `CBN`=?, `THC`=?, `Cost1`=?, `Price1`=?, `Unit1`=?, `Cost2`=?, `Price2`=?, `Unit2`=?, `Cost3`=?, `Price3`=?, `Unit3`=?, `Cost4`=?, `Price4`=?, `Unit4`=?, `Cost5`=?, `Price5`=?, `Unit5`=?, `Position`=?, `Status`=?, `Tax`=?, `TaxRate`=?, `InvAlert`=?, `Notes`=? WHERE PID=?";
			
			//$Query1 = "UPDATE `Products` SET `Category`=? WHERE PID=?";

			//$Query2 = "INSERT INTO `Activity` (`UID`, `UIP`, `Descr`, `DateTime`) VALUES (?, ?, ?, ?)"; 

			if($insert1 = $db->prepare($Query1)){
			//$insert1->bind_param('ssssssssssiiiiisiisiisiisiissssiisis', $ItemID, $Category, $ProductName, $Description, $DetailLink, $PicLink1, $PicLink2, $PicLink3, $PType, $Strain, $CBD, $CBN, $THC, $Cost1, $Price1, $Unit1, $Cost2, $Price2, $Unit2, $Cost3, $Price3, $Unit3, $Cost4, $Price4, $Unit4, $Cost5, $Price5, $Unit5, $Position, $Status, $Tax, $TaxRate, $InvAlert, $_SESSION['UserID'], $TS);
         
         $insert1->bind_param('sssssssssiiiiisiisiisiisiissssiisi', $Category, $ProductName, $Description, $DetailLink, $PicLink1, $PicLink2, $PicLink3, $PType, $Strain, $CBD, $CBN, $THC, $Cost1, $Price1, $Unit1, $Cost2, $Price2, $Unit2, $Cost3, $Price3, $Unit3, $Cost4, $Price4, $Unit4, $Cost5, $Price5, $Unit5, $Position, $Status, $Tax, $TaxRate, $InvAlert, $Notes, $PID);
         
         //$insert1->bind_param('si', $Category, $PID);
				if($insert1->execute()){
					$insert1->close();
					addActivity($Activity,$TS);
					//$insert2 = $db->prepare($Query2);
					//$insert2->bind_param('isss', $_SESSION['UserID'], $_SERVER["REMOTE_ADDR"], $Activity, $TS);
					//$insert2->bind_param('i', $_SESSION['UserID']);
					//$insert2->execute();
					//$insert2->close();
					header('Location: products.php');
					die();			
				}else{
					$error = $db->errno . ' ' . $db->error;
					echo "INSERT ERROR<br><div style=\"color:gray;\">". $error;
					//var_dump($this->db->error);
					var_dump($insert1);
					$insert1->error_list;
					$insert2->error_list;
					echo "</div>";
					die();
				}
			$insert->close();
			}else{
				$error = $db->errno . ' ' . $db->error;
				echo "PREPARE ERROR<br><div style=\"color:gray;\">". $error;
				//var_dump($this->db->error);
				$insert1->error_list;
				$insert2->error_list;
				echo "</div>";
				die();
			}
//
			} else {echo "Empty Fields: $Category $ProductName"; exit;}
		} else {echo "Fields not set: $Category $ProductName"; exit;}
	}//end post

	include_once('header.php');
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?> <?php echo $ItemID; ?></h2>
    <div class="displayblock">

      <div class="tableheader">
        <div>&nbsp;</div>
      </div>

      <div class="outputblock">
        <div class="form700px w-form">
          <div class="formtitle">Enter Product Information</div>
          <div class="forminstruction">This process puts a product in your line-up. Only asterisked (*) information is necessary. The steps to add Product in your store are: 1) Upload images to Gallery; 2) Enter Product Information [this screen], 3) Add inventory & batch.  You can add/edit images and inventory at any time. If you have additional information you'd like to communicate to your customers - e.g., terpenoids - there are two options: 1) Put all your information in 'Description' field; 2) Create a separate webpage and put the link in 'External Detail Link'. To make the product invisible to the customers, change Status to 'Inactive'.  Status will automatically change to Inactive when inventory reaches 0.</div>
          <form id="form1" name="NewProduct" enctype="multipart/form-data" method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">
        <input type="hidden" name="ItemID" value="<?phpphp echo $ItemID ?>"><input type="hidden" name="PID" value="<?phpphp echo $PID ?>">
            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Category">*Category</label></div>
              <div class="w-col w-col-9 w-col-small-9"><select class="inputfield w-select" id="Category" name="Category" required="required">
              <option value="">Select one...</option>
<?php
				reset($_ProdCat);
				  
				foreach ( $_ProdCat as $key => $value ) {
				 if ($Category==$value) {
				 	$Catsel ="selected='selected'";
				 	//echo "dfdg";
				 	} else {
				 		//echo "cat:".$Category.", val: ".$value;
				 		$Catsel="";
				 	}
					echo "<option ". $Catsel." value=\"".$key."\">".$value." </option> \n";
				} 
				//$stevevar = "WTF";
?>
            </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="ProductName">*Product Name</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="ProductName" maxlength="75" name="ProductName" placeholder="Enter Product Name*" required="required" type="text" value="<?phpphp echo $ProductName; ?>">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Description">Description</label></div>
              <div class="w-col w-col-9 w-col-small-9"><textarea class="inputfield w-input" id="Description" maxlength="500" name="Description" placeholder="Product Description*" required="required"><?phpphp echo $Description; ?></textarea>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="DetailLink">External Detail Link</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="DetailLink" maxlength="75" name="DetailLink" value="<?phpphp echo $DetailLink; ?>" placeholder="https://" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="PicLink">Product Pics</label></div>
              <div class="w-col w-col-9 w-col-small-9">
			  <!-- PUT IN ROW & USE FOR EDIT SCREEN <img class="productimage" src="uploads/products/no-image.png"> -->
            <select class="inputfield w-select" id="PicLink1" name="PicLink1">
              <option value="no-image.png">Pic 1 - No Product Image</option>
<?php

if ($handle = opendir('uploads/products')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
        	 if ($entry==$PicLink1) {
        	 	
            echo "<option selected=\"selected\">".$entry."</option>";
         }
         else
         {
         	  echo "<option>".$entry."</option>";
         }
        }
    }
    closedir($handle);
}



				//FILE READ ON DIRECTORY
				//foreach ( $_ProdCat as $key => $value ) {
					//echo "<option value=\"".$key."\">".$value."</option> \n";
				//} 
?>
            </select>
            <select class="inputfield w-select" id="PicLink2" name="PicLink2">
              <option value="">Pic 2 - Blank</option>
<?php
				if ($handle = opendir('uploads/products')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
        	 if ($entry==$PicLink2) {
        	 	
            echo "<option selected=\"selected\">".$entry."</option>";
         }
         else
         {
         	  echo "<option>".$entry."</option>";
         }
        }
    }
    closedir($handle);
}
?>
            </select>
            <select class="inputfield w-select" id="PicLink3" name="PicLink3">
              <option value="">Pic 3 - Blank</option>
<?php
			if ($handle = opendir('uploads/products')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
        	 if ($entry==$PicLink3) {
        	 	
            echo "<option selected=\"selected\">".$entry."</option>";
         }
         else
         {
         	  echo "<option>".$entry."</option>";
         }
        }
    }
    closedir($handle);
}
?>
            </select>
                <div class="forminstruction">Load product images in your Gallery, they will then appear in the selection box.
                  <br>A default "no image" will appear if you don't associate a picture with this product.
                  <br>You can change the product image at any time through the EDIT screen.
				  <br>{coming soon: multiple images for products!}</div>
              </div>
            </div>
<!-- 
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Unit9">Characteristics</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit9" name="Unit9">
                  <option value="">Select one...</option>
                </select>			  
			  </div>
              <div class="w-col w-col-3 w-col-small-3">
			  </div>
              <div class="w-col w-col-3 w-col-small-3"> 
			  </div>
            </div>
-->

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Strain">*Strain</label></div>
              <div class="w-col w-col-9 w-col-small-9">
                <select class="inputfield w-select" id="Strain" name="Strain" required="required">
                  <option value="">Select one...</option>
                  <option value="I" <?phpphp if ('I'==$Strain) echo ' selected="selected"'?>>Indica</option>
                  <option value="S" <?phpphp if ('S'==$Strain) echo ' selected="selected"'?>>Sativa</option>
				  <option value="H" <?phpphp if ('H'==$Strain) echo ' selected="selected"'?>>Hybrid</option>
				  <option value="ID" <?phpphp if ('ID'==$Strain) echo ' selected="selected"'?>>Indica Dominant</option>
				  <option value="SD" <?phpphp if ('SD'==$Strain) echo ' selected="selected"'?>>Sativa Dominant</option>
				  <option value="U" <?phpphp if ('U'==$Strain) echo ' selected="selected"'?>>Unknown</option>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="PType">Cultivation</label></div>
              <div class="w-col w-col-9 w-col-small-9">
                <select class="inputfield w-select" id="PType" name="PType">
                  <option value="" <?phpphp if (''==$PType) echo ' selected="selected"'?>>Not Known, Not Displayed</option>
                  <option value="I" <?phpphp if ('I'==$PType) echo ' selected="selected"'?>>Indoor</option>
                  <option value="O"<?phpphp if ('O'==$PType) echo ' selected="selected"'?>>Outdoor</option>
				  <option value="G" <?phpphp if ('G'==$PType) echo ' selected="selected"'?>>Greenhouse</option>
				  <option value="U" <?phpphp if ('U'==$PType) echo ' selected="selected"'?>>Display as Unknown</option>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="CBD">Medical Grade</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="CBD" maxlength="10" name="CBD" placeholder="CBD%" type="text" value="<?phpphp echo $CBD ?>"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="CBN" maxlength="10" name="CBN" placeholder="CBN%" type="text" value="<?phpphp echo $CBN ?>"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="THC" maxlength="10" name="THC" placeholder="THC%" type="text" value="<?phpphp echo $THC ?>"></div>
            </div>

<!--  PRODUCT PRICING SECTION -->
            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost1">*Product Price Point #1</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost1" maxlength="10" name="Cost1" placeholder="Cost / Unit*" required="required" type="text" value="<?phpphp echo $Cost1 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price1" maxlength="10" name="Price1" placeholder="Price / Unit*" required="required" type="text" value="<?phpphp echo $Price1 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit1" name="Unit1" required="required">
                  <option value="">Select one...*</option>
<?php
					reset($_ProdUnit);
					//new selector
					foreach ($_ProdUnit as $key=>$value ) {
					 if ($Unit1==$key) {
				 	$Unitsel ="selected='selected'";
				 	} else {
				 		$Unitsel="";
				 	}
					echo "<option ". $Unitsel." value=\"".$key."\">".$value." </option> \n";
				   } 
					
					//end new selector
					
					//old selector
					//foreach ($_ProdUnit as $key=>$value ) {
						//echo "<option value=\"".$key."\" ";
						//echo ">".$value."</option> \n";
						
					//end old selector	
					//} 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost2">Product Price Point #2</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost2" maxlength="10" name="Cost2" placeholder="Cost / Unit" type="text" value="<?phpphp echo $Cost2 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price2" maxlength="10" name="Price2" placeholder="Price / Unit" type="text" value="<?phpphp echo $Price2 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit2" name="Unit2">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					//foreach ($_ProdUnit as $key=>$value ) {
						//echo "<option value=\"".$key."\" ";
						//echo ">".$value."</option> \n";
					//} 
					foreach ($_ProdUnit as $key=>$value ) {
					 if ($Unit2==$key) {
				 	$Unitsel ="selected='selected'";
				 	} else {
				 		$Unitsel="";
				 	}
					echo "<option ". $Unitsel." value=\"".$key."\">".$value." </option> \n";
				   } 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost3">Product Price Point #3</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost3" maxlength="10" name="Cost3" placeholder="Cost / Unit" type="text" value="<?phpphp echo $Cost3 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price3" maxlength="10" name="Price3" placeholder="Price / Unit" type="text" value="<?phpphp echo $Price3 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit3" name="Unit3">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					//foreach ($_ProdUnit as $key=>$value ) {
						//echo "<option value=\"".$key."\" ";
						//echo ">".$value."</option> \n";
					//} 
					foreach ($_ProdUnit as $key=>$value ) {
					 if ($Unit3==$key) {
				 	$Unitsel ="selected='selected'";
				 	} else {
				 		$Unitsel="";
				 	}
					echo "<option ". $Unitsel." value=\"".$key."\">".$value." </option> \n";
				   } 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost4">Product Price Point #4</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost4" maxlength="10" name="Cost4" placeholder="Cost / Unit" type="text" value="<?phpphp echo $Cost4 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price4" maxlength="10" name="Price4" placeholder="Price / Unit" type="text" value="<?phpphp echo $Price4 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit4" name="Unit4">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					//foreach ($_ProdUnit as $key=>$value ) {
						//echo "<option value=\"".$key."\" ";
						//echo ">".$value."</option> \n";
					//} 
					foreach ($_ProdUnit as $key=>$value ) {
					 if ($Unit4==$key) {
				 	$Unitsel ="selected='selected'";
				 	} else {
				 		$Unitsel="";
				 	}
					echo "<option ". $Unitsel." value=\"".$key."\">".$value." </option> \n";
				   } 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost5">Product Price Point #5</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost5" maxlength="10" name="Cost5" placeholder="Cost / Unit" type="text" value="<?phpphp echo $Cost5 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price5" maxlength="10" name="Price5" placeholder="Price / Unit" type="text" value="<?phpphp echo $Price5 ?>"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit5" name="Unit5">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					//foreach ($_ProdUnit as $key=>$value ) {
						//echo "<option value=\"".$key."\" ";
						//echo ">".$value."</option> \n";
					//} 
					foreach ($_ProdUnit as $key=>$value ) {
					 if ($Unit5==$key) {
				 	$Unitsel ="selected='selected'";
				 	} else {
				 		$Unitsel="";
				 	}
					echo "<option ". $Unitsel." value=\"".$key."\">".$value." </option> \n";
				   } 
?>
                </select>
              </div>
            </div>
<!--  /PRODUCT PRICING SECTION -->

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Tax">*Tax Method &amp; Rate(s)</label></div>
              <div class="w-col w-col-6 w-col-small-6">
                <select class="inputfield w-select" id="Tax" name="Tax" required="required">
                  <option value="" <?phpphp if (''==$Tax) echo ' selected="selected"'?>>Select one...</option>
                  <option value="N" <?phpphp if ('N'==$Tax) echo ' selected="selected"'?>>None</option>
                  <option value="S" <?phpphp if ('S'==$Tax) echo ' selected="selected"'?>>Simple %</option>
                  <option value="T" <?phpphp if ('T'==$Tax) echo ' selected="selected"'?> disabled="disabled">Tax Table</option>
                </select>
              </div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="TaxRate" maxlength="10" name="TaxRate" placeholder="Tax Rate" type="text" value="<?phpphp echo $TaxRate ?>"></div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Position">Position</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Position1" name="Position" type="radio" value="F" <?phpphp if ('F'==$Position) echo ' checked="checked"'?>>
                  <label class="fieldlabel w-form-label" for="Position1">Featured</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Position2" name="Position" type="radio" value="R" <?phpphp if ('R'==$Position) echo ' checked="checked"'?>>
                  <label class="fieldlabel w-form-label" for="Position2">Regular</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">&nbsp;</div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Status">Product Status</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Status1" name="Status" type="radio" value="A" <?phpphp if ('A'==$Status) echo ' checked="checked"'?>>
                  <label class="fieldlabel w-form-label" for="Status1">Active</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Status2" name="Status" type="radio" value="I" <?phpphp if ('I'==$Status) echo ' checked="checked"'?>>
                  <label class="fieldlabel w-form-label" for="Status2">Inactive</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">&nbsp;</div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="InvAlert">Low Inventory Alert</label></div>
              <div class="w-col w-col-5 w-col-small-5"><input class="inputfield w-input" id="InvAlert" maxlength="10" name="InvAlert" placeholder="Low Inventory Alert Amount" type="text" value="<?phpphp echo $InvAlert?>"></div>
              <div class="w-col w-col-4 w-col-small-4">&nbsp;</div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Notes">Product Notes</label></div>
              <div class="w-col w-col-9 w-col-small-9"><textarea class="inputfield w-input" id="Notes" maxlength="500" name="Notes" placeholder="Product Notes - not visible to customers"><?phpphp echo $Notes ?></textarea></div>
            </div>

            <div class="floatcleardiv"></div>
            <div class="flexrow">
              <div><a class="cancelbutton w-button" href="products.php">Cancel</a>
              </div>
              <input class="submitbutton w-button" data-wait="Please wait..." type="submit" value="Submit">
            </div>
          </form>

        </div>
        <div class="spacerblock50"></div>
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>