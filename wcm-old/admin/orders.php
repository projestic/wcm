<?php
	$PageName = "Orders";
	include_once('preamble.php');
	include_once('header.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthO']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if (empty($_REQUEST["pp"])) {$pp=100000;} else {$pp = $_REQUEST['pp'];}
	if (empty($_REQUEST["offset"])) {$offset=0;} else {$offset = $_REQUEST['offset'];}

	//BUILD SORT
        if (empty($_REQUEST["s"])) {$s = "asc"; $s2 = "desc";} else {$s = $_REQUEST["s"];}
		if (empty($_REQUEST["sort"])) {$sort = "X";} else {$sort = $_REQUEST["sort"];}
        if ($sort =="P"){$Sorter = "AddedOn $s";}
		elseif ($sort =="X"){$Sorter = "AddedOn $s";}
        else {$Sorter = "AddedOn $s";}

	//BUILD CLAUSES
		$URLphrase = "&pp=$pp";
			//$Archive = date('YmdHi', strtotime('-2 years'));
			//if ($_SESSION['UserAuth'] > 1){$dbClause  = "WHERE (1)";}
			//else {$dbClause  = "WHERE (`TransDate` > '{$Archive}')";}
		$dbClause  = "WHERE (`Orders`.`Status` != 'X')";
		$URLphrase = str_replace(" ", "+", $URLphrase); //&sort=$sort&

	//GRAB DATA
		$query1 = "
		SELECT Orders.*, 
		Clients.Fname `Fname`, Clients.Lname `Lname`
		FROM `Orders` 
		LEFT JOIN 
		`Clients` ON (Orders.CID = Clients.CID)
		$dbClause
		ORDER BY $Sorter 
		LIMIT $offset,$pp
		";
		
		
		//echo $query1;		
		

		$result1 = sqliQuery($query1);
		/*$finfo = $result1->fetch_fields();
		foreach ($finfo as $fval) {      
      printf("- %s\n", $fval->name);	
      }*/
		$r = count($result1);
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader"><div><!-- HEADER TEXT -->&nbsp;</div>
        <div class="actionblock">
          <div class="submenudropdown w-dropdown" data-delay="0">
            <div class="submenutoggle w-dropdown-toggle">
              <div class="dropdowntext"><i class="iconimage fa" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true">&#xf013;</i></div>
              <div class="submenuicon w-icon-dropdown-toggle"></div>
            </div>
            <nav class="submenulist w-dropdown-list">
				<a class="submenulink w-dropdown-link" href="order-new.php">NEW Order</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Sales Report</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Sales CSV</a>
            </nav>
          </div>
        </div>
      </div>

      <div class="outputblock">
		<!-- SEARCH -->
        <div class="searchblock">
          <div class="w-form">
            <form class="w-clearfix" name="SearchForm" method="get" action="search.js">
              <input class="searchbutton w-button fa" type="submit" value="&#xf002;">
              <input class="searchbox w-input" id="Search" maxlength="75" name="Search" placeholder="Search" type="text">
            </form>
          </div>
        </div>
		<!-- SEARCH -->

<?php	if($r) {	?>

		<table class="resultstable" id="table1">
		<thead>
		<tr>  
			<th class="resultsheader" style="text-align:left;" title="Order Date">Order Date</th>
			<th class="resultsheader" style="text-align:left;" title="Order ID">Order ID</th>
			<th class="resultsheader hidden-xs" style="text-align:left;" title="Last, First Name">Last, First Name</th>
			<th class="resultsheader" style="text-align:right;" title="Order Amount">Amount</th>
			<th class="resultsheader hidden-xs" style="text-align:right;" title="Paid Amount">Paid</th>
			<th class="resultsheader hidden-xs"  style="text-align:center;" title="Status">Status</th>
			<th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
		</tr>
		</thead>
<?php
			while ($row = mysqli_fetch_assoc($result1)){$r++; extract($row);
				print ($r % 2) ? "<tr class=\"resultsrow\" title=\"[ID: ".escape($OrderID)."] ".escape($OrderID)."\" style=\"cursor: default;\"> \n" 
					: "<tr class=\"resultsrow oddrow\" title=\"[ID: ".escape($OrderID)."] ".escape($OrderID)."\" style=\"cursor: default;\"> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".date("M d Y", date2code($AddedOn))."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".$OrderID."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:left;\">".$Lname.", ".$Fname."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:right;\">".number_format($AmntDue, 2)."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:right;\">".number_format($AmntPaid, 2)."</td> \n";
		//ORDER STATUS
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".$Status."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:center;\">  \n";
				echo "<i class=\"iconaction fa\" title=\"VIEW\" aria-hidden=\"true\" onclick=\"self.location='order-view.php?o=".$OID."';\">&#xf06e;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"EDIT\" aria-hidden=\"true\" onclick=\"self.location='order-edit.php?o=".$OID."';\" >&#xf044;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"DELETE\" aria-hidden=\"true\" onclick=\"confirmDelete('Are you sure you wish to cancel: \\nOrder ID: ".escape($OrderID)."?', 'order-delete.php?o=".$OID."');\" >&#xf014;</i> \n";
				echo "</td> \n";
			echo "</tr>\n";
			}
			echo "</table>";
				if($r < 51){
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." ORDERS</div> \n";
				}else{
					//replace with pagination
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." ORDERS</div> \n";
				}
		//No Results
			} else {
				echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\"><h4>NO ORDERS IN DATABASE</h4></div>";
			}
	?>

<!-- /results insert -->
      </div>
    </div>
  </div>
<?php	include_once('footer.php'); ?>