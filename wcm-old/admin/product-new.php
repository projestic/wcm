<?php
	$PageName = "New Product";
	include_once('preamble.php'); 


  //STEVE'S CHECKING OF PARAMETERS

//echo "Inserted Variables: " . $_SESSION['UserID'] . " AND " . $_SERVER["REMOTE_ADDR"] . "<br>";

 //END  STEVE'S CHECKING OF PARAMETERS

	//CHECK AUTHORITY
	if ($_SESSION['AuthP']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

//echo $_SERVER["REMOTE_ADDR"] . "<BR>";

	if(!empty($_POST)){
		if(isset($_POST["Category"], $_POST["ProductName"])){
			$Category	= trim($_POST["Category"]);
			$ProductName	= trim($_POST["ProductName"]);
			if(!empty($Category) && !empty($ProductName)){
				$ItemID	= "P-".strtoupper(uniqidReal("6"));
				$ProductName = trim($_POST["ProductName"]); 
				$Description = trim($_POST["Description"]); 
				$DetailLink = trim($_POST["DetailLink"]); 
				$PicLink1 = trim($_POST["PicLink1"]); 
				$PicLink2 = trim($_POST["PicLink2"]);
				$PicLink3 = trim($_POST["PicLink3"]);

				$Cost1 = sprintf("%.2f", trim($_POST["Cost1"])); 
				$Price1 = sprintf("%.2f", trim($_POST["Price1"])); 
				$Unit1 = trim($_POST["Unit1"]); 
				$Cost2 = sprintf("%.2f", trim($_POST["Cost2"])); 
				$Price2 = sprintf("%.2f", trim($_POST["Price2"])); 
				$Unit2 = trim($_POST["Unit2"]); 
				$Cost3 = sprintf("%.2f", trim($_POST["Cost3"])); 
				$Price3 = sprintf("%.2f", trim($_POST["Price3"])); 
				$Unit3 = trim($_POST["Unit3"]); 
				$Cost4 = sprintf("%.2f", trim($_POST["Cost4"])); 
				$Price4 = sprintf("%.2f", trim($_POST["Price4"])); 
				$Unit4 = trim($_POST["Unit4"]); 
				$Cost5 = sprintf("%.2f", trim($_POST["Cost5"])); 
				$Price5 = sprintf("%.2f", trim($_POST["Price5"])); 
				$Unit5 = trim($_POST["Unit5"]); 
				$Position = $_POST["Position"];
				$Status = $_POST["Status"];
				$Tax = trim($_POST["Tax"]); 
				$TaxRate = sprintf("%.4f", trim($_POST["TaxRate"])); 
				$InvAlert = trim($_POST["InvAlert"]);
				$Notes = trim($_POST["Notes"]);
				$TS = date("YmdHis");
				$Activity = "ADDED PRODUCT: ".$ItemID;

			//$Query1 = "INSERT INTO `Products` ( `ItemID`, `Category`, `Product`, `Descr`, `Detail`, `Pic1`, `Pic2`, `Pic3`, `Cost1`, `Price1`, `Unit1`, `Cost2`, `Price2`, `Unit2`, `Cost3`, `Price3`, `Unit3`, `Cost4`, `Price4`, `Unit4`, `Cost5`, `Price5`, `Unit5`, `Position`, `Status`, `Tax`, `TaxRate`, `InvAlert`, `Notes`, `AddedBy`, `AddedOn`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			$Query1 = "INSERT INTO `Products` (`ItemID`, `Category`, `Product`, `Descr`, `Detail`, `Pic1`, `Pic2`, `Pic3`, `Cost1`, `Price1`, `Unit1`, `Cost2`, `Price2`, `Unit2`, `Cost3`, `Price3`, `Unit3`, `Cost4`, `Price4`, `Unit4`, `Cost5`, `Price5`, `Unit5`, `Position`, `Status`, `Tax`, `TaxRate`, `InvAlert`, `Notes`, `AddedBy`, `AddedOn`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			//$Query2 = "INSERT INTO `Activity` (`UID`, `UIP`, `Descr`, `DateTime`) VALUES (?, ?, ?, ?)"; 

			if($insert1 = $db->prepare($Query1)){
			//$insert1->bind_param('ssssssssssiiiiisiisiisiisiissssiisis', $ItemID, $Category, $ProductName, $Description, $DetailLink, $PicLink1, $PicLink2, $PicLink3, $Cost1, $Price1, $Unit1, $Cost2, $Price2, $Unit2, $Cost3, $Price3, $Unit3, $Cost4, $Price4, $Unit4, $Cost5, $Price5, $Unit5, $Position, $Status, $Tax, $TaxRate, $InvAlert, $_SESSION['UserID'], $TS);
         $insert1->bind_param('ssssssssiisiisiisiisiissssiisis', $ItemID, $Category, $ProductName, $Description, $DetailLink, $PicLink1, $PicLink2, $PicLink3, $Cost1, $Price1, $Unit1, $Cost2, $Price2, $Unit2, $Cost3, $Price3, $Unit3, $Cost4, $Price4, $Unit4, $Cost5, $Price5, $Unit5, $Position, $Status, $Tax, $TaxRate, $InvAlert, $Notes, $_SESSION['UserID'], $TS);
				if($insert1->execute()){
					$insert1->close();
					addActivity($Activity,$TS);
					//$insert2 = $db->prepare($Query2);
					//$insert2->bind_param('isss', $_SESSION['UserID'], $_SERVER["REMOTE_ADDR"], $Activity, $TS);
					//$insert2->bind_param('i', $_SESSION['UserID']);
					//$insert2->execute();
					//$insert2->close();
					header('Location: products.php');
					die();			
				}else{
					$error = $db->errno . ' ' . $db->error;
					echo "INSERT ERROR<br><div style=\"color:gray;\">". $error;
					//var_dump($this->db->error);
					var_dump($insert1);
					$insert1->error_list;
					$insert2->error_list;
					echo "</div>";
					die();
				}
			$insert->close();
			}else{
				$error = $db->errno . ' ' . $db->error;
				echo "PREPARE ERROR<br><div style=\"color:gray;\">". $error;
				//var_dump($this->db->error);
				$insert1->error_list;
				$insert2->error_list;
				echo "</div>";
				die();
			}
//
			} else {echo "Empty Fields: $Category $ProductName"; exit;}
		} else {echo "Fields not set: $Category $ProductName"; exit;}
	}//end post

	include_once('header.php');
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader">
        <div>&nbsp;</div>
      </div>

      <div class="outputblock">
        <div class="form700px w-form">
          <div class="formtitle">Enter Product Information</div>
          <div class="forminstruction">This process puts a product in your line-up. Only asterisked (*) information is necessary. The steps to add Product in your store are: 1) Upload images to Gallery; 2) Enter Product Information [this screen], 3) Add inventory & batch.  You can add/edit images and inventory at any time. If you have additional information you'd like to communicate to your customers - e.g., terpenoids - there are two options: 1) Put all your information in 'Description' field; 2) Create a separate webpage and put the link in 'External Detail Link'. To make the product invisible to the customers, change Status to 'Inactive'.  Status will automatically change to Inactive when inventory reaches 0.</div>
          <form id="form1" name="NewProduct" enctype="multipart/form-data" method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Category">*Category</label></div>
              <div class="w-col w-col-9 w-col-small-9"><select class="inputfield w-select" id="Category" name="Category" required="required">
              <option value="">Select one...</option>
<?php
				reset($_ProdCat);
				foreach ( $_ProdCat as $key => $value ) {
					echo "<option value=\"".$key."\">".$value."</option> \n";
				} 
?>
            </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="ProductName">*Product Name</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="ProductName" maxlength="75" name="ProductName" placeholder="Enter Product Name*" required="required" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Description">Description</label></div>
              <div class="w-col w-col-9 w-col-small-9"><textarea class="inputfield w-input" id="Description" maxlength="500" name="Description" placeholder="Product Description*" required="required"></textarea>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="DetailLink">External Detail Link</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="DetailLink" maxlength="75" name="DetailLink" placeholder="https://" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="PicLink">Product Pics</label></div>
              <div class="w-col w-col-9 w-col-small-9">
			  <!-- PUT IN ROW & USE FOR EDIT SCREEN <img class="productimage" src="uploads/products/no-image.png"> -->
            <select class="inputfield w-select" id="PicLink1" name="PicLink1">
              <option value="no-image.png">Pic 1 - No Product Image</option>
<?php
				if ($handle = opendir('uploads/products')) {
    				while (false !== ($entry = readdir($handle))) {
        			if ($entry != "." && $entry != "..") {
         	  				  echo "<option>".$entry."</option>"; 
        			}
   				 }
    			closedir($handle);
				}
?>
            </select>
            <select class="inputfield w-select" id="PicLink2" name="PicLink2">
              <option value="">Pic 2 - Blank</option>
<?php
				if ($handle = opendir('uploads/products')) {
    				while (false !== ($entry = readdir($handle))) {
        			if ($entry != "." && $entry != "..") {
         	  				  echo "<option>".$entry."</option>"; 
        			}
   				 }
    			closedir($handle);
				}
?>
            </select>
            <select class="inputfield w-select" id="PicLink3" name="PicLink3">
              <option value="">Pic 3 - Blank</option>
<?php
				if ($handle = opendir('uploads/products')) {
    				while (false !== ($entry = readdir($handle))) {
        			if ($entry != "." && $entry != "..") {
         	  				  echo "<option>".$entry."</option>"; 
        			}
   				 }
    			closedir($handle);
				}
?>
            </select>
                <div class="forminstruction">Load product images in your Gallery, they will then appear in the selection box.
                  <br>A default "no image" will appear if you don't associate a picture with this product.
                  <br>You can change the product image at any time through the EDIT screen.
				  <br>{coming soon: multiple images for products!}</div>
              </div>
            </div>
<!-- 
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Unit9">Characteristics</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit9" name="Unit9">
                  <option value="">Select one...</option>
                </select>			  
			  </div>
              <div class="w-col w-col-3 w-col-small-3">
			  </div>
              <div class="w-col w-col-3 w-col-small-3"> 
			  </div>
            </div>
-->

<!--  PRODUCT PRICING SECTION -->
            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost1">*Product Price Point #1</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost1" maxlength="10" name="Cost1" placeholder="Cost / Unit*" required="required" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price1" maxlength="10" name="Price1" placeholder="Price / Unit*" required="required" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit1" name="Unit1" required="required">
                  <option value="">Select one...*</option>
<?php
					reset($_ProdUnit);
					foreach ($_ProdUnit as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						echo ">".$value."</option> \n";
					} 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost2">Product Price Point #2</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost2" maxlength="10" name="Cost2" placeholder="Cost / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price2" maxlength="10" name="Price2" placeholder="Price / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit2" name="Unit2">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					foreach ($_ProdUnit as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						echo ">".$value."</option> \n";
					} 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost3">Product Price Point #3</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost3" maxlength="10" name="Cost3" placeholder="Cost / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price3" maxlength="10" name="Price3" placeholder="Price / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit3" name="Unit3">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					foreach ($_ProdUnit as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						echo ">".$value."</option> \n";
					} 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost4">Product Price Point #4</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost4" maxlength="10" name="Cost4" placeholder="Cost / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price4" maxlength="10" name="Price4" placeholder="Price / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit4" name="Unit4">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					foreach ($_ProdUnit as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						echo ">".$value."</option> \n";
					} 
?>
                </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Cost5">Product Price Point #5</label></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Cost5" maxlength="10" name="Cost5" placeholder="Cost / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="Price5" maxlength="10" name="Price5" placeholder="Price / Unit" type="text"></div>
              <div class="w-col w-col-3 w-col-small-3">
                <select class="inputfield w-select" id="Unit5" name="Unit5">
                  <option value="">Select one...</option>
<?php
					reset($_ProdUnit);
					foreach ($_ProdUnit as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						echo ">".$value."</option> \n";
					} 
?>
                </select>
              </div>
            </div>
<!--  /PRODUCT PRICING SECTION -->

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Tax">*Tax Method &amp; Rate(s)</label></div>
              <div class="w-col w-col-6 w-col-small-6">
                <select class="inputfield w-select" id="Tax" name="Tax" required="required">
                  <option value="">Select one...</option>
                  <option value="N">None</option>
                  <option value="S">Simple %</option>
                  <option value="T" disabled="disabled">Tax Table</option>
                </select>
              </div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="TaxRate" maxlength="10" name="TaxRate" placeholder="Tax Rate" type="text"></div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Position">Position</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Position1" name="Position" type="radio" value="F">
                  <label class="fieldlabel w-form-label" for="Position1">Featured</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Position2" name="Position" type="radio" value="R" checked="checked">
                  <label class="fieldlabel w-form-label" for="Position2">Regular</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">&nbsp;</div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Status">Product Status</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Status1" name="Status" type="radio" value="A" checked="checked">
                  <label class="fieldlabel w-form-label" for="Status1">Active</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Status2" name="Status" type="radio" value="I">
                  <label class="fieldlabel w-form-label" for="Status2">Inactive</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">&nbsp;</div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="InvAlert">Low Inventory Alert</label></div>
              <div class="w-col w-col-5 w-col-small-5"><input class="inputfield w-input" id="InvAlert" maxlength="10" name="InvAlert" placeholder="Low Inventory Alert Amount" type="text"></div>
              <div class="w-col w-col-4 w-col-small-4">&nbsp;</div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Notes">Product Notes</label></div>
              <div class="w-col w-col-9 w-col-small-9"><textarea class="inputfield w-input" id="Notes" maxlength="500" name="Notes" placeholder="Product Notes - not visible to customers"></textarea></div>
            </div>

            <div class="floatcleardiv"></div>
            <div class="flexrow">
              <div><a class="cancelbutton w-button" href="products.php">Cancel</a>
              </div>
              <input class="submitbutton w-button" data-wait="Please wait..." type="submit" value="Submit">
            </div>
          </form>

        </div>
        <div class="spacerblock50"></div>
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>