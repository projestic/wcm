<?php
	$PageName = "Clients";
	include_once('preamble.php');
	include_once('header.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthC']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if (empty($_REQUEST["pp"])) {$pp=100000;} else {$pp = $_REQUEST['pp'];}
	if (empty($_REQUEST["offset"])) {$offset=0;} else {$offset = $_REQUEST['offset'];}

	//BUILD SORT
        if (empty($_REQUEST["s"])) {$s = "asc"; $s2 = "desc";} else {$s = $_REQUEST["s"];}
		if (empty($_REQUEST["sort"])) {$sort = "X";} else {$sort = $_REQUEST["sort"];}
        if ($sort =="P"){$Sorter = "PatientID $s, Lname $s, FName $s";}
		elseif ($sort =="X"){$Sorter = "Lname $s, Fname $s";}
        else {$Sorter = "Lname $s, Fname $s";}

	//BUILD CLAUSES
		$URLphrase = "&pp=$pp";
			$Archive = date('YmdHi', strtotime('-2 years'));
			//if ($_SESSION['UserAuth'] > 1){$dbClause  = "WHERE (1)";}
			//else {$dbClause  = "WHERE (`LastActivity` > '{$Archive}')";}
		$dbClause  = "WHERE (`Status` != 'X')";
		$URLphrase = str_replace(" ", "+", $URLphrase); //&sort=$sort&

	//GRAB DATA
		$query1 = "
			SELECT * 
			FROM `Clients` 
			$dbClause 
			ORDER BY $Sorter
			LIMIT $offset,$pp
			";
		$result1 = sqliQuery($query1);
		$r = count($result1);

		//REPLACE WITH CALCULATIONS
			//$GrossRev = mt_rand(1,1000);  //db reference: $SalesCount
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader"><div><!-- HEADER TEXT -->&nbsp;</div>
        <div class="actionblock">
          <div class="submenudropdown w-dropdown" data-delay="0">
            <div class="submenutoggle w-dropdown-toggle">
              <div class="dropdowntext"><i class="iconimage fa" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true">&#xf013;</i></div>
              <div class="submenuicon w-icon-dropdown-toggle"></div>
            </div>
            <nav class="submenulist w-dropdown-list">
				<a class="submenulink w-dropdown-link" href="client-new.php">NEW Client</a>
				<a class="submenulink w-dropdown-link" href="client-upload.php">Client Doc Upload</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Client Report</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Client CSV</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Bulk Upload Clients</a>
            </nav>
          </div>
        </div>
      </div>

      <div class="outputblock">
		<!-- SEARCH -->
        <div class="searchblock">
          <div class="w-form">
            <form class="w-clearfix" name="SearchForm" method="get" action="search.js">
              <input class="searchbutton w-button fa" type="submit" value="&#xf002;">
              <input class="searchbox w-input" id="Search" maxlength="75" name="Search" placeholder="Search" type="text">
            </form>
          </div>
        </div>
		<!-- SEARCH -->

<?php	if($r){	?>

		<table class="resultstable" id="table1">
		<thead>
		<tr>  
			<th class="resultsheader" style="text-align:left;" title="Client #">Client #</th>
			<th class="resultsheader" style="text-align:left;" title="Last, First Name">Last, First Name</th>
			<th class="resultsheader hidden-xs" style="text-align:left;" title="Zip Code">Zip Code</th>
			<th class="resultsheader" style="text-align:center;" title="Email, Phone">Contact</th>
			<th class="resultsheader hidden-xs" style="text-align:right;" title="Gross $ Spent">Gross $</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Date Joined">Joined</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Last Activity">Last Activity</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Status">Status</th>
			<th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
		</tr>
		</thead>
<?php
			while ($row = mysqli_fetch_assoc($result1)){$r++; extract($row);
        $GrossRev = mt_rand(1,1000);
				print ($r % 2) ? "<tr class=\"resultsrow\" title=\"[ID: ".escape($ClientID)."] ".escape($Fname)."\" style=\"cursor: default;\"> \n" 
					: "<tr class=\"resultsrow oddrow\" title=\"[ID: ".escape($ClientID)."] ".escape($Fname)."\" style=\"cursor: default;\"> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($ClientID)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($Lname).", ".escape($Fname)."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:left;\">".escape($Zip)."</td> \n";
				/* if($Birthdate){
					$Month = substr($Birthdate, 2, 2); $BMonth = $_Months[$Month];
					$BDay = substr($Birthdate, 0, 2);
					echo "<td class=\"hidden-xs\">".$BMonth." ".$BDay."</td> \n";}
					else{echo "<td class=\"hidden-xs\">--</td> \n";}
				*/
				echo "<td class=\"resultscell\" style=\"text-align:center;\"> ";
				if($Email){
					echo "<a href=\"mailto:".escape($Email)."\" title=\"Email ".escape($Fname)."\" style=\"margin: 1px 5px;\"><i class=\"iconimage fa\" aria-hidden=\"true\">&#xf003;</i></a> \n";}
					else{echo "No Email";}
				if($Phone){
					echo "<a href=\"tel:".escape($Phone)."\" title=\"Call ".escape($Fname)."\" style=\"margin: 1px 5px;\"><i class=\"iconimage fa\" aria-hidden=\"true\">&#xf095;</i></a> \n";}
					else{echo "No Phone";}
				echo "</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:right;\">".number_format($GrossRev, 2)."</td> \n";
					if($AddedOn){$Joined = date("M d Y", date2code(escape($AddedOn)));}else {$Joined = "--";}
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".$Joined."</td> \n";
					if($LastActivity){$Active = date("M d Y", date2code(escape($LastActivity)));}else {$Active = "--";}
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".$Active."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".escape($Status)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:center;\">  \n";
				echo "<i class=\"iconaction fa\" title=\"VIEW\" aria-hidden=\"true\" onclick=\"self.location='client-view.php?c=".$CID."';\">&#xf06e;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"EDIT\" aria-hidden=\"true\" onclick=\"self.location='client-edit.php?c=".$CID."';\" >&#xf044;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"DELETE\" aria-hidden=\"true\" onclick=\"confirmDelete('Are you sure you wish to delete: \\n[ID: ".escape($ClientID)."] ".escape($Fname)."?', 'client-delete.php?c=".$CID."');\" >&#xf014;</i> \n";
				echo "</td> \n";
				echo "</tr>\n";
			}
			echo "</table>";
				if($r < 51){
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." CLIENTS</div> \n";
				}else{
					//replace with pagination
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." CLIENTS</div> \n";
				}
		//No Results
			}else{
				echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\"><h4>NO CLIENTS IN DATABASE</h4></div>";
			}
	?>

<!-- /results insert -->
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>
