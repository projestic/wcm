<!DOCTYPE html>
<!--  Last Published: Thu Jan 12 2017 00:53:42 GMT+0000 (UTC)  -->
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $PageName; ?></title>
  <meta name="robots" content="noindex">
  <meta content="<?php echo $PageName; ?>" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="../../public/css/admin/normalize.css" rel="stylesheet" type="text/css">
  <link href="../../public/css/admin/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dashboard.css" rel="stylesheet" type="text/css">
  <link href="../../public/css/admin/secondary.css" rel="stylesheet" type="text/css">
  <link href="../../public/css/admin/robocrop.min.css" rel="stylesheet" type="text/css" >
  <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" -->
	<!-- link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" rel="stylesheet" -->
   
  <script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
    WebFont.load({
      google: {
        families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]
      }
    });
  </script>
  <script src="../../public/js/admin/modernizr.js" type="text/javascript"></script>
  <link href="../../public/images/admin/StoreLogo_32px.png" rel="shortcut icon" type="image/x-icon">
  <link href="../../public/images/admin/StoreLogo_256px.png" rel="apple-touch-icon">
</head>
<body class="body">

  <div class="section">
    <div class="headerblock">
      <div class="headernav w-nav" data-animation="over-right" data-collapse="all" data-doc-height="1" data-duration="400">
        <div class="headerrow w-row">
          <div class="w-clearfix w-col w-col-1 w-col-medium-1 w-col-small-4 w-col-tiny-4">
            <a class="brandlink w-nav-brand" href="<?php print $StoreURL;?>" target="_blank"><img class="brandimage" src="../../public/images/admin/brandimage.png" width="36"></a>
          </div>
          <div class="w-col w-col-10 w-col-medium-10 w-col-small-4 w-col-tiny-4">
            <div class="bizname"><?php print $_SESSION['BizName']; ?></div>
			<!-- Welcome <?php print $_SESSION['UserName'] ?>
				<?php echo date("M d Y g:i A", $Tday); ?>
			//-->
          </div>
          <div class="w-clearfix w-col w-col-1 w-col-medium-1 w-col-small-4 w-col-tiny-4">
		  	<?php include('menu.php'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /HEADER SECTION -->
