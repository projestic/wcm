<?php
	$PageName = "Product Pic Upload";
	include_once('preamble.php'); 
?>

<?php
	//CHECK AUTHORITY
	if ($_SESSION['AuthP']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}
	
	include_once('header.php'); 
?>
    <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader">
        <div>&nbsp;</div>
      </div>

      <div class="outputblock">
        <div class="form700px w-form">
          <div class="formtitle">Select Product Image</div>
          <div class="forminstruction">This process adds an image to your product image gallery. Asterisked (*) information is necessary. You can add as many images to your gallery as you like. Images can then be assigned to a product in the Product Add or Product Edit screens.</div>
          
<!--		<form action="#" enctype="multipart/form-data" method="post">-->
            <div class="formrow w-row" style="margin-bottom:10px;">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="picture">*Product Image File</label></div>
              	<div class="w-col w-col-9 w-col-small-9">
              		<div id="demo-profile">
								<div class="crop-element" data-watermark="main" data-name="prodpic"  data-upload="submit_ROBOCROP.php" data-crop="=250,=250" data-crop-required="true">	
									<img/>
									<input type="file"/>
									<button class="btn btn-sm btn-default edit"><i class="fa fa-edit"></i></button>
								</div>
						</div>
				  </div>
			   </div>
				<div class="formrow w-row" style="margin-bottom:10px;">
				  <div class="w-col w-col-4 w-col-small-4"></div>
				</div>
				
			   <div class="floatcleardiv"></div>
            <div class="flexrow">
              <div><a class="cancelbutton w-button" href="products.php">Cancel</a>
              </div>
              <!--<input class="submitbutton w-button"  data-wait="Please wait..." type="submit" name="submit" value="Submit">-->
            </div>
				
<!--			</form>	-->
					
            </div>

        </div>
        <div class="spacerblock50"></div>
      </div>
    </div>
  </div>
<?php	include_once('footer.php'); ?>

<script src="assets/robocrop/robocrop.js"></script>
<script src="assets/robocrop/robocrop-custom.js"></script>

<script type="text/javascript">
robocrop.watermarks = {
    main: {
        file: 'images/mar-wm.png',
        opacity: .3,
        position: "bottom-right",
        margin_top: 0,
        margin_right: 5,
        margin_bottom: 5,
        margin_left: 0
    }
};
</script>