<?php
	$PageName = "New Client";
	include_once('preamble.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthC']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if(!empty($_POST)){
		if(isset($_POST['FName'])){
			$Fname = trim($_POST["FName"]);
			if(!empty($Fname)){
			//test for birthdate format, min age
				$AuthAge = strtotime("- {$MinAge} years");
				$Birthdate  = trim($_POST["BYr"]); //BYr

				$ClientID	= "C-".strtoupper(uniqidReal("7")); 
				$Lname = trim($_POST["LName"]); 
				$Addr1 = trim($_POST["Addr1"]);
				$Addr2 = trim($_POST["Addr2"]);
				$City = trim($_POST["City"]);
				$State = trim($_POST["State"]);
				$Zip = trim($_POST["Zip"]);
				$Email = trim($_POST["Email"]);
				$Phone = trim($_POST["Phone"]);
				$OK4TXT = trim($_POST["OK4TXT"]);
				$Referred  = trim($_POST["Referred"]);
				$IDType = trim($_POST["IDType"]);
				$IDNum = trim($_POST["IDNum"]);
				//$IDPic = trim($_POST["IDPic"]); //uploaded in other section
				$CertType = trim($_POST["CertType"]);
				$CertNum = trim($_POST["CertNum"]);
				//$CertPic = trim($_POST["CertPic"]); //uploaded in other section
				$Notes = trim($_POST["Notes"]);
				$TS = date("YmdHis");
				$Activity = "ADDED Client: ".$ClientID;
//
			$Query1 = "INSERT INTO `Clients` ( `ClientID`, `Fname`, `Lname`, `Addr1`, `Addr2`, `City`, `State`, `Zip`, `Birthdate`, `Email`, `Phone`, `OK4TXT`, `Referred`, `IDType`, `IDNum`, `CertType`, `CertNum`, `Notes`, `AddedBy`, `AddedOn`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			$Query2 = "INSERT INTO `Activity` (`UID`, `UIP`, `Descr`, `DateTime`) VALUES (?, ?, ?, ?)"; 

			if($insert1 = $db->prepare($Query1)){
			$insert1->bind_param('ssssssssssssssssssis', $ClientID, $Fname, $Lname, $Addr1, $Addr2, $City, $State, $Zip, $Birthdate, $Email, $Phone, $OK4TXT, $Referred, $IDType, $IDNum, $CertType, $CertNum, $Notes, $_SESSION['UserID'], $TS);

				if($insert1->execute()){
					$insert1->close();
					$insert2 = $db->prepare($Query2);
					$insert2->bind_param('isss', $_SESSION['UserID'], $_SERVER["REMOTE_ADDR"], $Activity, $TS);
					$insert2->execute();
					$insert2->close();
					header('Location: clients.php');
					die();
				}else{
					$error = $db->errno . ' ' . $db->error;
					echo "DATABASE OFFLINE<br><div style=\"color:white;\">". $error;
					//var_dump($this->db->error);
					$insert1->error_list;
					$insert2->error_list;
					echo "</div>";
					die();
				}
			$insert->close();
			}else{
				$error = $db->errno . ' ' . $db->error;
				echo "DATABASE OFFLINE<br><div style=\"color:white;\">". $error;
				//var_dump($this->db->error);
				$insert1->error_list;
				$insert2->error_list;
				echo "</div>";
				die();
			}
//
			} else {echo "Empty Fields:"; exit;}
		} else {echo "Fields not set:"; exit;}
	}//end post

	include_once('header.php');
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader">
        <div>&nbsp;</div>
      </div>

      <div class="outputblock">
        <div class="form700px w-form">
          <div class="formtitle">Enter Client Information</div>
          <div class="forminstruction">This process adds a new Client to your system. Only asterisked (*) information is necessary. You can add/edit documents at any time.</div>
          <form id="form1" name="NewClient" enctype="multipart/form-data" method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="FName">*First Name</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="FName" maxlength="35" name="FName" placeholder="Enter Clients First Name*" required="required" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="LName">Last Name</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="FName" maxlength="35" name="LName" placeholder="Enter Clients Last Name" required="required" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Addr1">Address</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="Addr1" maxlength="75" name="Addr1" placeholder="Delivery Address" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Addr2">&nbsp;</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="Addr2" maxlength="75" name="Addr2" placeholder="Suite, Apt, Special Instructions" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="City">City</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="City" maxlength="35" name="City" placeholder="Delivery City" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="State">State / Zip</label></div>
              <div class="w-col w-col-9 w-col-small-9">
			  <select class="inputfield w-input" id="State" name="State">
			  <option value="">Select one...</option>
<?php
					reset($_States);
					foreach ($_States as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						if($key == "CA"){ echo "selected";}
						echo ">".$value."</option> \n";
					} 
?>
			  </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Email">Email</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="Email" maxlength="75" name="Email" placeholder="Clients Email" type="email">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Phone">Phone</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="Phone" maxlength="35" name="Phone" placeholder="310-555-1212" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="OK4TXT">Okay to Text</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="OK4TXT1" name="OK4TXT" type="radio" value="Y" checked="checked">
                  <label class="fieldlabel w-form-label" for="OK4TXT1">Yes</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="OK4TXT2" name="OK4TXT" type="radio" value="N">
                  <label class="fieldlabel w-form-label" for="OK4TXT2">No</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">&nbsp;</div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="BYr">*Birthdate</label></div>
              <div class="w-col w-col-9 w-col-small-9">
			  <select class="inputfield w-input" id="BYr" name="BYr"  required="required">
			  <option value="">Select one...</option>
<?php
					reset($_Months);
					foreach ($_Months as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						if($key == "06"){echo "selected";}
						echo ">".$value."</option> \n";
					} 
?>

			  </select>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3">Uploads</div>
              <div class="w-col w-col-9 w-col-small-9">
                <div class="forminstruction">Upload scans or pics of Clients ID card, Medical Certificate, and/or any other documents using the Client Document Upload function.  It will automatically associate documents with the client.</div>
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="IDType">*ID Type</label></div>
              <div class="w-col w-col-6 w-col-small-6">
            <select class="inputfield w-select" id="IDType" name="IDType" required="required">
              <option value="">Select one...</option>
<?php
					reset($_IDTypes);
					foreach ($_IDTypes as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						echo ">".$value."</option> \n";
					} 
?>
            </select>
              </div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="IDNum" maxlength="35" name="IDNum" placeholder="ID Number" type="text"></div>
              </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="CertType">Cert Type</label></div>
              <div class="w-col w-col-6 w-col-small-6">
            <select class="inputfield w-select" id="CertType" name="CertType">
              <option value="">Select one...</option>
<?php
					reset($_CertTypes);
					foreach ($_CertTypes as $key=>$value ) {
						echo "<option value=\"".$key."\" ";
						echo ">".$value."</option> \n";
					} 
?>
            </select>
              </div>
              <div class="w-col w-col-3 w-col-small-3"><input class="fieldtextright inputfield w-input" id="CertNum" maxlength="35" name="CertNum" placeholder="Medical Certificate Number" type="text"></div>
              </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Referred">Referred By</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="Referred" maxlength="35" name="Referred" placeholder="Client#, Google, Weed Maps, etc." type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Notes">Client Notes</label></div>
              <div class="w-col w-col-9 w-col-small-9"><textarea class="inputfield w-input" id="Notes" maxlength="500" name="Notes" placeholder="Client Notes - not visible to customers"></textarea></div>
            </div>

            <div class="floatcleardiv"></div>
            <div class="flexrow">
              <div><a class="cancelbutton w-button" href="clients.php">Cancel</a>
              </div>
              <input class="submitbutton w-button" type="submit" value="Submit">
            </div>
          </form>

        </div>
        <div class="spacerblock50"></div>
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>