<?php
	$PageName = "New Vendor";
	include_once('preamble.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthV']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if(!empty($_POST)){
		if(isset($_POST['Vendor'])){
			$Vendor		= trim($_POST['Vendor']);
			if(!empty($Vendor)){
				$VendorID	= "V-".strtoupper(uniqidReal("6"));
				$CName = trim($_POST["Fname"]). "~~" . trim($_POST["Lname"]); 
				//$Fname = trim($_POST["Fname"]); 
				//$Lname = trim($_POST["Lname"]); 
				$Zip = trim($_POST["Zip"]); 
				//$GrossRev = trim($_POST["GrossRev"]); 
				//$LastActivity = trim($_POST["LastActivity"]); 
				$Status = trim($_POST["Status"]); 
				$Phone = trim($_POST["Phone"]); 
				$Email = trim($_POST["Email"]); 
				//$Notes = trim($_POST["Notes"]); 
				$TS = date("YmdHis");
				$Activity = "ADDED VENDOR: ".$VendorID;
//

			//$Query1 = "INSERT INTO `Vendors` ( `VendorID`, `Vendor`, `Lname`, `Fname`, `Zip`, `Status`, `Phone`, `Email`, `Notes`, `AddedBy`, `AddedOn`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			$Query1 = "INSERT INTO `Vendors` ( `VendorID`, `Vendor`, `CName`, `Zip`, `Addr1`, `Addr2`, `Status`, `AddedBy`, `AddedOn`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

			if($insert1 = $db->prepare($Query1)){
			//$insert1->bind_param('sssssssssis', $VendorID, $Vendor, $Lname, $Fname, $Zip, $Status, $Phone, $Email, $Notes, $_SESSION['UserID'], $TS);
			$insert1->bind_param('sssssssis', $VendorID, $Vendor, $CName, $Zip, $Phone, $Email, $Status, $_SESSION['UserID'], $TS);

				if($insert1->execute()){
					$insert1->close();
					addActivity($Activity,$TS);
					header('Location: vendors.php');
					die();
				}else{
					$error = $db->errno . ' ' . $db->error;
					echo "DATABASE OFFLINE<br><div style=\"color:white;\">". $error;
					//var_dump($this->db->error);
					$insert1->error_list;
					$insert2->error_list;
					echo "</div>";
					die();
				}
			$insert->close();
			}else{
				$error = $db->errno . ' ' . $db->error;
				echo "DATABASE OFFLINE<br><div style=\"color:white;\">". $error;
				//var_dump($this->db->error);
				$insert1->error_list;
				$insert2->error_list;
				echo "</div>";
				die();
			}
//
			} else {echo "Empty Fields: $Vendor"; exit;}
		} else {echo "Fields not set: $Vendor"; exit;}
	}//end post

	include_once('header.php');
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader">
        <div>&nbsp;</div>
      </div>

      <div class="outputblock">
        <div class="form700px w-form">
          <div class="formtitle">Enter Vendor Information</div>
          <div class="forminstruction">This process adds a vendor to your list of suppliers. Only asterisked (*) information is necessary.</div>
          <form id="form1" name="NewVendor" enctype="multipart/form-data" method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">


            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Vendor">*Vendor Name</label></div>
              <div class="w-col w-col-9 w-col-small-9"><input class="inputfield w-input" id="Vendor" maxlength="75" name="Vendor" placeholder="Enter Vendor Business Name*" required="required" type="text">
              </div>
            </div>

            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Lname">Contact Name</label></div>
              <div class="w-col w-col-4 w-col-small-4"><input class="inputfield w-input" id="Fname" maxlength="500" name="Fname" placeholder="First Name" type="text"></div>
              <div class="w-col w-col-5 w-col-small-5"><input class="inputfield w-input" id="Lname" maxlength="500" name="Lname" placeholder="Last Name" type="text"></div>
            </div>
            
            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Email">Contact Info</label></div>
              <div class="w-col w-col-4 w-col-small-4"><input class="inputfield w-input" id="Email" maxlength="500" name="Email" placeholder="Email Address" type="text"></div>
              <div class="w-col w-col-5 w-col-small-5"><input class="inputfield w-input" id="Phone" maxlength="500" name="Phone" placeholder="Phone Number" type="text"></div>
            </div>
            
            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"></div>
              <div class="w-col w-col-4 w-col-small-4"><input class="inputfield w-input" id="Zip" maxlength="500" name="Zip" placeholder="Zip Code" type="text"></div>
              <div class="w-col w-col-5 w-col-small-5"></div>
            </div>
            
            
     <!--       <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="GrossRev">Activity</label></div>
              <div class="w-col w-col-4 w-col-small-4"><input class="inputfield w-input" id="GrossRev" maxlength="500" name="GrossRev" placeholder="Gross Sales through this Vendor" type="text"></div>
              <div class="w-col w-col-5 w-col-small-5"><input class="inputfield w-input" id="LastActivity" maxlength="500" name="LastActivity" placeholder="Date of Last Activity with this Vendor" type="text"></div>
            </div>            -->


            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Status">Vendor Status</label></div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Status1" name="Status" type="radio" value="A" checked="checked">
                  <label class="fieldlabel w-form-label" for="Status1">Active</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">
                <div class="w-clearfix w-radio">
                  <input class="radio w-radio-input" id="Status2" name="Status" type="radio" value="I">
                  <label class="fieldlabel w-form-label" for="Status2">Inactive</label>
                </div>
              </div>
              <div class="w-col w-col-3 w-col-small-3">&nbsp;</div>
            </div>


            <div class="formrow w-row">
              <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="Notes">Vendor Notes</label></div>
              <div class="w-col w-col-9 w-col-small-9"><textarea class="inputfield w-input" id="Notes" maxlength="500" name="Notes" placeholder="Notes about this Vendor"></textarea></div>
            </div>

            <div class="floatcleardiv"></div>
            <div class="flexrow">
              <div><a class="cancelbutton w-button" href="products.php">Cancel</a>
              </div>
              <input class="submitbutton w-button" data-wait="Please wait..." type="submit" value="Submit">
            </div>
          </form>

        </div>
        <div class="spacerblock50"></div>
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>