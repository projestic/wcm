<?php
	$PageName = "Workflow";
	include_once('preamble.php'); 
	include_once('header.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthW']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

?>
  <div class="maincontent section">
  <h2 class="pagename">Workflow</h2>

    <div class="w-row">
      <div class="w-col w-col-3">
	<!-- PENDING ORDERS CARD -->
      <div class="cardwrapper kanban salmon">
        <div class="cardtitle">Pending</div>
        <div class="carddatablock">
<?php
	//DEMO PURPOSES
	$aTime = strtotime("now");
	for($a=0; $a<13; $a++){
		$aSecs = mt_rand(7500,17000); 
		$aDate = strtotime("- {$aSecs} seconds", $aTime); 
		// TEST echo "<br>".$a." ".date("Md g:i", $aTime)." / ".date("Md g:i", $aDate)." / ".$aSecs;
		echo "<div class=\"carddatatext\">".date("M d h:ia", $aDate)."&nbsp;|&nbsp;Order # S-".strtoupper(uniqidReal("9"))."</div> \n";
		$aTime = $aDate;
	}
?>
        </div>
        <div class="reportcardsubtext"><strong>TOTAL: <?php print $a;?></strong></div>
      </div>

      </div>
      <div class="w-col w-col-3">
	<!-- PROCESSED ORDERS CARD -->
      <div class="cardwrapper kanban orange">
        <div class="cardtitle">Processed</div>
        <div class="carddatablock">
<?php
	//DEMO PURPOSES
	$bTime = strtotime("now");
	for($b=0; $b<7; $b++){
		$bSecs = mt_rand(7500,17000); 
		$bDate = strtotime("- {$bSecs} seconds", $bTime); 
		echo "<div class=\"carddatatext\">".date("M d h:ia", $bDate)."&nbsp;|&nbsp;Order # S-".strtoupper(uniqidReal("9"))."</div> \n";
		$bTime = $bDate;
	}

?>
        </div>
        <div class="reportcardsubtext"><strong>TOTAL: <?php print $b;?></strong></div>
      </div>

      </div>
      <div class="w-col w-col-3">
	<!-- PENDING PICKUP/DELIVERY CARD -->
      <div class="cardwrapper kanban skyblue">
        <div class="cardtitle">Pickup/Delivery</div>
        <div class="carddatablock">
<?php
	//DEMO PURPOSES
	$cTime = strtotime("now");
	for($c=0; $c<10; $c++){
		$cSecs = mt_rand(7500,17000); 
		$cDate = strtotime("- {$cSecs} seconds", $cTime); 
		echo "<div class=\"carddatatext\">".date("M d h:ia", $cDate)."&nbsp;|&nbsp;Order # S-".strtoupper(uniqidReal("9"))."</div> \n";
		$cTime = $cDate;
	}
?>
        </div>
        <div class="reportcardsubtext"><strong>TOTAL: <?php print $c;?></strong></div>
      </div>

      </div>
      <div class="w-col w-col-3">
	<!-- COMPLETED ORDERS CARD -->
      <div class="aquamarine cardwrapper kanban">
        <div class="cardtitle">Completed</div>
        <div class="carddatablock">
<?php
	//DEMO PURPOSES
	$dTime = strtotime("now");
	for($d=0; $d<26; $d++){
		$dSecs = mt_rand(7500,17000); 
		$dDate = strtotime("- {$dSecs} seconds", $dTime); 
		echo "<div class=\"carddatatext\">".date("M d h:ia", $dDate)."&nbsp;|&nbsp;Order # S-".strtoupper(uniqidReal("9"))."</div> \n";
		$dTime = $dDate;
	}
?>
        </div>
        <div class="reportcardsubtext"><strong>LAST 25 ORDERS</strong></div>
      </div>

      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>