<?php
	$PageName = "Delivery View";
	include_once('preamble.php'); 
	include_once('header.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthD']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	// TO BE WRITTEN
?>
  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader"><div><!-- HEADER TEXT -->&nbsp;</div>
        <div class="actionblock">
        </div>
      </div>

      <div class="outputblock">
		<!-- SEARCH -->
        <div class="searchblock">
          <div class="w-form">
            <form class="w-clearfix" name="SearchForm" method="get" action="search.js">
              <input class="searchbutton w-button fa" type="submit" value="&#xf002;">
              <input class="searchbox w-input" id="Search" maxlength="75" name="Search" placeholder="Search" type="text">
            </form>
          </div>
        </div>
		<!-- SEARCH -->

      <div class="outputblock"><h4><?php echo $PageName; ?> Offline for Demo</h4></div>

      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>