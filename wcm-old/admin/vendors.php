<?php
	$PageName = "Vendors";
	include_once('preamble.php');
	include_once('header.php'); 
	
	//echo $db->host_info;

	//CHECK AUTHORITY
	if ($_SESSION['AuthV']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if (empty($_REQUEST["pp"])) {$pp=100000;} else {$pp = $_REQUEST['pp'];}
	if (empty($_REQUEST["offset"])) {$offset=0;} else {$offset = $_REQUEST['offset'];}

	//BUILD SORT
        if (empty($_REQUEST["s"])) {$s = "asc"; $s2 = "desc";} else {$s = $_REQUEST["s"];}
		if (empty($_REQUEST["sort"])) {$sort = "X";} else {$sort = $_REQUEST["sort"];}
        if ($sort =="V"){$Sorter = "Vendor $s, Zip $s";}
		elseif ($sort =="Z"){$Sorter = "Zip $s, Vendor $s";}
        else {$Sorter = "Vendor $s, Zip $s";}

	//BUILD CLAUSES
		$URLphrase = "&pp=$pp";
		$Archive = date('YmdHi', strtotime('-2 years'));
			//if ($_SESSION['UserAuth'] > 1){$dbClause  = "WHERE (1)";}
			//else {$dbClause  = "WHERE (`LastActivity` > '{$Archive}')";}
		$dbClause  = "WHERE (`Status` != 'X')";
		$URLphrase = str_replace(" ", "+", $URLphrase); //&sort=$sort&

	//GRAB DATA
		$query1 = "
			SELECT * 
			FROM `Vendors` 
			$dbClause 
			ORDER BY $Sorter
			LIMIT $offset,$pp
			"; 
			//$query1 = "Select * from Vendors";
			//echo $query1;
		$result1 = sqliQuery($query1);
		//DEBUG: RETURN COLUMN NAMES
		/*
		$finfo = $result1->fetch_field_direct(11);
      printf("Name:     %s\n", $finfo->name);	
      echo "wtf";*/
      
		$r = count($result1); 
		


		//REPLACE WITH CALCULATIONS
			$GrossRev = mt_rand(1,1000);  //db reference: $SalesCount
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader"><div><!-- HEADER TEXT -->&nbsp;</div>
        <div class="actionblock">
          <div class="submenudropdown w-dropdown" data-delay="0">
            <div class="submenutoggle w-dropdown-toggle">
              <div class="dropdowntext"><i class="iconimage fa" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true">&#xf013;</i></div>
              <div class="submenuicon w-icon-dropdown-toggle"></div>
            </div>
            <nav class="submenulist w-dropdown-list">
				<a class="submenulink w-dropdown-link" href="vendor-new.php">NEW Vendor</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Vendor Report</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Vendor CSV</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Bulk Upload Vendors</a>
            </nav>
          </div>
        </div>
      </div>

      <div class="outputblock">
		<!-- SEARCH -->
        <div class="searchblock">
          <div class="w-form">
            <form class="w-clearfix" name="SearchForm" method="get" action="search.js">
              <input class="searchbutton w-button fa" type="submit" value="&#xf002;">
              <input class="searchbox w-input" id="Search" maxlength="75" name="Search" placeholder="Search" type="text">
            </form>
          </div>
        </div>
		<!-- SEARCH -->

<?php	if($r){	?>

		<table class="resultstable" id="table1">
		<thead>
		<tr>  
			<th class="resultsheader" style="text-align:left;" title="Vendor #">Vendor #</th>
			<th class="resultsheader" style="text-align:left;" title="Vendor">Vendor Name</th>
			<th class="resultsheader" style="text-align:left;" title="Contact Name">Representative</th>
			<th class="resultsheader hidden-xs" style="text-align:left;" title="Zip Code">Zip Code</th>
			<th class="resultsheader" style="text-align:center;" title="Email, Phone">Contact</th>
			<th class="resultsheader hidden-xs" style="text-align:right;" title="Gross $ Spent">Gross $</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Last Activity">Last Activity</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Status">Status</th>
			<th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
		</tr>
		</thead>
<?php
			while ($row = mysqli_fetch_assoc($result1)){$r++; extract($row);
			$Phone = $Addr1;
			$Email = $Addr2;
				print ($r % 2) ? "<tr class=\"resultsrow\" title=\"[ID: ".escape($VendorID)."] ".escape($Vendor)."\" style=\"cursor: default;\"> \n" 
					: "<tr class=\"resultsrow oddrow\" title=\"[ID: ".escape($VendorID)."] ".escape($Vendor)."\" style=\"cursor: default;\"> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($VendorID)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($Vendor)."</td> \n";
				$cnr = explode('~~', escape($CName));
				$Fname = $cnr[0]; 
				$Lname = $cnr[1];
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".$Lname. ", ".$Fname."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:left;\">".escape($Zip)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:center;\"> ";
				if($Email){
					echo "<a href=\"mailto:".escape($Email)."\" title=\"Email ".escape($CName)."\" style=\"margin: 1px 5px;\"><i class=\"iconimage fa\" aria-hidden=\"true\">&#xf003;</i></a> \n";}
					else{echo "No Email";}
				if(!$Email&&!$Phone) {echo " / ";
				}			
				if($Phone){
					echo "<a href=\"tel:".escape($Phone)."\" title=\"Call ".escape($Vendor)."\" style=\"margin: 1px 5px;\"><i class=\"iconimage fa\" aria-hidden=\"true\">&#xf095;</i></a> \n";}
					else{echo "No Phone";}
				echo "</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:right;\">$".number_format($GrossRev, 2)."</td> \n";
					if($LastActivity){$Active = date("M d Y", date2code(escape($LastActivity)));}else {$Active = "--";}
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".$Active."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".escape($Status)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:center;\">  \n";
				echo "<i class=\"iconaction fa\" title=\"VIEW\" aria-hidden=\"true\" onclick=\"self.location='vendor-view.php?v=".$VID."';\">&#xf06e;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"EDIT\" aria-hidden=\"true\" onclick=\"self.location='vendor-edit.php?v=".$VID."';\" >&#xf044;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"DELETE\" aria-hidden=\"true\" onclick=\"confirmDelete('Are you sure you wish to delete: \\n[ID: ".escape($VendorID)."] ".escape($Vendor)."?', 'vendor-delete.php?v=".$VID."');\" >&#xf014;</i> \n";
				echo "</td> \n";
				echo "</tr>\n";
			}
			echo "</table>";
				if($r < 51){
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." VENDORS</div> \n";
				}else{
					//replace with pagination
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." VENDORS</div> \n";
				}
		//No Results
			}else{
				echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\"><h4>NO VENDORS IN DATABASE</h4></div>";
			}
	?>

<!-- /results insert -->
      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>
