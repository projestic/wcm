<?php
	$PageName = "New Product";
	include_once('header.php'); 

/*  *** NOTE: THIS PAGE HAS BEEN REPLACED BY product-new.php ****   */


	//CHECK AUTHORITY
	if ($_SESSION['AuthP']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if(isset($_REQUEST["p"])){$P = $_REQUEST["p"];} else {$P = '';}
	if(isset($_POST["OrderID"], $_POST['Item-1'])) {
		$Descr  = sanitize($_POST['QTY-1']). "|" .$_POST['Item-1'] ."::";
		$Descr .= sanitize($_POST['QTY-2']). "|" .$_POST['Item-2'] ."::";
		$Descr .= sanitize($_POST['QTY-3']). "|" .$_POST['Item-3'] ."::";
		$Descr .= sanitize($_POST['QTY-4']). "|" .$_POST['Item-4'] ."::";
		$Descr .= sanitize($_POST['QTY-5']). "|" .$_POST['Item-5'];
		$TransDate = $_POST['TR-YR'].$_POST['TR-MO'].$_POST['TR-DY'];

		$query1 = "INSERT INTO `Orders`
			( `PID`, `OrderID`, `Descr`, `Notes`, `AmntTotal`, `Adjust`, `AmntDue`, `AmntPaid`, `TransDate`, `UID`, `AddedBy`, `AddedDate`)
			VALUES ( '".$_POST['PID']."', '".sanitize($_POST['OrderID'])."', '".$Descr."', '".sanitize($_POST['Notes'])."', '".$_POST['Total']."', '".sanitize($_POST['Adjust'])."', '".$_POST['Due']."', '".sanitize($_POST['Paid'])."', '".$TransDate."', '".$_SESSION['UserID']."', '".$_SESSION['UserName']."', '".date("YmdHis", $Tday)."') ";
		if(!$result1 = sqlQuery($query1)){sqlError(1);}

		$OID = mysql_insert_id();

		$query2 = "INSERT INTO `Inventory` (`PrID`, `OID`, `Amnt`, `TransType`, `TransBy`, `TransDate`) VALUES ";
		if(sanitize($_POST['QTY-1']) && $_POST['Item-1']){
			$query2 .= "  ('".$_POST['Item-1']."', '".$OID."', '".(sanitize($_POST['QTY-1']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-2']) && $_POST['Item-2']){
			$query2 .= ", ('".$_POST['Item-2']."', '".$OID."', '".(sanitize($_POST['QTY-2']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-3']) && $_POST['Item-3']){
			$query2 .= ", ('".$_POST['Item-3']."', '".$OID."', '".(sanitize($_POST['QTY-3']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-4']) && $_POST['Item-4']){
			$query2 .= ", ('".$_POST['Item-4']."', '".$OID."', '".(sanitize($_POST['QTY-4']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-5']) && $_POST['Item-5']){
			$query2 .= ", ('".$_POST['Item-5']."', '".$OID."', '".(sanitize($_POST['QTY-5']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		$query2 .= ";";
		if(!$result2 = sqlQuery($query2)){sqlError(2);}

		//LOOKUP Products
			/*
			$query8 = "SELECT * FROM `Products` WHERE (`Status` = 'A')";
			if(!$result8 = sqlQuery($query8)){sqlError(8);}
			$prod_array = array();
				while ($row = mysqli_fetch_assoc($result8)) {
					$prod_array[$row['name']] = $row['value'];
				}
			*/

		//LOOKUP Patient ID
			$query9 = "SELECT `Pnum` FROM `Patients` WHERE (`PID` = '".$_POST['PID']."')";
			if(!$result9 = sqlQuery($query9)){sqlError(9);}

		//MAIL IT
		$message = "** NEW ORDER ** \n\n";
		$message .= "Order Date : ".$TransDate." \n";
		$message .= "Order ID : ".sanitize($_POST["OrderID"])." \n"; 
		$message .= "Patient ID : ".mysql_result($result9, 0, "Pnum")." \n\n";
		if(sanitize($_POST['QTY-1'])){$message .= " ".sanitize($_POST['QTY-1']). " - #" .$_POST['Item-1']." \n";}
		if(sanitize($_POST['QTY-2'])){$message .= " ".sanitize($_POST['QTY-2']). " - #" .$_POST['Item-2']." \n";}
		if(sanitize($_POST['QTY-3'])){$message .= " ".sanitize($_POST['QTY-3']). " - #" .$_POST['Item-3']." \n";}
		if(sanitize($_POST['QTY-4'])){$message .= " ".sanitize($_POST['QTY-4']). " - #" .$_POST['Item-4']." \n";}
		if(sanitize($_POST['QTY-5'])){$message .= " ".sanitize($_POST['QTY-5']). " - #" .$_POST['Item-5']." \n";}
		$message .= "-----------------------------------\n";
		$message .= number_format($_POST['Total'], 2, '.', ',')." - Total\n";
		$message .= number_format(sanitize($_POST['Adjust']), 2, '.', ',')." - Adjustment\n";
		$message .= number_format($_POST['Due'], 2, '.', ',')." - Amount Due\n";
		$message .= number_format(sanitize($_POST['Paid']), 2, '.', ',')." - Amount Received\n\n";

		$message .= "Notes : ".sanitize($_POST["Notes"])." \n\n";
		$message .= "Submitted : ".date("m/d/y g:ia", $Tday);
		$message .= " by ".$_SESSION["UserName"]." [".$_SESSION["UserID"]."] \n";

		$to	= "New Orders <orders@puresourcecollective.com>";
		//$to	= "New Orders <consultant99@gmail.com>";
		$subject = "NEW ORDER  ".sanitize($_POST["OrderID"]);
		$headers = "From: postmaster@puresourcecollective.com ". PHP_EOL;
		$headers .= "Reply-To: postmaster@puresourcecollective.com ". PHP_EOL;
		//$headers .= "X-Mailer: PHP ". phpversion();

		if (mail($to, $subject, $message, $headers)) {
			$msg = "Report Submitted";
		} else {
			$msg = "Error - Not Submitted";
		}
		//echo "<script>javascript: alert('$msg');</script>";

	//UPDATE ACTIVITY FEED
		$ActDescr = "Added Order: ".$_POST['OrderID'];
		$query9 = "
			INSERT INTO `Activity` 
			(`UID`, `Descr`, `DateTime`, `UIP`) 
			VALUES ('".$_SESSION["UserID"]."','".$ActDescr."','".$TdayF."','".$_SERVER["REMOTE_ADDR"]."')
		";
		if(!$result9 = sqlQuery($query9)){sqlError(9);}

	//REDIRECT
		if ($_POST["NextAction"] == "P"){$url = "patients.php";}
		elseif ($_POST["NextAction"] == "D"){$url = "dashboard.php";}
		elseif ($_POST["NextAction"] == "A"){$url = "order-add.php";}
		else {$url = "dashboard.php"; }
		header ("Location: $url");
		//echo "<script language=\"javascript\" type=\"text/javascript\">self.location='$url';</script>";
		exit;
	} //END POST

?>

  <h2 class="pagename"><?php echo $PageName; ?></h2>

  <div class="maincontent section">
    <div class="producttable">
      <div class="tableheader"><div>&nbsp;</div></div>

      <div class="tableresults">
        <div class="form700px w-form">
          <div class="formtitle">Enter Product Information</div>
          <div class="forminstruction">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et semper quam, ac fringilla nulla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam molestie pharetra pellentesque. Suspendisse feugiat mauris non arcu venenatis, in condimentum ante ultricies. Suspendisse lobortis leo in aliquet ornare. Mauris accumsan convallis turpis sit amet suscipit. Cras ac dolor laoreet, dictum magna id, auctor metus. Nullam sed sem interdum, facilisis ligula non, porttitor dolor.</div>

	<form id="NewOrder" name="NewProduct" enctype="multipart/form-data" method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">

            <!-- label class="fieldlabel" for="name">Name</label -->
            <input class="inputfield w-input" id="ProductName" maxlength="75" name="ProductName" placeholder="Enter Product Name" type="text">
            <select class="inputfield w-select" id="Category" name="Category" required="required">
              <option value="">Select one...</option>
              <option value="Flowers">Flowers</option>
              <option value="Edibles">Edibles</option>
			  <option value="Extracts">Extracts</option>
			  <option value="Misc">Misc</option>
            </select>
			<textarea class="inputfield w-input" id="Description" maxlength="500" name="Description" placeholder="Product Description"></textarea>
            <select class="inputfield w-select" id="Unit" name="Unit" required="required">
              <option value="">Select one...</option>
              <option value="gm">/gram</option>
              <option value="ea">/ea</option>
            </select>
			<input class="inputfield w-input" id="Category" maxlength="75" name="Category" placeholder="Cost / Unit" type="text">
			<input class="inputfield w-input" id="Category" maxlength="75" name="Category" placeholder="Price / Unit" type="text">
			<input class="inputfield w-input" id="Category" maxlength="75" name="Category" placeholder="Low Inv Alert" type="text">
            <div class="w-clearfix w-radio">
              <input class="radio w-radio-input" id="Status" name="Status" type="radio" value="Active">
              <label class="fieldlabel w-form-label" for="Status">Active</label>
			</div>
			<div class="w-clearfix w-radio">
			  <input class="radio w-radio-input" id="Status" name="Status" type="radio" value="Inactive">
			  <label class="fieldlabel w-form-label" for="Status">Inactive</label>
            </div>

<!-- -->
<!-- 
            <input class="halfsized inputfield w-input" data-name="Name 2" id="name-2" maxlength="256" name="name-2" placeholder="Enter your name" type="text">
            <div class="floatcleardiv"></div>
            <input class="inputfield thirdsized w-input" data-name="Name 3" id="name-3" maxlength="256" name="name-3" placeholder="Enter your name" type="text">
            <div class="floatcleardiv"></div>
            <label class="fieldlabel" for="email">Email Address</label>
            <input class="inputfield w-input" data-name="Email" id="email" maxlength="256" name="email" placeholder="Enter your email address" required="required" type="email">
            <div class="w-checkbox w-clearfix">
              <input class="checkbox w-checkbox-input" data-name="Checkbox" id="checkbox" name="checkbox" type="checkbox">
              <label class="fieldlabel w-form-label" for="checkbox">Checkbox</label>
            </div>
            <div class="w-checkbox w-clearfix">
              <input class="w-checkbox-input" data-name="Checkbox 2" id="checkbox-2" name="checkbox-2" type="checkbox">
              <label class="fieldlabel w-form-label" for="checkbox-2">Checkbox</label>
            </div>
            <div class="w-clearfix w-radio">
              <input class="radio w-radio-input" data-name="Radio" id="radio" name="radio" type="radio" value="Radio">
              <label class="fieldlabel w-form-label" for="radio">Radio</label>
            </div>
            <div class="w-clearfix w-radio">
              <input class="w-radio-input" data-name="Radio 2" id="radio" name="radio-2" type="radio" value="Radio">
              <label class="fieldlabel w-form-label" for="radio">Radio</label>
            </div>
            <select class="inputfield w-select" data-name="Select" id="Select" name="Select" required="required">
              <option value="">Select one...</option>
              <option value="First">First Choice</option>
              <option value="Second">Second Choice</option>
              <option value="Third">Third Choice</option>
            </select>
            <textarea class="inputfield w-input" id="field" maxlength="5000" name="field" placeholder="Example Text"></textarea>
-->
<!-- -->

          <div class="w-row">
            <div class="w-col w-col-2"><a class="cancelbutton w-button" href="dashboard.php">Cancel</a>
            </div>
            <div class="w-col w-col-10">
              <input class="submitbutton w-button" type="submit" value="Add Product">
            </div>
          </div>

          </form>

        </div>
        <div class="spacerblock50"></div>
      </div>
    </div>
  </div>

<?php 	include_once("footer.php"); ?>