<?php
	require_once('config.php');

//DB CONNECT
	$db = new mysqli($db_server, $db_user, $db_pass, $db_name);
	mysqli_set_charset($db,'utf8');

	if($db->connect_errno){
	        $msg  = "LOCATION: ".$_SERVER["REQUEST_URI"]." \n";
		    $msg .= "REFERRER: ".$_SERVER["HTTP_REFERER"]." \n";
			$msg .= "DB ERROR: ".$db->connect_errno." \n";
			$msg .= "MESSAGE : ".$db->connect_error." \n";
			$msg .= "QUERY : ".$Q." \n";
		    $to  = "Tech Support <$TechEmail>";
		    $subject = "DB ERROR ".$BizID;
			$headers = "From: $SiteName <$AdminEmail>\r\n";
	        $headers .= "Reply-To: $AdminEmail\r\n";
			mail($to, $subject, $msg, $headers);

			echo "<div style=\"text-align:center;\"><div style=\"color:#BB0000;\"> \n";
			echo "<h4>THE REQUESTED INFORMATION IS TEMPORARILY OFFLINE<h4> \n";
			echo "Please try again in a few minutes.</div> \n";
			echo "<div style=\"color:\"#FFFFFF\"> \n";
			echo "<p>".nl2br($msg); //<!-- $msg -->
			echo "<br>".$Q; //<!-- $Q -->
			echo "\n</div> \n";
		    exit;
		die();
	}
	global $db;
	//$db = new pdo($db_server, $db_user, $db_pass, $db_name);

	//Grab Timezone from DB
	date_default_timezone_set('America/Los_Angeles');

//FUNCTIONS
		function check_session_A()
		{
			if(empty($_SESSION["UserID"])){
				$url = "../";
				header ("Location: $url");
				exit;
			}
		}

		function sqliQuery($Q){
				global $db, $SiteName, $AdminEmail, $TechEmail, $BizID;
			if($results = $db->query($Q)){
				if($results->num_rows){
					//while($row = $results->fetch_object()){
					//$records[] = $row;
					//}
					//return($records);
					return($results);
				$results->free();
				$db->close();
				}
			} else {
				$msg  = "LOCATION: ".$_SERVER["REQUEST_URI"]." \n";
				$msg .= "REFERRER: ".$_SERVER["HTTP_REFERER"]." \n";
				$msg .= "DB ERROR: ".$db->errno." \n";
				$msg .= "MESSAGE : ".$db->error." \n";
				$msg .= "QUERY : ".$Q." \n";
				$to  = "Tech Support <$TechEmail>";
				$subject = "DB ERROR ".$BizID;
				$headers = "From: $SiteName <$AdminEmail>\r\n";
				$headers .= "Reply-To: $AdminEmail\r\n";
				mail($to, $subject, $msg, $headers);
				echo "<div style=\"text-align:center;\"><div style=\"color:#BB0000;\"> \n";
				echo "<h4>THIS INFORMATION IS TEMPORARILY UNAVAILABLE<h4> \n";
				echo "Please try again in a few minutes.</div> \n";
				echo "<!--  \n";
				echo "<p>".nl2br($msg);
				echo "<br>".$Q;
				echo "\n --> \n";
				//$results->free();
				$db->close();
				die(); exit;
			}
		}

		function escape($string) {
		  return htmlentities(trim($string), ENT_QUOTES, 'UTF-8');
		}

		function uniqidReal($len) {
			// uniqid gives 13 chars, but you could adjust it to your needs.
			if (function_exists("random_bytes")) {
				$bytes = random_bytes(ceil($len / 2));
			} elseif (function_exists("openssl_random_pseudo_bytes")) {
				$bytes = openssl_random_pseudo_bytes(ceil($len / 2));
			} else {
				throw new Exception("no cryptographically secure random function available");
			}
			return substr(bin2hex($bytes), 0, $len);
		}


	function date2code(){ 
	 $var = 1;
	  //OLD FUNCTION TO FORMAT DATE
	}


//FUNCTIONS BY STEVE
   function getProductInfo() {
    //echo "Got your Product Info Here.<BR>";
    global $db, $PID, $Category, $ItemID, $ProductName, $Description, $DetailLink, $PicLink1, $PicLink2, $PicLink3, $PType, $Strain, $CBN, $CBD, $THC, $Cost1,$Cost2,$Cost3,$Cost4,$Cost5, $Price1, $Price2, $Price3, $Price4, $Price5, $Unit1, $Unit2, $Unit3, $Unit4, $Unit5, $Position, $Status, $Tax, $TaxRate, $InvAlert, $Notes;
         $PID_IN	= sanitize($_GET['p']);		   

			if(!empty($PID_IN)){				
				$returnArray = array();
		      $query = "Select * from Products where PID ='". $PID_IN . "'"; 

		      $result = $db->query($query);
               if(!$result->num_rows){

		         } else {
    			    while($prodinfo = $result->fetch_assoc()){
					 $PID = $prodinfo['PID'];
					 $ItemID = $prodinfo['ItemID'];
				    $ProductName = $prodinfo['Product'];
				    $Category = $prodinfo['Category'];
				    $Description = $prodinfo['Descr'];
				    $DetailLink = $prodinfo['Detail'];
				    $PType = $prodinfo['PType'];
				    $PicLink1 = $prodinfo['Pic1'];
				    $PicLink2 = $prodinfo['Pic2'];
				    $PicLink3 = $prodinfo['Pic3'];
				    $Strain = $prodinfo['Strain'];
				    $CBN = $prodinfo['CBN'];
				    $CBD = $prodinfo['CBD'];
				    $THC = $prodinfo['THC'];
				    $Cost1 = $prodinfo['Cost1'];
				    $Cost2 = $prodinfo['Cost2'];
				    $Cost3 = $prodinfo['Cost3'];
				    $Cost4 = $prodinfo['Cost4'];
				    $Cost5 = $prodinfo['Cost5'];
				    $Price1 = $prodinfo['Price1'];
				    $Price2 = $prodinfo['Price2'];
				    $Price3 = $prodinfo['Price3'];
				    $Price4 = $prodinfo['Price4'];
				    $Price5 = $prodinfo['Price5'];
				    $Unit1 = $prodinfo['Unit1'];
				    $Unit2 = $prodinfo['Unit2'];
				    $Unit3 = $prodinfo['Unit3'];
				    $Unit4 = $prodinfo['Unit4'];
				    $Unit5 = $prodinfo['Unit5'];
				    $Position = $prodinfo['Position'];
				    $Status = $prodinfo['Status'];
				    $Tax = $prodinfo['Tax'];
				    $TaxRate = $prodinfo['TaxRate'];
				    $InvAlert = $prodinfo['InvAlert'];
				    $Notes = $prodinfo['Notes'];
                //
            		}	
           		//
         		 }
         		 //
    		  }
   			  //
	 } 

   function getVendorInfo() {
    //echo "Got your Product Info Here.<BR>";
    //global $db, $VID, $Vendor, $VendorID, $Zip, $Fname, $Lname, $GrossRev, $LastActivity, $Status, $Phone, $Email, $Notes;
    global $db, $VID, $Vendor, $VendorID, $Zip, $CName, $Phone, $Email, $Fname, $Lname, $Status;
         $VendorCode	= sanitize($_GET['v']);		   

			if(!empty($VendorCode)){				
				$returnArray = array();
		      $query = "Select * from Vendors where VID ='". $VendorCode . "'"; 

		      $result = $db->query($query);
               if(!$result->num_rows){

		         } else {
    			    while($vendorinfo = $result->fetch_assoc()){
					 $VID = $vendorinfo['VID'];
					 $VendorID = $vendorinfo['VendorID'];
				    $Vendor = $vendorinfo['Vendor'];
				    $Zip = $vendorinfo['Zip'];
				    $CName = $vendorinfo['CName'];
				    $cnr = explode('~~', $CName);
					 $Fname = $cnr[0]; 
					 $Lname =$cnr[1];
				    //$Fname = $vendorinfo['Fname'];
				   // $Lname = $vendorinfo['Lname'];
				   // $GrossRev = $vendorinfo['GrossRev'];
				    //$LastActivity = $vendorinfo['LastActivity'];
				    $Status = $vendorinfo['Status'];
				    $Phone = $vendorinfo['Addr1'];
				    $Email = $vendorinfo['Addr2'];
				    //$Phone = $vendorinfo['Phone'];
				    //$Email = $vendorinfo['Email'];
				    //$Notes = $vendorinfo['Notes'];
                //
            		}	
           		//
         		 }
         		 //
    		  }
   			  //
	 } 
	 
	function addActivity($Activity,$TS) {
		global $db;
	   //echo "Activity Added.<BR>Activity: ".$Activity."<BR>Time: ".$TS."<BR>";
			$Query2 = "INSERT INTO `Activity` (`UID`, `UIP`, `Descr`, `DateTime`) VALUES (?, ?, ?, ?)"; 
			//$Query2 = "INSERT INTO `Activity` (`UID`) VALUES (?)"; 
			$insert2 = $db->prepare($Query2);
			//echo "Query2: ".$Query2 . "Time: ". $TS . "Activity: ". $Activity;
			$insert2->bind_param('isss', $_SESSION["UserID"], $_SERVER["REMOTE_ADDR"], $Activity, $TS);
			$insert2->execute();			
			$insert2->close();

	}
	
	function updateVActivity($VID,$TS) {
		global $db;
         $Query2 = "UPDATE `Vendors` SET `LastActivity`=? WHERE VID=?";
			$insert2 = $db->prepare($Query2);
			$insert2->bind_param('si', $TS, $VID);
			$insert2->execute();			
			$insert2->close();
	}	


	function listProductDD() {
     global $db;
	  $query1 = "SELECT * FROM `Products` WHERE (`Status` != 'X') ORDER BY Product ASC";
	   echo $query1;
		$result1 = $db->query($query1);
				while ($row = $result1->fetch_assoc()) {
    				echo "<option value='" . $row['PID'] . "'>" . $row['ItemID'] ." : ". $row['Product'] . " [". $row['Unit1'] . "]</option>";
				}
	}

	function listVendorDD() {
     global $db;
	  $query1 = "SELECT * FROM `Vendors` WHERE (`Status` != 'X') ORDER BY Vendor ASC";    
	   echo $query1;
		$result1 = $db->query($query1);
					while ($row = $result1->fetch_assoc()) {
    				echo "<option value='" . $row['VID'] . "'>" . $row['VendorID'] ." : ". $row['Vendor'] . "</option>";
				}
	}
	
	function workflow_list($status) {
		$query1 = "
		SELECT Orders.*
		FROM `Orders` 
		WHERE `Status` = '$Status'
		ORDER BY AddedOn
		";
		//echo $query1;		

		$result1 = sqliQuery($query1);
		$r = count($result1);

		if($r){
			while ($row = mysqli_fetch_assoc($result1)){$r++; extract($row);
		 	echo "<div class=\"carddatatext\"><a href=\"order-view.php?o=".$OrderID."\">".date("M d h:ia", $AddedOn)."&nbsp;|&nbsp;". $OrderID . "</a></div> \n";
		}
			}
		else {
		echo "<div class=\"carddatatext\">no orders</div> \n";	
		}
		echo "Status: ".$status;
		
		echo "<div class=\"reportcardsubtext\"><strong>TOTAL:". print $r ."</strong></div";
      
		
	}

//END FUNCTIONS BY STEVE

//PASS TO SCRIPTS
		$Tday = time();
?>