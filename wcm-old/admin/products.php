<?php
	$PageName = "Products";
	include_once('preamble.php');
	include_once('header.php'); 

	//CHECK AUTHORITY
	if ($_SESSION['AuthP']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}

	if (empty($_REQUEST["pp"])) {$pp=100000;} else {$pp = $_REQUEST['pp'];}
	if (empty($_REQUEST["offset"])) {$offset=0;} else {$offset = $_REQUEST['offset'];}

	//BUILD SORT
        if (empty($_REQUEST["s"])) {$s = "asc"; $s2 = "desc";} else {$s = $_REQUEST["s"];}
		if (empty($_REQUEST["sort"])) {$sort = "X";} else {$sort = $_REQUEST["sort"];}
        if ($sort =="P"){$Sorter = "P.Category $s, P.Product $s";}
		elseif ($sort =="X"){$Sorter = "P.Category $s, P.Product $s";}
        else {$Sorter = "P.Category $s, P.Product $s";}

	//BUILD CLAUSES
		$URLphrase = "&pp=$pp";
		//if ($_SESSION['UserAuth'] > 1){$dbClause  = "WHERE (1)";}
		//else {$dbClause  = "WHERE (`TransDate` > '{$Archive}')";}
		$dbClause  = "WHERE (P.Status != 'X')";
		$URLphrase = str_replace(" ", "+", $URLphrase); //&sort=$sort&

	//GRAB DATA
	//OLD LEFT JOIN METHOD FOR ONLY INVENTORY
	/*	$query1 = "
			SELECT Products.*, 
			IFNULL(I.Stock, 0) AS `Stock`
			FROM `Products`
			LEFT JOIN 
				(SELECT *, SUM(I.TransAmnt) AS `Stock` 
				FROM `Inventory` AS I
				GROUP BY PID) I 
				ON (Products.PID = I.PID)
			$dbClause
			ORDER BY $Sorter
			LIMIT $offset,$pp
			"; // Products.PID AS `ProductID`,*/
			
	//OLD LEFT JOIN METHOD FOR INVENTORY AND ORDER DATA		
	/*	  $query1 = "
		  SELECT Products.*, 
			IFNULL(I.Stock, 0) AS `Stock`,
			IFNULL(O.OrdTot, 0) AS `OrdTot`
			FROM `Products`
			LEFT JOIN 
				(SELECT *, SUM(I.TransAmnt) AS `Stock` 
				FROM `Inventory` AS I
				GROUP BY PID) I 
				ON (Products.PID = I.PID)
			LEFT JOIN 
				(SELECT *, SUM(O.AmntTotal) AS `OrdTot` 
				FROM `Orders` AS O
				GROUP BY PID) O 
				ON (Products.PID = O.PID)	
				$dbClause
			ORDER BY $Sorter
			LIMIT $offset,$pp
			"; // Products.PID AS `ProductID`, */
			
			//$query1="Select * from Products";

			//Orders table currently not capturing PID -- Can't Use -SC
			/*$query1 = "
			SELECT P.*, 
    			IFNULL((SELECT sum(I.TransAmnt) 
      			FROM `Inventory` I 
      			WHERE I.PID=P.PID ), 0.00) AS 'Stock',
    			IFNULL((SELECT sum(O.AmntTotal) 
      			FROM `Orders` O 
      			WHERE O.PID=P.PID ),0.00) AS `OrdTot`,
      			IFNULL((SELECT count(O.AmntTotal) 
      			FROM `Orders` O 
      			WHERE O.PID=P.PID ),0.00) AS `OrdCt`
				FROM `Products` P 
				$dbClause
			ORDER BY $Sorter
			LIMIT $offset,$pp
			"; 
			 */
			
		// Query not pulling order data
		$query1 = "
			SELECT P.*, 
    			IFNULL((SELECT sum(I.TransAmnt) 
      			FROM `Inventory` I 
      			WHERE I.PID=P.PID ), 0.00) AS 'Stock'
				FROM `Products` P 
				$dbClause
			ORDER BY $Sorter
			LIMIT $offset,$pp
			"; 
	
		//	echo $query1;
			
		$result1 = sqliQuery($query1);
		
		//DEBUG: RETURN COLUMN NAMES
		/*
		$finfo = $result1->fetch_field_direct(0);
      printf("Name:     %s\n", $finfo->name);	
      */
		$r = count($result1);		
		
	
?>

  <div class="maincontent section">
  <h2 class="pagename"><?php echo $PageName; ?></h2>
    <div class="displayblock">

      <div class="tableheader"><div><!-- HEADER TEXT -->&nbsp;</div>
        <div class="actionblock">
          <div class="submenudropdown w-dropdown" data-delay="0">
            <div class="submenutoggle w-dropdown-toggle">
              <div class="dropdowntext"><i class="iconimage fa" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true">&#xf013;</i></div>
              <div class="submenuicon w-icon-dropdown-toggle"></div>
            </div>
            <nav class="submenulist w-dropdown-list">
				<a class="submenulink w-dropdown-link" href="product-new.php">NEW Product</a>
				<a class="submenulink w-dropdown-link" href="inventory-add.php">ADD Inventory</a>
				<a class="submenulink w-dropdown-link" href="product-upload.php">ADD Product Pics</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Product Report</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Product CSV</a>
				<a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Product Bulk Upload</a>
            </nav>
          </div>
        </div>
      </div>

      <div class="outputblock">
		<!-- SEARCH -->
        <div class="searchblock">
          <div class="w-form">
            <form class="w-clearfix" name="SearchForm" method="get" action="search.js">
              <input class="searchbutton w-button fa" type="submit" value="&#xf002;">
              <input class="searchbox w-input" id="Search" maxlength="75" name="Search" placeholder="Search" type="text">
            </form>
          </div>
        </div>
		<!-- SEARCH -->

<?php	if($r){	?>

		<table class="resultstable" id="table1">
		<thead>
		<tr>  
			<th class="resultsheader" style="text-align:left;" title="Product Code">Item Code</th>
			<th class="resultsheader hidden-xs" style="text-align:left;" title="Category">Category</th>
			<th class="resultsheader" style="text-align:left;" title="Product">Product</th
			<th class="resultsheader hidden-xs" style="text-align:center;" title="Type">Type</th>
            <th class="resultsheader hidden-xs" style="text-align:center;" title="Type">U/M</th>
			<th class="resultsheader hidden-xs" style="text-align:right;" id="salesc" title="Cost">Sales #</th>
			<th class="resultsheader hidden-xs" style="text-align:right;" id="salesd" title="Price">Sales $</th>
			<th class="resultsheader" style="text-align:right;" id="inventory" title="Units in Stock">Inventory</th>
			<th class="resultsheader hidden-xs" style="text-align:center;" id="status" title="Status">Status</th>
			<th class="resultsheader" style="text-align:center;" id="action" title="Action">&nbsp;</th>
		</tr>
		</thead>
<?php
			while ($row = mysqli_fetch_assoc($result1)){$r++; extract($row);
				print ($r % 2) ? "<tr class=\"resultsrow\" title=\"[ID: ".escape($ItemID)."] ".escape($Product)."\" style=\"cursor: default;\"> \n" 
					: "<tr class=\"resultsrow oddrow\" title=\"[ID: ".escape($ItemID)."] ".escape($Product)."\" style=\"cursor: default;\"> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($ItemID)."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:left;\">".escape($Category)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:left;\">".escape($Product)."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".escape($PType)."</td> \n";
                echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".escape($Unit1)."</td> \n";
					//REPLACE WITH CALCULATIONS
							$TotalSalesD = mt_rand(25,4000); //db reference: $SalesDollars
							$TotalSalesV = mt_rand(25,75);  //Sales Volume
							$TotalSalesC = ($TotalSalesD/$TotalSalesV); //db reference: $SalesCount
							$TotalSalesC = $OrdTot;
                                                        $OrdCt = mt_rand(25,75);
                                                        $OrdTot = ($OrdCt * $Price1);
                                                        $Stock = mt_rand(0,13);
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:right;\">".number_format(escape($OrdCt), 0)."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:right;\">$&nbsp;".number_format(escape($OrdTot), 2)."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:right;\"> ";
				if($Stock <= $InvAlert){echo "<i class=\"iconimage fa\" style=\"color: orange; font-size: 12px; margin: 2px 5px;\" aria-hidden=\"true\">&#xf071;</i>";}
				echo number_format(escape($Stock), 4)."</td> \n";
				echo "<td class=\"resultscell hidden-xs\" style=\"text-align:center;\">".escape($Status)."</td> \n";
				echo "<td class=\"resultscell\" style=\"text-align:center;\">  \n";
				echo "<i class=\"iconaction fa\" title=\"VIEW\" aria-hidden=\"true\" onclick=\"self.location='product-view.php?p=".$PID."';\">&#xf06e;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"EDIT\" aria-hidden=\"true\" onclick=\"self.location='product-edit.php?p=".$PID."';\" >&#xf044;</i> \n";
				echo "<i class=\"iconaction fa\" title=\"DELETE\" aria-hidden=\"true\" onclick=\"confirmDelete('Are you sure you wish to delete: \\n[ID: ".escape($ItemID)."] ".escape($Product)."?', 'product-delete.php?p=".$PID."');\" >&#xf014;</i> \n";
				echo "</td> \n";
				echo "</tr> \n";
			}
			echo "</table>";
				if($r < 51){
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." ITEMS</div> \n";
				}else{
					//replace with pagination
					echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\">".($r-1)." ITEMS</div> \n";
				}
		//No Results
			}else{
				echo "<div style=\"text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;\"><h4>NO PRODUCTS IN DATABASE</h4></div>";
			}
	?>

<!-- /results insert -->

      </div>
    </div>
  </div>

<?php	include_once('footer.php'); ?>