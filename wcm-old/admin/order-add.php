<?php
	session_start();
	require_once('assets/functions.php');
	check_session_U();

	$PageName = "New Order";

	//CHECK AUTHORITY
	if ($_SESSION['AuthO']	!== 'Y'){echo "Not Authorized to view ".$PageName; exit;}



	if(isset($_REQUEST["p"])){$P = $_REQUEST["p"];} else {$P = '';}

	if(isset($_POST["OrderID"], $_POST['Item-1'])) {

		$Descr  = sanitize($_POST['QTY-1']). "|" .$_POST['Item-1'] ."::";
		$Descr .= sanitize($_POST['QTY-2']). "|" .$_POST['Item-2'] ."::";
		$Descr .= sanitize($_POST['QTY-3']). "|" .$_POST['Item-3'] ."::";
		$Descr .= sanitize($_POST['QTY-4']). "|" .$_POST['Item-4'] ."::";
		$Descr .= sanitize($_POST['QTY-5']). "|" .$_POST['Item-5'];
		$TransDate = $_POST['TR-YR'].$_POST['TR-MO'].$_POST['TR-DY'];

		$query1 = "INSERT INTO `Orders`
			( `PID`, `OrderID`, `Descr`, `Notes`, `AmntTotal`, `Adjust`, `AmntDue`, `AmntPaid`, `TransDate`, `UID`, `AddedBy`, `AddedDate`)
			VALUES ( '".$_POST['PID']."', '".sanitize($_POST['OrderID'])."', '".$Descr."', '".sanitize($_POST['Notes'])."', '".$_POST['Total']."', '".sanitize($_POST['Adjust'])."', '".$_POST['Due']."', '".sanitize($_POST['Paid'])."', '".$TransDate."', '".$_SESSION['UserID']."', '".$_SESSION['UserName']."', '".date("YmdHis", $Tday)."') ";
		if(!$result1 = sqlQuery($query1)){sqlError(1);}

		$OID = mysql_insert_id();

		$query2 = "INSERT INTO `Inventory` (`PrID`, `OID`, `Amnt`, `TransType`, `TransBy`, `TransDate`) VALUES ";
		if(sanitize($_POST['QTY-1']) && $_POST['Item-1']){
			$query2 .= "  ('".$_POST['Item-1']."', '".$OID."', '".(sanitize($_POST['QTY-1']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-2']) && $_POST['Item-2']){
			$query2 .= ", ('".$_POST['Item-2']."', '".$OID."', '".(sanitize($_POST['QTY-2']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-3']) && $_POST['Item-3']){
			$query2 .= ", ('".$_POST['Item-3']."', '".$OID."', '".(sanitize($_POST['QTY-3']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-4']) && $_POST['Item-4']){
			$query2 .= ", ('".$_POST['Item-4']."', '".$OID."', '".(sanitize($_POST['QTY-4']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		if(sanitize($_POST['QTY-5']) && $_POST['Item-5']){
			$query2 .= ", ('".$_POST['Item-5']."', '".$OID."', '".(sanitize($_POST['QTY-5']) *-1)."', 'SALE', '".$_SESSION['UserID']."', '".$TransDate."') ";}

		$query2 .= ";";
		if(!$result2 = sqlQuery($query2)){sqlError(2);}

		//LOOKUP Products
			/*
			$query8 = "SELECT * FROM `Products` WHERE (`Status` = 'A')";
			if(!$result8 = sqlQuery($query8)){sqlError(8);}
			$prod_array = array();
				while ($row = mysqli_fetch_assoc($result8)) {
					$prod_array[$row['name']] = $row['value'];
				}
			*/

		//LOOKUP Patient ID
			$query9 = "SELECT `Pnum` FROM `Patients` WHERE (`PID` = '".$_POST['PID']."')";
			if(!$result9 = sqlQuery($query9)){sqlError(9);}

		//MAIL IT
		$message = "** NEW ORDER ** \n\n";
		$message .= "Order Date : ".$TransDate." \n";
		$message .= "Order ID : ".sanitize($_POST["OrderID"])." \n"; 
		$message .= "Patient ID : ".mysql_result($result9, 0, "Pnum")." \n\n";
		if(sanitize($_POST['QTY-1'])){$message .= " ".sanitize($_POST['QTY-1']). " - #" .$_POST['Item-1']." \n";}
		if(sanitize($_POST['QTY-2'])){$message .= " ".sanitize($_POST['QTY-2']). " - #" .$_POST['Item-2']." \n";}
		if(sanitize($_POST['QTY-3'])){$message .= " ".sanitize($_POST['QTY-3']). " - #" .$_POST['Item-3']." \n";}
		if(sanitize($_POST['QTY-4'])){$message .= " ".sanitize($_POST['QTY-4']). " - #" .$_POST['Item-4']." \n";}
		if(sanitize($_POST['QTY-5'])){$message .= " ".sanitize($_POST['QTY-5']). " - #" .$_POST['Item-5']." \n";}
		$message .= "-----------------------------------\n";
		$message .= number_format($_POST['Total'], 2, '.', ',')." - Total\n";
		$message .= number_format(sanitize($_POST['Adjust']), 2, '.', ',')." - Adjustment\n";
		$message .= number_format($_POST['Due'], 2, '.', ',')." - Amount Due\n";
		$message .= number_format(sanitize($_POST['Paid']), 2, '.', ',')." - Amount Received\n\n";

		$message .= "Notes : ".sanitize($_POST["Notes"])." \n\n";
		$message .= "Submitted : ".date("m/d/y g:ia", $Tday);
		$message .= " by ".$_SESSION["UserName"]." [".$_SESSION["UserID"]."] \n";

		$to	= "New Orders <orders@puresourcecollective.com>";
		//$to	= "New Orders <consultant99@gmail.com>";
		$subject = "NEW ORDER  ".sanitize($_POST["OrderID"]);
		$headers = "From: postmaster@puresourcecollective.com ". PHP_EOL;
		$headers .= "Reply-To: postmaster@puresourcecollective.com ". PHP_EOL;
		//$headers .= "X-Mailer: PHP ". phpversion();

		if (mail($to, $subject, $message, $headers)) {
			$msg = "Report Submitted";
		} else {
			$msg = "Error - Not Submitted";
		}
		//echo "<script>javascript: alert('$msg');</script>";

	//UPDATE ACTIVITY FEED
		$ActDescr = "Added Order: ".$_POST['OrderID'];
		$query9 = "
			INSERT INTO `Activity` 
			(`UID`, `Descr`, `DateTime`, `UIP`) 
			VALUES ('".$_SESSION["UserID"]."','".$ActDescr."','".$TdayF."','".$_SERVER["REMOTE_ADDR"]."')
		";
		if(!$result9 = sqlQuery($query9)){sqlError(9);}

	//REDIRECT
		if ($_POST["NextAction"] == "P"){$url = "patients.php";}
		elseif ($_POST["NextAction"] == "D"){$url = "dashboard.php";}
		elseif ($_POST["NextAction"] == "A"){$url = "order-add.php";}
		else {$url = "dashboard.php"; }
		header ("Location: $url");
		//echo "<script language=\"javascript\" type=\"text/javascript\">self.location='$url';</script>";
		exit;
	} //END POST
	
	include_once("header.php");
	include_once("menu.php");
?>

    <div class="contentblock">
      <h3><?php print $PageName; ?></h3>
      <div class="formwrapper600 w-form">

	<form id="NewOrder" name="NewOrder" enctype="multipart/form-data" method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="OrderID">Order ID</label></div>
            <div class="w-col w-col-10">
              <input class="inputfield w-input" id="OrderID" maxlength="50" name="OrderID" placeholder="Enter Order Number" required="required" type="text" value="<?php print "O-". strtoupper(uniqidReal('9'));?>">
			</div>
          </div>

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="PID">Patient</label></div>
            <div class="w-col w-col-10">
  				<select class="inputfield w-select" id="PID" name="PID" required="required">
				<option value="">Select...</option>
<?php
			$patients = array();

			$query2 = "SELECT * FROM `Patients` WHERE `Status` ='A'";
			if($results = $db->query($query2)){
				if($results->num_rows){
					while($row = $results->fetch_object()){
						$patients[] = $row;
					}
					$results->free();
				}
			} else {die($db->error);}

				//echo '<pre>', print_r($patients), '</pre>; //test output
				//		while($dbresult->fetch()){ echo $request1, '', $result2, '<br>'; }

			if(!count($patients)){
				echo "<option value=\"\">No Active Patients found</option>";
			} else {
				foreach($patients as $r){
					echo "<option value=\"".escape($r->PID)."\" ";
						if ($P == escape($r->PID)){print " selected=\"selected\" ";}
					echo ">".escape($r->Fname)." ".escape($r->Lname)." [".escape($r->Pnum)."]</option>\n";
				}
			}
?>
				</select>
            </div>
          </div>

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2">
              <label class="inputlabel" for="Trans">Transaction Date</label>
            </div>
            <div class="w-clearfix w-col w-col-10">
              <select class="inputfield w-select year" id="TR-YR" name="TR-YR" required="required">
<?php				$ThisYear = date("Y", $Tday);
				for($k=0; $k<3; $k++){
					echo "<option value=\"".($ThisYear-$k)."\" ";
						if(($ThisYear-$k) == ($ThisYear)){echo " selected=\"selected\" ";}
					echo ">".($ThisYear-$k)."</option> \n";
				}
?>
              </select>
              <select class="inputfield month w-select" id="TR-MO" name="TR-MO" required="required">
<?php
				reset($_Months);
				$ThisMonth = date("m", $Tday);
				while (list($MoNum, $MoName) = each ($_Months)){
					echo "<option value=\"$MoNum\" ";
						if($MoNum == $ThisMonth){echo " selected=\"selected\" ";}
					echo ">".$MoName."</option>";
				}
?>
              </select>
              <select class="day inputfield w-select" id="TR-DY" name="TR-DY" required="required">
<?php
				for ($wd=1; $wd<32; $wd++) {
					echo "<option value=\"".sprintf("%02d", $wd)."\" ";
					if ($wd == date("j", $Tday) ){echo " selected=\"selected\" ";}
					echo ">";
					print sprintf("%02d", $wd);
					echo "</option>";
				}
?>
              </select>
            </div>
          </div>

<?php
			$products = array();

			$query3 = "SELECT * FROM `Products` WHERE `Status` ='A' ORDER BY `Category` asc";
			if($results = $db->query($query3)){
				if($results->num_rows){
					while($row = $results->fetch_object()){
						$products[] = $row;
					}
					$results->free();
				}
			} else {die($db->error);}

			//LOOP for 5 Inputs
			for($x=1; $x<6; $x++){ 
				if($x == 1){$req = "required=\"required\"";} else {$req = "";}
	?>

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="Item-<?php print $x;?>">Item <?php print $x;?></label></div>
            <div class="w-col w-col-10">
              <input class="inputfield w-input" id="QTY-<?php print $x;?>" name="QTY-<?php print $x;?>" value="0" style="width:40px; float:left; margin-right:10px;" onfocus="this.select();" onkeyup="getTotal()">
              <select class="inputfield w-select" id="Item-<?php print $x;?>" name="Item-<?php print $x;?>" style="width:300px;" <?php print $req; ?> onchange="getTotal()">
                <option value="0">Select...</option>
<?php
			if(!count($products)){
				echo "<option value=\"\">No Active Products found</option>";
			} else {
				foreach($products as $p){
					$Category = "[".substr(strtoupper(escape($p->Category)), 0, 2)."]";
					echo "<option value=\"".escape($p->PrID)."\">".$Category." ".escape($p->Product)." ".escape($p->Price)." /".escape($p->Unit)."</option>\n";
				}
			}
			reset($products);
?>
              </select>
            </div>
          </div>

<?php		}//end loop of 5 items ?>

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="Notes">Notes</label></div>
            <div class="w-col w-col-10">
              <textarea class="inputfield w-input" id="Notes" maxlength="5000" name="Notes" placeholder="Notes about this order"></textarea>
            </div>
          </div>

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="Total">Amount</label></div>
            <div class="w-col w-col-2">TOTAL</div>
			<div class="w-col w-col-6" style="text-align:right; float:right;"><input class="inputfield w-input" id="Total" maxlength="15" name="Total" placeholder="Total" type="text" value="0.00" style="width:140px; text-align:right;" readonly></div>
			</div>

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="Adjust"></label></div>
            <div class="w-col w-col-2">ADJUST</div>
			<div class="w-col w-col-6" style="text-align:right; float:right;"><input class="inputfield w-input" id="Adjust" maxlength="15" name="Adjust" placeholder="Adjust" required="required" type="text" value="0.00" style="width:140px; text-align:right;" onkeyup="getTotal()" onfocus="this.select();"></div>
			</div>

          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="Due"></label></div>
            <div class="w-col w-col-2">DUE</div>
			<div class="w-col w-col-6" style="text-align:right; float:right;"><input class="inputfield w-input" id="Due" maxlength="15" name="Due" placeholder="Amount Due" required="required" type="text" value="0.00" style="width:140px; text-align:right;" readonly></div>
			</div>


			<div class="w-row">
			<div class="w-clearfix w-col w-col-2"></div>
			<div class="w-col w-col-2">PAID</div>
			<div class="w-col w-col-6" style="text-align:right; float:right;"><input class="inputfield w-input" id="Paid" maxlength="15" name="Paid" placeholder="Amount Paid" required="required" type="text" value="0.00" style="width:140px; text-align:right;" onfocus="this.select();"></div>
          </div>


          <div class="w-row">
            <div class="w-clearfix w-col w-col-2"><label class="inputlabel" for="NextAction">Add, then:</label></div>
            <div class="w-col w-col-10">
              <select class="inputfield w-select" id="NextAction" name="NextAction" required="required">
                <option value="D">View Dashboard</option>
				<option value="P">View Patient Info</option>
                <option value="A">Place another Order</option>
              </select>
            </div>
          </div>

          <div class="w-row">
            <div class="w-col w-col-2">
              <label class="inputlabel" for="name">&nbsp;</label>
            </div>
            <div class="w-col w-col-10">
              <div class="text10px">Input by <?php print $_SESSION["UserName"]." on ".date("M d Y H:i", $Tday)." from ".$_SERVER["REMOTE_ADDR"]; ?></div>
            </div>
          </div>

          <div class="w-row">
            <div class="w-col w-col-2"><a class="cancelbutton w-button" data-ix="modalclose" href="dashboard.php">Cancel</a>
            </div>
            <div class="w-col w-col-10">
              <input class="submitbutton w-button" type="submit" value="Submit">
            </div>
          </div>
        </form>

      </div>
    </div>

<script type="text/javascript">
			var theForm = document.forms["NewOrder"];
			var product_prices = new Array();
			product_prices["0"]="0.00";

			<?php
			if(!count($products)){
				echo "<option value=\"\">No Active Products found</option>";
			} else {
				foreach($products as $p){
					print "product_prices[\"".escape($p->PrID)."\"]=".escape($p->Price).";\n";
				}
			}
			?>

			function getProductPrice()
			{
				var productPrice=0;
				var adjust = 0;
				var productCount = <?php print count($products); ?>;
				var theForm = document.forms["NewOrder"];
				var itemCount = 1;

				//loop for each product

			<?php
			if(count($products)){
				for($x=1; $x<6; $x++)
				{
					print "var productTotal_".$x." = 0; \n";
					print "var quantity_".$x." = theForm.elements['QTY-".$x."'].value; \n";
					print "var selectedProduct_".$x." = theForm.elements['Item-".$x."'].value; \n";
					print "var productPrice_".$x." = product_prices[selectedProduct_".$x."]; \n";
					print "var productTotal_".$x." = quantity_".$x." * productPrice_".$x."; \n";
					print "productPrice += productTotal_".$x."; \n";
				}
			}
			?>
				productPrice = productPrice.toFixed(2);
				document.getElementById("Total").value=productPrice;
				adjust = theForm.elements['Adjust'].value;
				productPrice = Number(productPrice) + Number(adjust);
				return Number(productPrice);
			}

			function getTotal()
			{
				var orderCost = getProductPrice();
				orderCost = orderCost.toFixed(2);
				document.getElementById("Due").value=orderCost;
			}
</script>

<?php 	include_once("footer.php"); ?>