$( document ).ready(function() {
    $( ".datepicker" ).datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('.delete-btn').append(function(){
        return "<form action='"+$(this).attr('href')+"' method='POST' style='display:none'>"+
            "<input type='hidden' name='_method' value='delete'>"+
            "<input type='hidden' name='_token' value='" + window.Laravel.csrfToken + "'>"+
            "</form>"
    })
    .removeAttr('href')
    .attr('onclick','$(this).find("form").submit();');
});
