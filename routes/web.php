<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Admin\Auth\LoginController@login');
Route::post('admin/logout', 'Admin\Auth\LoginController@logout')->name('logout');
// Password Reset Routes...
Route::get('admin/password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('admin/password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('admin/password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('admin/password/reset', 'Admin\Auth\ResetPasswordController@reset');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['menu', 'auth']], function () {
    //Dashboard
    Route::get('/', [
        'as' => 'admin.dashboard',
        'uses' => 'DashboardController@index'
    ]);
    Route::get('dashboard', [
        'as' => 'admin.dashboard',
        'uses' => 'DashboardController@index'
    ]);
    //Clients
    Route::get('clients', [
        'as' => 'admin.clients',
        'uses' => 'ClientController@index'
    ]);
    Route::get('clients/view/{id}', [
        'as' => 'admin.clients.view',
        'uses' => 'ClientController@view'
    ]);
    Route::get('clients/create', [
        'as' => 'admin.clients.create',
        'uses' => 'ClientController@create'
    ]);
    Route::post('clients/store', [
        'as' => 'admin.clients.store',
        'uses' => 'ClientController@store'
    ]);
    Route::delete('clients/delete/{id}', [
        'as' => 'admin.clients.delete',
        'uses' => 'ClientController@delete'
    ]);
    //Products
    Route::get('products', [
        'as' => 'admin.products',
        'uses' => 'ProductController@index'
    ]);
    Route::get('products/view/{id}', [
        'as' => 'admin.products.view',
        'uses' => 'ProductController@view'
    ]);
    Route::get('products/create', [
        'as' => 'admin.products.create',
        'uses' => 'ProductController@create'
    ]);
    Route::post('products/store', [
        'as' => 'admin.products.store',
        'uses' => 'ProductController@store'
    ]);
    Route::delete('products/delete/{id}', [
        'as' => 'admin.products.delete',
        'uses' => 'ProductController@delete'
    ]);
    Route::get('products/upload-picture', [
        'as' => 'admin.products.upload-picture',
        'uses' => 'ProductController@uploadPicture'
    ]);
    Route::post('products/store-picture', [
        'as' => 'admin.products.store-picture',
        'uses' => 'ProductController@storePicture'
    ]);
    //Vendors
    Route::get('vendors', [
        'as' => 'admin.vendors',
        'uses' => 'VendorController@index'
    ]);
    Route::get('vendors/view/{id}', [
        'as' => 'admin.vendors.view',
        'uses' => 'VendorController@view'
    ]);
    Route::get('vendors/create', [
        'as' => 'admin.vendors.create',
        'uses' => 'VendorController@create'
    ]);
    Route::post('vendors/store', [
        'as' => 'admin.vendors.store',
        'uses' => 'VendorController@store'
    ]);
    Route::delete('vendors/delete/{id}', [
        'as' => 'admin.vendors.delete',
        'uses' => 'VendorController@delete'
    ]);
    //Inventories
    Route::get('inventories/create', [
        'as' => 'admin.inventories.create',
        'uses' => 'InventoryController@create'
    ]);
    Route::post('inventories/store', [
        'as' => 'admin.inventories.store',
        'uses' => 'InventoryController@store'
    ]);
    Route::delete('inventories/delete/{id}', [
        'as' => 'admin.inventories.delete',
        'uses' => 'InventoryController@delete'
    ]);
    //Staff
    Route::get('staff', [
        'as' => 'admin.staff',
        'uses' => 'StaffController@index'
    ]);
    Route::delete('staff/delete/{id}', [
        'as' => 'admin.staff.delete',
        'uses' => 'StaffController@delete'
    ]);
    //Orders
    Route::get('orders', [
        'as' => 'admin.orders',
        'uses' => 'OrderController@index'
    ]);
    Route::delete('orders/delete/{id}', [
        'as' => 'admin.orders.delete',
        'uses' => 'OrderController@delete'
    ]);
});
