@extends('admin.layouts.main')

@section('content')
    <div class="maincontent section">
        <div class="cardflexblock">
            <div class="cardwrapper">
                <div class="cardtitle">Clients</div>
                <div class="reportcardamount">425 Active</div>
                <div class="reportcardwtd">+ 2 WTD</div>
                <div class="reportcardmtd">+ 7 MTD</div>
                <div class="reportcardytd">+ 23 YTD</div>
                <div class="reportcardsubtext">Details...</div>
            </div>
            <div class="cardwrapper">
                <div class="cardtitle">Products</div>
                <div class="reportcardamount">13 Active</div>
                <div class="reportcardsubtext">2 Low Stock</div>
                <div class="reportcardsubtext">3 Stale Stock</div>
                <div class="reportcardsubtext">2 Inactive</div>
                <div class="reportcardsubtext">Details...</div>
            </div>
            <div class="cardwrapper">
                <div class="cardtitle">Sales</div>
                <div class="reportcardamount">$1,300</div>
                <div class="reportcardwtd">$1,300 WTD</div>
                <div class="reportcardmtd">$7,650 MTD</div>
                <div class="reportcardytd">$32,743 YTD</div>
                <div class="reportcardsubtext">Details...</div>
            </div>
            <div class="cardwrapper">
                <div class="cardtitle">Deliveries</div>
                <div class="reportcardamount">41</div>
                <div class="reportcardwtd">+ 42 WTD</div>
                <div class="reportcardmtd">+ 76 MTD</div>
                <div class="reportcardytd">+ 327 YTD</div>
                <div class="reportcardsubtext">Details...</div>
            </div>
        </div>
    </div>
@endsection