<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', 'Title')</title>

    <!-- Styles -->
    {{ Html::style('css/admin/normalize.css') }}
    {{ Html::style('css/admin/webflow.css') }}
    {{ Html::style('css/admin/app.css') }}
    {{ Html::style('css/admin/secondary.css') }}
    {{ Html::style('css/admin/robocrop.min.css') }}
    {{ Html::style('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}
    {{ Html::style('//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css') }}

    <!-- JavaScripts -->
    {{ Html::script('//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js') }}

    <script type="text/javascript">
        WebFont.load({
            google: {
                families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]
            }
        });
    </script>

    {{ Html::script('js/admin/modernizr.js') }}

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="body">

@include('admin.includes.header')
<!-- Content Wrapper. Contains page content -->
<div class="maincontent section">
    @yield('content')
</div>

@include('admin.includes.footer')
<!-- JavaScripts -->
@yield('before-scripts')
{{ Html::script('//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js') }}
{{ Html::script('//code.jquery.com/ui/1.11.2/jquery-ui.js') }}
{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js') }}
{{ Html::script('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js') }}
{{ Html::script('js/admin/webflow.js') }}
{{ Html::script('js/admin/app.js') }}
@yield('after-scripts')
</body>
</html>