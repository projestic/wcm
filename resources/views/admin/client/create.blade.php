@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">New Client</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
        </div>
        {{ print_r($errors->all()) }}
        <div class="outputblock">
            <div class="form700px w-form">
                <div class="formtitle">Enter Client Information</div>
                <div class="forminstruction">This process adds a new Client to your system. Only asterisked (*)
                    information is necessary. You can add/edit documents at any time.
                </div>
                {!! Form::open([
                    'route' => 'admin.clients.store'
                ]) !!}

                <div class="formrow w-row {{$errors->has('first_name') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('first_name', '*First Name', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('first_name', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Enter Clients First Name*',
                            'maxlength' => 75,
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('last_name') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('last_name', 'Last Name', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('last_name', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Enter Clients Last Name',
                            'maxlength' => 75
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('address_1') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('address_1', 'Address', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('address_1', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Delivery Address',
                            'maxlength' => 50
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('address_2') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('address_2', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Suite, Apt, Special Instructions',
                            'maxlength' => 50
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('city') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('city', 'City', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('city', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Delivery City',
                            'maxlength' => 50
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('state_id') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('state_id', 'State', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::select('state_id', $states, null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('email') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('email', 'Email', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('email', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Clients Email',
                            'type' => 'email',
                            'maxlength' => 75
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('phone') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('phone', 'Phone', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('phone', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => '310-555-1212',
                            'maxlength' => 15
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('to_text') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('to_text', 'Okay to Text', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('to_text', 1, true, [
                                'id' => 'to_text_1',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('to_text_1', 'Yes', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('to_text', 0, false, [
                                'id' => 'to_text_2',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('to_text_2', 'No', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('gender') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('gender', 'Gender', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('gender', \App\Models\Client::GENDER_MALE, true, [
                                'id' => 'gender_1',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('birthday_1', 'Male', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('gender', \App\Models\Client::GENDER_FEMALE, false, [
                                'id' => 'gender_2',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('gender_2', 'Female', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('birthday') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('birthday', 'Birthday', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('birthday', null, [
                            'class' => 'inputfield w-input datepicker'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3">Uploads</div>
                    <div class="w-col w-col-9 w-col-small-9">
                        <div class="forminstruction">Upload scans or pics of Clients ID card, Medical Certificate,
                            and/or any other documents using the Client Document Upload function. It will automatically
                            associate documents with the client.
                        </div>
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['id_type', 'id_num']) ? 'error-block' : ''}}">
                        {!! Form::label('id_type', '*ID Type', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-6 w-col-small-6 {{$errors->has('id_type') ? 'error-block' : ''}}">
                        {!! Form::select('id_type', \App\Models\Client::getIdTypesList(), null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...',
                            'required' => 'required'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('id_num') ? 'error-block' : ''}}">
                        {!! Form::text('id_num', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'ID Number',
                            'required' => 'required',
                            'maxlength' => 50
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('cert_type', 'cert_num') ? 'error-block' : ''}}">
                        {!! Form::label('cert_type', 'Cert Type', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-6 w-col-small-6 {{$errors->has('cert_type') ? 'error-block' : ''}}">
                        {!! Form::select('cert_type', \App\Models\Client::getCertTypesList(), null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('cert_num') ? 'error-block' : ''}}">
                        {!! Form::text('cert_num', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Medical Certificate Number',
                            'maxlength' => 50
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('referred') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('referred', 'Referred By', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('referred', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Client#, Google, Weed Maps, etc.',
                            'maxlength' => 100
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('notes') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('notes', 'Client Notes', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::textarea('notes', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Client Notes - not visible to customers',
                            'rows' => 5
                        ]) !!}
                    </div>
                </div>

                <div class="floatcleardiv"></div>
                <div class="flexrow">
                    <div>
                        <a class="cancelbutton w-button" href="{{ route('admin.clients') }}">Cancel</a>
                    </div>
                    {!! Form::submit('SUBMIT', [
                        'class' => 'submitbutton w-button'
                    ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection