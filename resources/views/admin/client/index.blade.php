@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Clients</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
            <div class="actionblock">
                <div class="submenudropdown w-dropdown" data-delay="0">
                    <div class="submenutoggle w-dropdown-toggle">
                        <div class="dropdowntext">
                            <i class="iconimage fa fa-cog" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true"></i>
                        </div>
                        <div class="submenuicon w-icon-dropdown-toggle"></div>
                    </div>
                    <nav class="submenulist w-dropdown-list">
                        <a class="submenulink w-dropdown-link" href="{{url('admin/clients/create')}}">NEW Client</a>
                        <a class="submenulink w-dropdown-link" href="#">Client Doc Upload</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Client Report</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Client CSV</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Bulk Upload Clients</a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="outputblock">

            @include('admin.includes.list_search', ['formRoute' => 'admin.clients'])

            <table class="resultstable">
                <thead>
                <tr>
                    <th class="resultsheader" style="text-align:left;" title="Client #">Client #</th>
                    <th class="resultsheader" style="text-align:left;" title="Last, First Name">Last, First Name</th>
                    <th class="resultsheader" style="text-align:left;" title="Zip Code">Zip Code</th>
                    <th class="resultsheader" style="text-align:center;" title="Email, Phone">Contact</th>
                    <th class="resultsheader" style="text-align:right;" title="Gross $ Spent">Gross $</th>
                    <th class="resultsheader" style="text-align:center;" title="Date Joined">Joined</th>
                    <th class="resultsheader" style="text-align:center;" title="Last Activity">Last Activity</th>
                    <th class="resultsheader" style="text-align:center;" title="Status">Status</th>
                    <th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($clients as $key => $client)
                        <tr class="resultsrow {{$key % 2 ? '' : 'oddrow'}}" title="ID: {{$client->client_id}} {{ $client->first_name }}" style="cursor: default;">
                            <td class="resultscell" style="text-align:left;">ID: {{ $client->client_id }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $client->last_name }} {{ $client->first_name }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $client->zip_code }}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="mailto:{{ $client->email }}" title="Email {{ $client->email }}" style="margin: 1px 5px;">
                                    <i class="iconimage fa fa-envelope-o" aria-hidden="true"></i>
                                </a>
                                <br/>
                                <a href="tel:{{ $client->phone }}" title="CAll {{ $client->first_name }}" style="margin: 1px 5px;">
                                    <i class="iconimage fa fa-phone" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="resultscell" style="text-align:center;">{!! mt_rand(1, 1000) !!}</td>
                            <td class="resultscell" style="text-align:center;">{!! date('Y-m-d', strtotime($client->created_at)) !!}</td>
                            <td class="resultscell" style="text-align:center;">{!! $client->last_activity !!}</td>
                            <td class="resultscell" style="text-align:center;">{!! $client->status !!}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="{{url('admin/clients/view', [$client->id])}}">
                                    <i class="iconaction fa fa-eye" title="VIEW" aria-hidden="true"></i>
                                </a>
                                <a href="{{url('admin/clients/edit', [$client->id])}}">
                                    <i class="iconaction fa fa-pencil-square-o" title="EDIT" aria-hidden="true"></i>
                                </a>
                                <a href="{{url('admin/clients/delete', [$client->id])}}" class="delete-btn">
                                    <i class="iconaction fa fa-trash-o" title="DELETE" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;">{{ $clients->total() }} CLIENTS</div>
            <div class="pagination-wrap w-clearfix">
                {{ $clients->links() }}
            </div>
        </div>
    </div>
@endsection