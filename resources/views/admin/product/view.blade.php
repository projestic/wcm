@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Product View</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
        </div>
        <div class="outputblock">
            <!-- SEARCH -->
            <div class="searchblock">
                <div class="w-form">
                    <form class="w-clearfix" name="SearchForm" method="get" action="search.js">
                        <input class="searchbutton w-button fa" type="submit" value="&#xf002;">
                        <input class="searchbox w-input" id="Search" maxlength="75" name="Search" placeholder="Search" type="text">
                    </form>
                </div>
            </div>
            <!-- SEARCH -->
            <div class="outputblock"><h4>Product View Offline for Demo</h4></div>
        </div>
    </div>
@endsection