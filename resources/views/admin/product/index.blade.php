@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Products</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
            <div class="actionblock">
                <div class="submenudropdown w-dropdown" data-delay="0">
                    <div class="submenutoggle w-dropdown-toggle">
                        <div class="dropdowntext">
                            <i class="iconimage fa fa-cog" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true"></i>
                        </div>
                        <div class="submenuicon w-icon-dropdown-toggle"></div>
                    </div>
                    <nav class="submenulist w-dropdown-list">
                        <a class="submenulink w-dropdown-link" href="{{ route('admin.products.create') }}">NEW Product</a>
                        <a class="submenulink w-dropdown-link" href="{{ url('admin/inventories/create') }}">ADD Inventory</a>
                        <a class="submenulink w-dropdown-link" href="{{ url('admin/products/upload-picture') }}">ADD Product Pics</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Product Report</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Product CSV</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Product Bulk Upload</a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="outputblock">

            @include('admin.includes.list_search', ['formRoute' => 'admin.products'])

            <table class="resultstable">
                <thead>
                <tr>
                    <th class="resultsheader" style="text-align:left;" title="Product Code">Item Code</th>
                    <th class="resultsheader" style="text-align:left;" title="Category">Category</th>
                    <th class="resultsheader" style="text-align:left;" title="Product">Product</th>
                    <th class="resultsheader" style="text-align:center;" title="Type">Type</th>
                    <th class="resultsheader" style="text-align:center;" title="Type">U/M</th>
                    <th class="resultsheader" style="text-align:center;" title="Cost">Sales #</th>
                    <th class="resultsheader" style="text-align:right;" title="Price">Sales $</th>
                    <th class="resultsheader" style="text-align:right;" title="Inventory">Inventory</th>
                    <th class="resultsheader" style="text-align:center;" title="Status">Status</th>
                    <th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($products as $key => $product)
                        <tr class="resultsrow {{$key % 2 ? '' : 'oddrow'}}" title="ID: {{ $product->item_id }} {{ $product->product }}" style="cursor: default;">
                            <td class="resultscell" style="text-align:left;">{{ $product->item_id }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $product->category }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $product->product }}</td>
                            <td class="resultscell" style="text-align:center;"></td>
                            <td class="resultscell" style="text-align:center;"></td>
                            <td class="resultscell" style="text-align:center;">{{ $product->sales_count }}</td>
                            <td class="resultscell" style="text-align:right;">$ {!! $product->sales_count  * $product->price_1 !!}</td>
                            <td class="resultscell" style="text-align:right;">{!! rand(1, 1000) !!}</td>
                            <td class="resultscell" style="text-align:center;">{{ $product->status }}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="{{url('admin/products/view', [$product->id])}}">
                                    <i class="iconaction fa fa-eye" title="VIEW" aria-hidden="true"></i>
                                </a>
                                <a href="{{url('admin/products/edit', [$product->id])}}">
                                    <i class="iconaction fa fa-pencil-square-o" title="EDIT" aria-hidden="true"></i>
                                </a>
                                <a href="{{url('admin/products/delete', [$product->id])}}" class="delete-btn">
                                    <i class="iconaction fa fa-trash-o" title="DELETE" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;">{{ $products->total() }} PRODUCTS</div>
            <div class="pagination-wrap w-clearfix">
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection