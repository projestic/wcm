@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">New Product</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
        </div>
        <div class="outputblock">
            <div class="form700px w-form">
                <div class="formtitle">Enter Product Information</div>
                <div class="forminstruction">
                    This process puts a product in your line-up. Only asterisked (*) information is necessary. The steps
                    to add Product in your store are: 1) Upload images to Gallery; 2) Enter Product Information [this
                    screen], 3) Add inventory & batch. You can add/edit images and inventory at any time. If you have
                    additional information you'd like to communicate to your customers - e.g., terpenoids - there are
                    two options: 1) Put all your information in 'Description' field; 2) Create a separate webpage and
                    put the link in 'External Detail Link'. To make the product invisible to the customers, change
                    Status to 'Inactive'. Status will automatically change to Inactive when inventory reaches 0.
                </div>
                {!! Form::open([
                    'route' => 'admin.products.store'
                ]) !!}

                <div class="formrow w-row {{$errors->has('category') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('category', '*Category', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::select('category', \App\Models\Product::getCategoriesList(), null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('product') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('product', '*Product Name', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('product', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Enter Product Name*',
                            'maxlength' => 50,
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('description') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('description', 'Description', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::textarea('description', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Product Description',
                            'rows' => 5
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('detail_link') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('detail_link', 'External Detail Link', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('detail_link', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'http://'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['picture_1', 'picture_2', 'picture_3']) ? 'error-block' : ''}}">
                        {!! Form::label('picture_1', 'Product Pics', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9 {{$errors->has('picture_1') ? 'error-block' : ''}}">
                        {!! Form::select('picture_1', [], null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Pic 1 - No Product Image'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('picture_2') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::select('picture_2', [], null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Pic 2 - Blank'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('picture_3') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::select('picture_3', [], null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Pic 3 - Blank'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['cost_1', 'price_1', 'unit_1']) ? 'error-block' : ''}}">
                        {!! Form::label('cost_1', '*Product Price Point #1', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('cost_1') ? 'error-block' : ''}}">
                        {!! Form::text('cost_1', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Cost / Unit*',
                            'required' => 'required'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('price_1') ? 'error-block' : ''}}">
                        {!! Form::text('price_1', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Price / Unit*',
                            'required' => 'required'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('unit_1') ? 'error-block' : ''}}">
                        {!! Form::select('unit_1', \App\Models\Product::getUnitTypesList(), null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...*',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['cost_2', 'price_2', 'unit_2']) ? 'error-block' : ''}}">
                        {!! Form::label('cost_2', 'Product Price Point #2', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('cost_2') ? 'error-block' : ''}}">
                        {!! Form::text('cost_2', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Cost / Unit'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('price_2') ? 'error-block' : ''}}">
                        {!! Form::text('price_2', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Price / Unit'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('unit_2') ? 'error-block' : ''}}">
                        {!! Form::select('unit_2', [], null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['cost_3', 'price_3', 'unit_3']) ? 'error-block' : ''}}">
                        {!! Form::label('cost_3', 'Product Price Point #3', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('cost_3') ? 'error-block' : ''}}">
                        {!! Form::text('cost_3', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Cost / Unit',
                            'maxlength' => 10
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('price_3') ? 'error-block' : ''}}">
                        {!! Form::text('price_3', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Price / Unit',
                            'maxlength' => 10
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('unit_3') ? 'error-block' : ''}}">
                        {!! Form::select('unit_3', [], null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['cost_4', 'price_4', 'unit_4']) ? 'error-block' : ''}}">
                        {!! Form::label('cost_4', 'Product Price Point #4', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('cost_4') ? 'error-block' : ''}}">
                        {!! Form::text('cost_4', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Cost / Unit',
                            'maxlength' => 10
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('price_4') ? 'error-block' : ''}}">
                        {!! Form::text('price_4', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Price / Unit',
                            'maxlength' => 10
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('unit_4') ? 'error-block' : ''}}">
                        {!! Form::select('unit_4', [], null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['cost_5', 'price_5', 'unit_5']) ? 'error-block' : ''}}">
                        {!! Form::label('cost_5', 'Product Price Point #5', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('cost_5') ? 'error-block' : ''}}">
                        {!! Form::text('cost_5', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Cost / Unit',
                            'maxlength' => 10
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('price_5') ? 'error-block' : ''}}">
                        {!! Form::text('price_5', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Price / Unit',
                            'maxlength' => 10
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('unit_5') ? 'error-block' : ''}}">
                        {!! Form::select('unit_5', [], null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['tax', 'tax_rate']) ? 'error-block' : ''}}">
                        {!! Form::label('tax', '*Tax Method &amp; Rate(s)', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-6 w-col-small-6 {{$errors->has('tax') ? 'error-block' : ''}}">
                        {!! Form::select('tax', \App\Models\Product::getTaxMethodsList(), null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select one...',
                            'required' => 'required'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->has('tax_rate') ? 'error-block' : ''}}">
                        {!! Form::text('tax_rate', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Tax Rate',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('position') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('position', 'Position', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('position', \App\Models\Product::POSITION_FEATURED, true, [
                                'id' => 'position_1',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('position_1', 'Featured', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('position', \App\Models\Product::POSITION_REGULAR, false, [
                                'id' => 'position_2',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('position_2', 'Regular', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('status') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('status', 'Product Status', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('status', \App\Models\Product::STATUS_ACTIVE, true, [
                                'id' => 'status_1',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('status_1', 'Active', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('status', \App\Models\Product::STATUS_INACTIVE, false, [
                                'id' => 'status_2',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('status_2', 'Inactive', [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('inv_alert') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('inv_alert', 'Low Inventory Alert', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-6 w-col-small-6">
                        {!! Form::text('inv_alert', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Low Inventory Alert Amount'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('notes') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('notes', 'Product Notes', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::textarea('notes', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Product Notes - not visible to customers',
                            'rows' => 2
                        ]) !!}
                    </div>
                </div>

                <div class="floatcleardiv"></div>
                <div class="flexrow">
                    <div>
                        <a class="cancelbutton w-button" href="{{ route('admin.products') }}">Cancel</a>
                    </div>
                    {!! Form::submit('SUBMIT', [
                        'class' => 'submitbutton w-button'
                    ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection