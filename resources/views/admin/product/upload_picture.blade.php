@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Products</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
        </div>
        <div class="outputblock">
            <div class="form700px w-form">
                <div class="formtitle">Select Product Image</div>
                <div class="forminstruction">This process adds an image to your product image gallery. Asterisked (*) information is necessary. You can add as many images to your gallery as you like. Images can then be assigned to a product in the Product Add or Product Edit screens.</div>
                <div class="formrow w-row" style="margin-bottom:10px;">
                    <div class="w-col w-col-3 w-col-small-3"><label class="fieldlabel" for="picture">*Product Image File</label></div>
                    <div class="w-col w-col-9 w-col-small-9">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div id="demo-profile">
                            <div class="crop-element"
                                 data-watermark="main"
                                 data-name="product_picture"
                                 data-upload="{{ url('admin/products/store-picture') }}"
                                 data-crop="=250,=250"
                                 data-crop-required="true">
                                <img/>
                                <input type="file"/>
                                <button class="btn btn-sm btn-default edit"><i class="fa fa-edit"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="formrow w-row" style="margin-bottom:10px;">
                    <div class="w-col w-col-4 w-col-small-4"></div>
                </div>
                <div class="floatcleardiv"></div>
                <div class="flexrow">
                    <div><a class="cancelbutton w-button" href="products.php">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    {{ Html::script('js/admin/robocrop.js') }}
    {{ Html::script('js/admin/robocrop-custom.js') }}

    <script type="text/javascript">
        robocrop.watermarks = {
            main: {
                file: '/images/admin/brandimage.png',
                opacity: .3,
                position: "bottom-right",
                margin_top: 0,
                margin_right: 5,
                margin_bottom: 5,
                margin_left: 0
            }
        };
    </script>
@endsection