<nav class="navmenu w-nav-menu" role="navigation">
    {!! $adminMenu->asUl(['class' => 'menuscrollwrap']) !!}
</nav>

<div class="menubutton w-nav-button">
    <div class="navicon w-icon-nav-menu"></div>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>