<div class="section">
    <div class="headerblock">
        <div class="headernav w-nav" data-animation="over-right" data-collapse="all" data-doc-height="1" data-duration="400">
            <div class="headerrow w-row">
                <div class="w-clearfix w-col w-col-1 w-col-medium-1 w-col-small-4 w-col-tiny-4">
                    <a class="brandlink w-nav-brand" href="#" target="_blank">
                        <img class="brandimage" src="{{URL::asset('/images/admin/brandimage.png')}}" width="36">
                    </a>
                </div>
                <div class="w-col w-col-10 w-col-medium-10 w-col-small-4 w-col-tiny-4">
                    <div class="bizname"></div>
                </div>
                <div class="w-clearfix w-col w-col-1 w-col-medium-1 w-col-small-4 w-col-tiny-4">
                    @include('admin.includes.menu')
                </div>
            </div>
        </div>
    </div>
</div>