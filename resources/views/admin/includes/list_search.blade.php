<!-- SEARCH -->
<div class="searchblock">
    <div class="w-form">
        {!! Form::open([
            'method' => 'get',
            'route' => $formRoute
        ]) !!}
        {!! Form::button('<i class="fa fa-search"></i>', [
            'type' => 'submit',
            'class' => 'searchbutton w-button'
        ]) !!}
        {!! Form::text('search', Request::get('search'), [
            'class' => 'searchbox w-input',
            'placeholder' => 'Search',
            'maxlength' => 75
        ]) !!}
        {!! Form::close() !!}
    </div>
</div>
<!-- SEARCH -->