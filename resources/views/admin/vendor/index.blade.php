@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Vendors</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
            <div class="actionblock">
                <div class="submenudropdown w-dropdown" data-delay="0">
                    <div class="submenutoggle w-dropdown-toggle">
                        <div class="dropdowntext">
                            <i class="iconimage fa fa-cog" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true"></i>
                        </div>
                        <div class="submenuicon w-icon-dropdown-toggle"></div>
                    </div>
                    <nav class="submenulist w-dropdown-list">
                        <a class="submenulink w-dropdown-link" href="{{ url('admin/vendors/create') }}">NEW Vendor</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Vendor Report</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Vendor CSV</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Bulk Upload Vendors</a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="outputblock">

            @include('admin.includes.list_search', ['formRoute' => 'admin.vendors'])

            <table class="resultstable">
                <thead>
                <tr>
                    <th class="resultsheader" style="text-align:left;" title="Vendor #">Vendor #</th>
                    <th class="resultsheader" style="text-align:left;" title="Vendor Name">Vendor Name</th>
                    <th class="resultsheader" style="text-align:left;" title="Contact Name">Representative</th>
                    <th class="resultsheader" style="text-align:center;" title="Zip Code">Zip Code</th>
                    <th class="resultsheader" style="text-align:center;" title="Email, Phone">Contact</th>
                    <th class="resultsheader" style="text-align:right;" title="Gross $ Spent">Gross $</th>
                    <th class="resultsheader" style="text-align:center;" title="Last Activity">Last Activity</th>
                    <th class="resultsheader hidden-xs" style="text-align:center;" title="Status">Status</th>
                    <th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($vendors as $key => $vendor)
                        <tr class="resultsrow {{$key % 2 ? '' : 'oddrow'}}" title="ID: {{$vendor->vendor_id}} {{ $vendor->$vendor }}" style="cursor: default;">
                            <td class="resultscell" style="text-align:left;">{{ $vendor->vendor_id }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $vendor->vendor }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $vendor->last_name }}, {{ $vendor->first_name }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $vendor->zip_code }}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="mailto:{{ $vendor->email }}" title="Email {{ $vendor->email }}" style="margin: 1px 5px;">
                                    <i class="iconimage fa fa-envelope-o" aria-hidden="true"></i>
                                </a>
                                <br/>
                                <a href="tel:{{ $vendor->phone }}" title="CAll {{ $vendor->cname }}" style="margin: 1px 5px;">
                                    <i class="iconimage fa fa-phone" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="resultscell" style="text-align:center;">{!! mt_rand(1, 1000) !!}</td>
                            <td class="resultscell" style="text-align:center;">{{ $vendor->last_activity }}</td>
                            <td class="resultscell" style="text-align:center;">{{ $vendor->status }}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="{{ url('admin/vendors/view', [$vendor->id]) }}">
                                    <i class="iconaction fa fa-eye" title="VIEW" aria-hidden="true"></i>
                                </a>
                                <a href="{{ url('admin/vendors/edit', [$vendor->id]) }}">
                                    <i class="iconaction fa fa-pencil-square-o" title="EDIT" aria-hidden="true"></i>
                                </a>
                                <a href="{{ url('admin/vendors/delete', [$vendor->id]) }}" class="delete-btn">
                                    <i class="iconaction fa fa-trash-o" title="DELETE" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;">{{ $vendors->total() }} VENDORS</div>
            <div class="pagination-wrap w-clearfix">
                {{ $vendors->links() }}
            </div>
        </div>
    </div>
@endsection