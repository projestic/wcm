@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">New Vendor</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
        </div>
        <div class="outputblock">
            <div class="form700px w-form">
                <div class="formtitle">Enter Vendor Information</div>
                <div class="forminstruction">
                    This process adds a vendor to your list of suppliers. Only asterisked (*) information is necessary.
                </div>
                {!! Form::open([
                    'route' => 'admin.vendors.store'
                ]) !!}

                <div class="formrow w-row {{$errors->has('vendor') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('vendor', '*Vendor Name', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('vendor', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Enter Vendor Business Name*',
                            'maxlength' => 75,
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['first_name', 'last_name']) ? 'error-block' : ''}}">
                        {!! Form::label('first_name', 'Contact Name', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-4 w-col-small-4 {{$errors->has('first_name') ? 'error-block' : ''}}">
                        {!! Form::text('first_name', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'First Name',
                            'maxlength' => 75
                        ]) !!}
                    </div>
                    <div class="w-col w-col-5 w-col-small-5 {{$errors->has('last_name') ? 'error-block' : ''}}">
                        {!! Form::text('last_name', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Last Name',
                            'maxlength' => 75
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->hasAny(['email', 'phone']) ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('email', 'Contact Info', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-4 w-col-small-4 {{$errors->has('email') ? 'error-block' : ''}}">
                        {!! Form::text('email', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Email Address',
                            'maxlength' => 75,
                        ]) !!}
                    </div>
                    <div class="w-col w-col-5 w-col-small-5 {{$errors->has('phone') ? 'error-block' : ''}}">
                        {!! Form::text('phone', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Phone Number',
                            'maxlength' => 15,
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('city') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                    </div>
                    <div class="w-col w-col-4 w-col-small-4">
                        {!! Form::text('city', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'City',
                            'maxlength' => 50,
                        ]) !!}
                    </div>
                    <div class="w-col w-col-5 w-col-small-5">
                        {!! Form::text('zip', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Zip Code',
                            'maxlength' => 15,
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('address_1') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('address_1', 'Address', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('address_1', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Delivery Address',
                            'maxlength' => 50
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('address_2') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::text('address_2', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Suite, Apt, Special Instructions',
                            'maxlength' => 50
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('status') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('status', 'Vendor Status', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('status', \App\Models\Vendor::STATUS_ACTIVE, true, [
                                'id' => 'status_1',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('status_1', \App\Models\Vendor::STATUS_ACTIVE, [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3">
                        <div class="w-clearfix w-radio">
                            {!! Form::radio('status', \App\Models\Vendor::STATUS_INACTIVE, false, [
                                'id' => 'status_2',
                                'class' => 'radio w-radio-input'
                            ]) !!}
                            {!! Form::label('status_2', \App\Models\Vendor::STATUS_INACTIVE, [
                                'class' => 'fieldlabel w-form-label'
                            ]) !!}
                        </div>
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('notes') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('notes', 'Vendor Notes', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::textarea('notes', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Notes about this Vendor',
                            'rows' => 5
                        ]) !!}
                    </div>
                </div>

                <div class="floatcleardiv"></div>
                <div class="flexrow">
                    <div>
                        <a class="cancelbutton w-button" href="{{ route('admin.clients') }}">Cancel</a>
                    </div>
                    {!! Form::submit('SUBMIT', [
                        'class' => 'submitbutton w-button'
                    ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection