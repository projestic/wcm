@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Staff</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
            <div class="actionblock">
                <div class="submenudropdown w-dropdown" data-delay="0">
                    <div class="submenutoggle w-dropdown-toggle">
                        <div class="dropdowntext">
                            <i class="iconimage fa fa-cog" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true"></i>
                        </div>
                        <div class="submenuicon w-icon-dropdown-toggle"></div>
                    </div>
                    <nav class="submenulist w-dropdown-list">
                        <a class="submenulink w-dropdown-link" href="{{ url('admin/staff/create') }}">NEW Staff</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Sales Report</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Activity Report</a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="outputblock">

            @include('admin.includes.list_search', ['formRoute' => 'admin.staff'])

            <table class="resultstable">
                <thead>
                <tr>
                    <th class="resultsheader" style="text-align:left;" title="User #">User #</th>
                    <th class="resultsheader" style="text-align:left;" title="Name">Name</th>
                    <th class="resultsheader" style="text-align:center;" title="Email, Phone">Contact</th>
                    <th class="resultsheader hidden-xs" style="text-align:center;" title="Roles">Roles</th>
                    <th class="resultsheader hidden-xs" style="text-align:center;" title="Staff Access">Admin Access</th>
                    <th class="resultsheader hidden-xs" style="text-align:center;" title="Date Added">Added</th>
                    <th class="resultsheader hidden-xs" style="text-align:center;" title="Last Login">Last Login</th>
                    <th class="resultsheader hidden-xs" style="text-align:center;" title="Status">Status</th>
                    <th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($staff as $key => $user)
                        <tr class="resultsrow {{$key % 2 ? '' : 'oddrow'}}" title="ID: {{$user->user_id}} {{ $user->first_name }}" style="cursor: default;">
                            <td class="resultscell" style="text-align:left;">{{ $user->user_id }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $user->first_name }}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="mailto:{{ $user->email }}" title="Email {{ $user->email }}" style="margin: 1px 5px;">
                                    <i class="iconimage fa fa-envelope-o" aria-hidden="true"></i>
                                </a>
                                <br/>
                                <a href="tel:{{ $user->phone }}" title="CAll {{ $user->first_name }}" style="margin: 1px 5px;">
                                    <i class="iconimage fa fa-phone" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="resultscell" style="text-align:center;">
                                @php ($hasRole = false)
                                @if ($user->role_p == 'Y')
                                    @php ($hasRole = true)
                                    <i class="fa" style="margin-right: 5px;" title="Point of Sale">&#xf155</i>
                                @endif
                                @if ($user->role_d == 'Y')
                                    @php ($hasRole = true)
                                    <i class="fa" style="margin-right: 5px;" title="Delivery">&#xf1b9</i>
                                @endif
                                @if ($user->role_u == 'Y')
                                    @php ($hasRole = true)
                                    <i class="fa" style="margin-right: 5px;" title="User">&#xf2c0</i>
                                @endif
                                @if (!$hasRole)
                                    <i class="fa" style="margin-right: 5px;" title="None">&#xf05e</i>
                                @endif
                            </td>
                            <td class="resultscell hidden-xs" style="text-align:center;">
                                @php ($hasAuth = false)
                                @if ($user->auth_a == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Dashboard">&#xf135</i>
                                @endif
                                @if ($user->auth_w == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Workflow">&#xf0e8</i>
                                @endif
                                @if ($user->auth_p == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Products">&#xf217</i>
                                @endif
                                @if ($user->auth_c == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Clients">&#xf1ae</i>
                                @endif
                                @if ($user->auth_d == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Orders">&#xf155</i>
                                @endif
                                @if ($user->auth_s == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Deliveries">&#xf1b9</i>
                                @endif
                                @if ($user->auth_v == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Staff">&#xf2c0</i>
                                @endif
                                @if ($user->auth_r == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Vendors">&#xf0d1</i>
                                @endif
                                @if ($user->auth_x == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="Reports">&#xf080</i>
                                @endif
                                @if ($user->auth_z == 'Y')
                                    @php ($hasAuth = true)
                                    <i class="fa" style="margin-right: 5px;" title="System">&#xf0ad</i>
                                @endif
                                @if (!$hasAuth)
                                    <i class="fa" style="margin-right: 5px;" title="None">&#xf05e</i>
                                @endif
                            </td>
                            <td class="resultscell" style="text-align:center;">{{ $user->created_at }}</td>
                            <td class="resultscell" style="text-align:center;">{{ $user->last_login }}</td>
                            <td class="resultscell" style="text-align:center;">{{ $user->status }}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="{{url('admin/staff/view', [$user->id])}}">
                                    <i class="iconaction fa fa-eye" title="VIEW" aria-hidden="true"></i>
                                </a>
                                <a href="{{url('admin/staff/edit', [$user->id])}}">
                                    <i class="iconaction fa fa-pencil-square-o" title="EDIT" aria-hidden="true"></i>
                                </a>
                                <a href="{{url('admin/staff/delete', [$user->id])}}" class="delete-btn">
                                    <i class="iconaction fa fa-trash-o" title="DELETE" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;">{{ $staff->total() }} STAFF</div>
            <div class="pagination-wrap w-clearfix">
                {{ $staff->links() }}
            </div>
        </div>
    </div>
@endsection