@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Orders</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
            <div class="actionblock">
                <div class="submenudropdown w-dropdown" data-delay="0">
                    <div class="submenutoggle w-dropdown-toggle">
                        <div class="dropdowntext">
                            <i class="iconimage fa fa-cog" style="font-size: 10px; margin: 1px 1px;" aria-hidden="true"></i>
                        </div>
                        <div class="submenuicon w-icon-dropdown-toggle"></div>
                    </div>
                    <nav class="submenulist w-dropdown-list">
                        <a class="submenulink w-dropdown-link" href="{{ url('admin/order/create') }}">NEW Order</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Sales Report</a>
                        <a class="submenulink w-dropdown-link" href="#" onclick="javascript:alert('Contact Support to Activate');">Activity Report</a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="outputblock">

            @include('admin.includes.list_search', ['formRoute' => 'admin.orders'])

            <table class="resultstable">
                <thead>
                <tr>
                    <th class="resultsheader" style="text-align:left;" title="Order Date">Order Date</th>
                    <th class="resultsheader" style="text-align:left;" title="Order ID">Order ID</th>
                    <th class="resultsheader hidden-xs" style="text-align:left;" title="Last, First Name">Last, First Name</th>
                    <th class="resultsheader" style="text-align:right;" title="Order Amount">Amount</th>
                    <th class="resultsheader hidden-xs" style="text-align:right;" title="Paid Amount">Paid</th>
                    <th class="resultsheader hidden-xs"  style="text-align:center;" title="Status">Status</th>
                    <th class="resultsheader" style="text-align:center;" title="Action">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $key => $order)
                        <tr class="resultsrow {{$key % 2 ? '' : 'oddrow'}}" title="ID: {{$order->order_id}} {{ $order->order_id }}" style="cursor: default;">
                            <td class="resultscell" style="text-align:left;">{{ date('M d Y', strtotime($order->date)) }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $order->order_id }}</td>
                            <td class="resultscell" style="text-align:left;">{{ $order->client->first_name }} {{ $order->client->last_name }}</td>
                            <td class="resultscell" style="text-align:center;">{{ $order->amount_due }}</td>
                            <td class="resultscell" style="text-align:center;">{{ 0.00 }}</td>
                            <td class="resultscell" style="text-align:center;">{{ $order->status }}</td>
                            <td class="resultscell" style="text-align:center;">
                                <a href="{{ url('admin/orders/view', [$order->id]) }}">
                                    <i class="iconaction fa fa-eye" title="VIEW" aria-hidden="true"></i>
                                </a>
                                <a href="{{ url('admin/orders/edit', [$order->id]) }}">
                                    <i class="iconaction fa fa-pencil-square-o" title="EDIT" aria-hidden="true"></i>
                                </a>
                                <a href="{{ url('admin/orders/delete', [$order->id]) }}" class="delete-btn">
                                    <i class="iconaction fa fa-trash-o" title="DELETE" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="text-align:center; background-color:#FFF; border-top: silver 1px solid; margin-top:10px;">{{ $orders->total() }} ORDERS</div>
            <div class="pagination-wrap w-clearfix">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection