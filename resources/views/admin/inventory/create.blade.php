@extends('admin.layouts.main')

@section('content')
    <h2 class="pagename">Add Inventory</h2>
    <div class="displayblock">
        <div class="tableheader">
            <div><!-- HEADER TEXT -->&nbsp;</div>
        </div>
        <div class="outputblock">
            <div class="form700px w-form">
                <div class="formtitle">Enter Inventory Amount</div>
                <div class="forminstruction">
                    This process adds inventory to your products in stock. Asterisked (*) information is necessary. You
                    can add/edit inventory at any time. Product Status will automatically change to Inactive when
                    inventory reaches 0.
                </div>
                {!! Form::open([
                    'route' => 'admin.inventories.store'
                ]) !!}

                <div class="formrow w-row {{$errors->has('product_id') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('product_id', '*Product', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::select('product_id', $products, null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select Product...',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('trans_amount') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('trans_amount', '*Inventory Amount', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::number('trans_amount', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Enter Desired Amount to be added to Inventory*',
                            'step' => '0.0001',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row">
                    <div class="w-col w-col-3 w-col-small-3 {{$errors->hasAny(['vendor_id', 'batch']) ? 'error-block' : ''}}">
                        {!! Form::label('vendor_id', 'Vendor Info', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-4 w-col-small-4 {{$errors->has('vendor_id') ? 'error-block' : ''}}">
                        {!! Form::select('vendor_id', $vendors, null, [
                            'class' => 'inputfield w-select',
                            'placeholder' => 'Select Vendor...',
                            'required' => 'required'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-5 w-col-small-5 {{$errors->has('batch') ? 'error-block' : ''}}">
                        {!! Form::number('batch', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Batch Number',
                            'maxlength' => 75,
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="formrow w-row {{$errors->has('notes') ? 'error-block' : ''}}">
                    <div class="w-col w-col-3 w-col-small-3">
                        {!! Form::label('notes', 'Inventory Notes', [
                            'class' => 'fieldlabel'
                        ]) !!}
                    </div>
                    <div class="w-col w-col-9 w-col-small-9">
                        {!! Form::textarea('notes', null, [
                            'class' => 'inputfield w-input',
                            'placeholder' => 'Inventory Transaction Notes - not visible to customers',
                            'rows' => 5
                        ]) !!}
                    </div>
                </div>

                <div class="floatcleardiv"></div>
                <div class="flexrow">
                    <div>
                        <a class="cancelbutton w-button" href="{{ route('admin.products') }}">Cancel</a>
                    </div>
                    {!! Form::submit('SUBMIT', [
                        'class' => 'submitbutton w-button'
                    ]) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection